set grid_mode,1
set surface_quality, 0
load AARNA6193231W_u_BP.pdb
hide everything, AARNA6193231W_u_BP
show cartoon, AARNA6193231W_u_BP
color white, AARNA6193231W_u_BP
spectrum b, rainbow, AARNA6193231W_u_BP
set grid_slot, 1, AARNA6193231W_u_BP
center AARNA6193231W_u_BP
orient AARNA6193231W_u_BP
show surface, AARNA6193231W_u_BP
load AARNA6193231W_u_EC.pdb
hide everything, AARNA6193231W_u_EC
show cartoon, AARNA6193231W_u_EC
color white, AARNA6193231W_u_EC
spectrum b, rainbow, AARNA6193231W_u_EC
set grid_slot, 2, AARNA6193231W_u_EC
center AARNA6193231W_u_EC
orient AARNA6193231W_u_EC
show surface, AARNA6193231W_u_EC
load AARNA6193231W_u_LNMAX.pdb
hide everything, AARNA6193231W_u_LNMAX
show cartoon, AARNA6193231W_u_LNMAX
color white, AARNA6193231W_u_LNMAX
spectrum b, rainbow, AARNA6193231W_u_LNMAX
set grid_slot, 3, AARNA6193231W_u_LNMAX
center AARNA6193231W_u_LNMAX
orient AARNA6193231W_u_LNMAX
show surface, AARNA6193231W_u_LNMAX
load AARNA6193231W_u_LNMIN.pdb
hide everything, AARNA6193231W_u_LNMIN
show cartoon, AARNA6193231W_u_LNMIN
color white, AARNA6193231W_u_LNMIN
spectrum b, rainbow, AARNA6193231W_u_LNMIN
set grid_slot, 4, AARNA6193231W_u_LNMIN
center AARNA6193231W_u_LNMIN
orient AARNA6193231W_u_LNMIN
show surface, AARNA6193231W_u_LNMIN
set seq_view, 1
set transparency, 0.0
