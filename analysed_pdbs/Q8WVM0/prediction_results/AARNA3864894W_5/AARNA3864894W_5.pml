set grid_mode,1
set surface_quality, 0
load AARNA3864894W_5_BP.pdb
hide everything, AARNA3864894W_5_BP
show cartoon, AARNA3864894W_5_BP
color white, AARNA3864894W_5_BP
spectrum b, rainbow, AARNA3864894W_5_BP
set grid_slot, 1, AARNA3864894W_5_BP
center AARNA3864894W_5_BP
orient AARNA3864894W_5_BP
show surface, AARNA3864894W_5_BP
load AARNA3864894W_5_EC.pdb
hide everything, AARNA3864894W_5_EC
show cartoon, AARNA3864894W_5_EC
color white, AARNA3864894W_5_EC
spectrum b, rainbow, AARNA3864894W_5_EC
set grid_slot, 2, AARNA3864894W_5_EC
center AARNA3864894W_5_EC
orient AARNA3864894W_5_EC
show surface, AARNA3864894W_5_EC
load AARNA3864894W_5_LNMAX.pdb
hide everything, AARNA3864894W_5_LNMAX
show cartoon, AARNA3864894W_5_LNMAX
color white, AARNA3864894W_5_LNMAX
spectrum b, rainbow, AARNA3864894W_5_LNMAX
set grid_slot, 3, AARNA3864894W_5_LNMAX
center AARNA3864894W_5_LNMAX
orient AARNA3864894W_5_LNMAX
show surface, AARNA3864894W_5_LNMAX
load AARNA3864894W_5_LNMIN.pdb
hide everything, AARNA3864894W_5_LNMIN
show cartoon, AARNA3864894W_5_LNMIN
color white, AARNA3864894W_5_LNMIN
spectrum b, rainbow, AARNA3864894W_5_LNMIN
set grid_slot, 4, AARNA3864894W_5_LNMIN
center AARNA3864894W_5_LNMIN
orient AARNA3864894W_5_LNMIN
show surface, AARNA3864894W_5_LNMIN
set seq_view, 1
set transparency, 0.0
