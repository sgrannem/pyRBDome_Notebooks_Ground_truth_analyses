set grid_mode,1
set surface_quality, 0
load AARNA5386195W_y_BP.pdb
hide everything, AARNA5386195W_y_BP
show cartoon, AARNA5386195W_y_BP
color white, AARNA5386195W_y_BP
spectrum b, rainbow, AARNA5386195W_y_BP
set grid_slot, 1, AARNA5386195W_y_BP
center AARNA5386195W_y_BP
orient AARNA5386195W_y_BP
show surface, AARNA5386195W_y_BP
load AARNA5386195W_y_EC.pdb
hide everything, AARNA5386195W_y_EC
show cartoon, AARNA5386195W_y_EC
color white, AARNA5386195W_y_EC
spectrum b, rainbow, AARNA5386195W_y_EC
set grid_slot, 2, AARNA5386195W_y_EC
center AARNA5386195W_y_EC
orient AARNA5386195W_y_EC
show surface, AARNA5386195W_y_EC
load AARNA5386195W_y_LNMAX.pdb
hide everything, AARNA5386195W_y_LNMAX
show cartoon, AARNA5386195W_y_LNMAX
color white, AARNA5386195W_y_LNMAX
spectrum b, rainbow, AARNA5386195W_y_LNMAX
set grid_slot, 3, AARNA5386195W_y_LNMAX
center AARNA5386195W_y_LNMAX
orient AARNA5386195W_y_LNMAX
show surface, AARNA5386195W_y_LNMAX
load AARNA5386195W_y_LNMIN.pdb
hide everything, AARNA5386195W_y_LNMIN
show cartoon, AARNA5386195W_y_LNMIN
color white, AARNA5386195W_y_LNMIN
spectrum b, rainbow, AARNA5386195W_y_LNMIN
set grid_slot, 4, AARNA5386195W_y_LNMIN
center AARNA5386195W_y_LNMIN
orient AARNA5386195W_y_LNMIN
show surface, AARNA5386195W_y_LNMIN
set seq_view, 1
set transparency, 0.0
