set grid_mode,1
set surface_quality, 0
load AARNA2328807W_O_BP.pdb
hide everything, AARNA2328807W_O_BP
show cartoon, AARNA2328807W_O_BP
color white, AARNA2328807W_O_BP
spectrum b, rainbow, AARNA2328807W_O_BP
set grid_slot, 1, AARNA2328807W_O_BP
center AARNA2328807W_O_BP
orient AARNA2328807W_O_BP
show surface, AARNA2328807W_O_BP
load AARNA2328807W_O_EC.pdb
hide everything, AARNA2328807W_O_EC
show cartoon, AARNA2328807W_O_EC
color white, AARNA2328807W_O_EC
spectrum b, rainbow, AARNA2328807W_O_EC
set grid_slot, 2, AARNA2328807W_O_EC
center AARNA2328807W_O_EC
orient AARNA2328807W_O_EC
show surface, AARNA2328807W_O_EC
load AARNA2328807W_O_LNMAX.pdb
hide everything, AARNA2328807W_O_LNMAX
show cartoon, AARNA2328807W_O_LNMAX
color white, AARNA2328807W_O_LNMAX
spectrum b, rainbow, AARNA2328807W_O_LNMAX
set grid_slot, 3, AARNA2328807W_O_LNMAX
center AARNA2328807W_O_LNMAX
orient AARNA2328807W_O_LNMAX
show surface, AARNA2328807W_O_LNMAX
load AARNA2328807W_O_LNMIN.pdb
hide everything, AARNA2328807W_O_LNMIN
show cartoon, AARNA2328807W_O_LNMIN
color white, AARNA2328807W_O_LNMIN
spectrum b, rainbow, AARNA2328807W_O_LNMIN
set grid_slot, 4, AARNA2328807W_O_LNMIN
center AARNA2328807W_O_LNMIN
orient AARNA2328807W_O_LNMIN
show surface, AARNA2328807W_O_LNMIN
set seq_view, 1
set transparency, 0.0
