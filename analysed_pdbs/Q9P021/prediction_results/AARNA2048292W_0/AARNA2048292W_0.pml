set grid_mode,1
set surface_quality, 0
load AARNA2048292W_0_BP.pdb
hide everything, AARNA2048292W_0_BP
show cartoon, AARNA2048292W_0_BP
color white, AARNA2048292W_0_BP
spectrum b, rainbow, AARNA2048292W_0_BP
set grid_slot, 1, AARNA2048292W_0_BP
center AARNA2048292W_0_BP
orient AARNA2048292W_0_BP
show surface, AARNA2048292W_0_BP
load AARNA2048292W_0_EC.pdb
hide everything, AARNA2048292W_0_EC
show cartoon, AARNA2048292W_0_EC
color white, AARNA2048292W_0_EC
spectrum b, rainbow, AARNA2048292W_0_EC
set grid_slot, 2, AARNA2048292W_0_EC
center AARNA2048292W_0_EC
orient AARNA2048292W_0_EC
show surface, AARNA2048292W_0_EC
load AARNA2048292W_0_LNMAX.pdb
hide everything, AARNA2048292W_0_LNMAX
show cartoon, AARNA2048292W_0_LNMAX
color white, AARNA2048292W_0_LNMAX
spectrum b, rainbow, AARNA2048292W_0_LNMAX
set grid_slot, 3, AARNA2048292W_0_LNMAX
center AARNA2048292W_0_LNMAX
orient AARNA2048292W_0_LNMAX
show surface, AARNA2048292W_0_LNMAX
load AARNA2048292W_0_LNMIN.pdb
hide everything, AARNA2048292W_0_LNMIN
show cartoon, AARNA2048292W_0_LNMIN
color white, AARNA2048292W_0_LNMIN
spectrum b, rainbow, AARNA2048292W_0_LNMIN
set grid_slot, 4, AARNA2048292W_0_LNMIN
center AARNA2048292W_0_LNMIN
orient AARNA2048292W_0_LNMIN
show surface, AARNA2048292W_0_LNMIN
set seq_view, 1
set transparency, 0.0
