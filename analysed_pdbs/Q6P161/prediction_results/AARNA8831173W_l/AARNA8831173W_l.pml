set grid_mode,1
set surface_quality, 0
load AARNA8831173W_l_BP.pdb
hide everything, AARNA8831173W_l_BP
show cartoon, AARNA8831173W_l_BP
color white, AARNA8831173W_l_BP
spectrum b, rainbow, AARNA8831173W_l_BP
set grid_slot, 1, AARNA8831173W_l_BP
center AARNA8831173W_l_BP
orient AARNA8831173W_l_BP
show surface, AARNA8831173W_l_BP
load AARNA8831173W_l_EC.pdb
hide everything, AARNA8831173W_l_EC
show cartoon, AARNA8831173W_l_EC
color white, AARNA8831173W_l_EC
spectrum b, rainbow, AARNA8831173W_l_EC
set grid_slot, 2, AARNA8831173W_l_EC
center AARNA8831173W_l_EC
orient AARNA8831173W_l_EC
show surface, AARNA8831173W_l_EC
load AARNA8831173W_l_LNMAX.pdb
hide everything, AARNA8831173W_l_LNMAX
show cartoon, AARNA8831173W_l_LNMAX
color white, AARNA8831173W_l_LNMAX
spectrum b, rainbow, AARNA8831173W_l_LNMAX
set grid_slot, 3, AARNA8831173W_l_LNMAX
center AARNA8831173W_l_LNMAX
orient AARNA8831173W_l_LNMAX
show surface, AARNA8831173W_l_LNMAX
load AARNA8831173W_l_LNMIN.pdb
hide everything, AARNA8831173W_l_LNMIN
show cartoon, AARNA8831173W_l_LNMIN
color white, AARNA8831173W_l_LNMIN
spectrum b, rainbow, AARNA8831173W_l_LNMIN
set grid_slot, 4, AARNA8831173W_l_LNMIN
center AARNA8831173W_l_LNMIN
orient AARNA8831173W_l_LNMIN
show surface, AARNA8831173W_l_LNMIN
set seq_view, 1
set transparency, 0.0
