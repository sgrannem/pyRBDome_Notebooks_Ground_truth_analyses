set grid_mode,1
set surface_quality, 0
load AARNA1044423W_O_BP.pdb
hide everything, AARNA1044423W_O_BP
show cartoon, AARNA1044423W_O_BP
color white, AARNA1044423W_O_BP
spectrum b, rainbow, AARNA1044423W_O_BP
set grid_slot, 1, AARNA1044423W_O_BP
center AARNA1044423W_O_BP
orient AARNA1044423W_O_BP
show surface, AARNA1044423W_O_BP
load AARNA1044423W_O_EC.pdb
hide everything, AARNA1044423W_O_EC
show cartoon, AARNA1044423W_O_EC
color white, AARNA1044423W_O_EC
spectrum b, rainbow, AARNA1044423W_O_EC
set grid_slot, 2, AARNA1044423W_O_EC
center AARNA1044423W_O_EC
orient AARNA1044423W_O_EC
show surface, AARNA1044423W_O_EC
load AARNA1044423W_O_LNMAX.pdb
hide everything, AARNA1044423W_O_LNMAX
show cartoon, AARNA1044423W_O_LNMAX
color white, AARNA1044423W_O_LNMAX
spectrum b, rainbow, AARNA1044423W_O_LNMAX
set grid_slot, 3, AARNA1044423W_O_LNMAX
center AARNA1044423W_O_LNMAX
orient AARNA1044423W_O_LNMAX
show surface, AARNA1044423W_O_LNMAX
load AARNA1044423W_O_LNMIN.pdb
hide everything, AARNA1044423W_O_LNMIN
show cartoon, AARNA1044423W_O_LNMIN
color white, AARNA1044423W_O_LNMIN
spectrum b, rainbow, AARNA1044423W_O_LNMIN
set grid_slot, 4, AARNA1044423W_O_LNMIN
center AARNA1044423W_O_LNMIN
orient AARNA1044423W_O_LNMIN
show surface, AARNA1044423W_O_LNMIN
set seq_view, 1
set transparency, 0.0
