set grid_mode,1
set surface_quality, 0
load AARNA6676443W_n_BP.pdb
hide everything, AARNA6676443W_n_BP
show cartoon, AARNA6676443W_n_BP
color white, AARNA6676443W_n_BP
spectrum b, rainbow, AARNA6676443W_n_BP
set grid_slot, 1, AARNA6676443W_n_BP
center AARNA6676443W_n_BP
orient AARNA6676443W_n_BP
show surface, AARNA6676443W_n_BP
load AARNA6676443W_n_EC.pdb
hide everything, AARNA6676443W_n_EC
show cartoon, AARNA6676443W_n_EC
color white, AARNA6676443W_n_EC
spectrum b, rainbow, AARNA6676443W_n_EC
set grid_slot, 2, AARNA6676443W_n_EC
center AARNA6676443W_n_EC
orient AARNA6676443W_n_EC
show surface, AARNA6676443W_n_EC
load AARNA6676443W_n_LNMAX.pdb
hide everything, AARNA6676443W_n_LNMAX
show cartoon, AARNA6676443W_n_LNMAX
color white, AARNA6676443W_n_LNMAX
spectrum b, rainbow, AARNA6676443W_n_LNMAX
set grid_slot, 3, AARNA6676443W_n_LNMAX
center AARNA6676443W_n_LNMAX
orient AARNA6676443W_n_LNMAX
show surface, AARNA6676443W_n_LNMAX
load AARNA6676443W_n_LNMIN.pdb
hide everything, AARNA6676443W_n_LNMIN
show cartoon, AARNA6676443W_n_LNMIN
color white, AARNA6676443W_n_LNMIN
spectrum b, rainbow, AARNA6676443W_n_LNMIN
set grid_slot, 4, AARNA6676443W_n_LNMIN
center AARNA6676443W_n_LNMIN
orient AARNA6676443W_n_LNMIN
show surface, AARNA6676443W_n_LNMIN
set seq_view, 1
set transparency, 0.0
