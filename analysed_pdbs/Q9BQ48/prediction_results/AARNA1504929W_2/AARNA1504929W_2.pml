set grid_mode,1
set surface_quality, 0
load AARNA1504929W_2_BP.pdb
hide everything, AARNA1504929W_2_BP
show cartoon, AARNA1504929W_2_BP
color white, AARNA1504929W_2_BP
spectrum b, rainbow, AARNA1504929W_2_BP
set grid_slot, 1, AARNA1504929W_2_BP
center AARNA1504929W_2_BP
orient AARNA1504929W_2_BP
show surface, AARNA1504929W_2_BP
load AARNA1504929W_2_EC.pdb
hide everything, AARNA1504929W_2_EC
show cartoon, AARNA1504929W_2_EC
color white, AARNA1504929W_2_EC
spectrum b, rainbow, AARNA1504929W_2_EC
set grid_slot, 2, AARNA1504929W_2_EC
center AARNA1504929W_2_EC
orient AARNA1504929W_2_EC
show surface, AARNA1504929W_2_EC
load AARNA1504929W_2_LNMAX.pdb
hide everything, AARNA1504929W_2_LNMAX
show cartoon, AARNA1504929W_2_LNMAX
color white, AARNA1504929W_2_LNMAX
spectrum b, rainbow, AARNA1504929W_2_LNMAX
set grid_slot, 3, AARNA1504929W_2_LNMAX
center AARNA1504929W_2_LNMAX
orient AARNA1504929W_2_LNMAX
show surface, AARNA1504929W_2_LNMAX
load AARNA1504929W_2_LNMIN.pdb
hide everything, AARNA1504929W_2_LNMIN
show cartoon, AARNA1504929W_2_LNMIN
color white, AARNA1504929W_2_LNMIN
spectrum b, rainbow, AARNA1504929W_2_LNMIN
set grid_slot, 4, AARNA1504929W_2_LNMIN
center AARNA1504929W_2_LNMIN
orient AARNA1504929W_2_LNMIN
show surface, AARNA1504929W_2_LNMIN
set seq_view, 1
set transparency, 0.0
