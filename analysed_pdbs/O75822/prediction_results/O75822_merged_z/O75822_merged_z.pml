set grid_mode,1
set surface_quality, 0
load AARNA868994W_z_BP.pdb
hide everything, AARNA868994W_z_BP
show cartoon, AARNA868994W_z_BP
color white, AARNA868994W_z_BP
spectrum b, rainbow, AARNA868994W_z_BP
set grid_slot, 1, AARNA868994W_z_BP
center AARNA868994W_z_BP
orient AARNA868994W_z_BP
show surface, AARNA868994W_z_BP
load AARNA868994W_z_EC.pdb
hide everything, AARNA868994W_z_EC
show cartoon, AARNA868994W_z_EC
color white, AARNA868994W_z_EC
spectrum b, rainbow, AARNA868994W_z_EC
set grid_slot, 2, AARNA868994W_z_EC
center AARNA868994W_z_EC
orient AARNA868994W_z_EC
show surface, AARNA868994W_z_EC
load AARNA868994W_z_LNMAX.pdb
hide everything, AARNA868994W_z_LNMAX
show cartoon, AARNA868994W_z_LNMAX
color white, AARNA868994W_z_LNMAX
spectrum b, rainbow, AARNA868994W_z_LNMAX
set grid_slot, 3, AARNA868994W_z_LNMAX
center AARNA868994W_z_LNMAX
orient AARNA868994W_z_LNMAX
show surface, AARNA868994W_z_LNMAX
load AARNA868994W_z_LNMIN.pdb
hide everything, AARNA868994W_z_LNMIN
show cartoon, AARNA868994W_z_LNMIN
color white, AARNA868994W_z_LNMIN
spectrum b, rainbow, AARNA868994W_z_LNMIN
set grid_slot, 4, AARNA868994W_z_LNMIN
center AARNA868994W_z_LNMIN
orient AARNA868994W_z_LNMIN
show surface, AARNA868994W_z_LNMIN
set seq_view, 1
set transparency, 0.0
