set grid_mode,1
set surface_quality, 0
load AARNA9576949W_N_BP.pdb
hide everything, AARNA9576949W_N_BP
show cartoon, AARNA9576949W_N_BP
color white, AARNA9576949W_N_BP
spectrum b, rainbow, AARNA9576949W_N_BP
set grid_slot, 1, AARNA9576949W_N_BP
center AARNA9576949W_N_BP
orient AARNA9576949W_N_BP
show surface, AARNA9576949W_N_BP
load AARNA9576949W_N_EC.pdb
hide everything, AARNA9576949W_N_EC
show cartoon, AARNA9576949W_N_EC
color white, AARNA9576949W_N_EC
spectrum b, rainbow, AARNA9576949W_N_EC
set grid_slot, 2, AARNA9576949W_N_EC
center AARNA9576949W_N_EC
orient AARNA9576949W_N_EC
show surface, AARNA9576949W_N_EC
load AARNA9576949W_N_LNMAX.pdb
hide everything, AARNA9576949W_N_LNMAX
show cartoon, AARNA9576949W_N_LNMAX
color white, AARNA9576949W_N_LNMAX
spectrum b, rainbow, AARNA9576949W_N_LNMAX
set grid_slot, 3, AARNA9576949W_N_LNMAX
center AARNA9576949W_N_LNMAX
orient AARNA9576949W_N_LNMAX
show surface, AARNA9576949W_N_LNMAX
load AARNA9576949W_N_LNMIN.pdb
hide everything, AARNA9576949W_N_LNMIN
show cartoon, AARNA9576949W_N_LNMIN
color white, AARNA9576949W_N_LNMIN
spectrum b, rainbow, AARNA9576949W_N_LNMIN
set grid_slot, 4, AARNA9576949W_N_LNMIN
center AARNA9576949W_N_LNMIN
orient AARNA9576949W_N_LNMIN
show surface, AARNA9576949W_N_LNMIN
set seq_view, 1
set transparency, 0.0
