set grid_mode,1
set surface_quality, 0
load AARNA17675W_G_BP.pdb
hide everything, AARNA17675W_G_BP
show cartoon, AARNA17675W_G_BP
color white, AARNA17675W_G_BP
spectrum b, rainbow, AARNA17675W_G_BP
set grid_slot, 1, AARNA17675W_G_BP
center AARNA17675W_G_BP
orient AARNA17675W_G_BP
show surface, AARNA17675W_G_BP
load AARNA17675W_G_EC.pdb
hide everything, AARNA17675W_G_EC
show cartoon, AARNA17675W_G_EC
color white, AARNA17675W_G_EC
spectrum b, rainbow, AARNA17675W_G_EC
set grid_slot, 2, AARNA17675W_G_EC
center AARNA17675W_G_EC
orient AARNA17675W_G_EC
show surface, AARNA17675W_G_EC
load AARNA17675W_G_LNMAX.pdb
hide everything, AARNA17675W_G_LNMAX
show cartoon, AARNA17675W_G_LNMAX
color white, AARNA17675W_G_LNMAX
spectrum b, rainbow, AARNA17675W_G_LNMAX
set grid_slot, 3, AARNA17675W_G_LNMAX
center AARNA17675W_G_LNMAX
orient AARNA17675W_G_LNMAX
show surface, AARNA17675W_G_LNMAX
load AARNA17675W_G_LNMIN.pdb
hide everything, AARNA17675W_G_LNMIN
show cartoon, AARNA17675W_G_LNMIN
color white, AARNA17675W_G_LNMIN
spectrum b, rainbow, AARNA17675W_G_LNMIN
set grid_slot, 4, AARNA17675W_G_LNMIN
center AARNA17675W_G_LNMIN
orient AARNA17675W_G_LNMIN
show surface, AARNA17675W_G_LNMIN
set seq_view, 1
set transparency, 0.0
