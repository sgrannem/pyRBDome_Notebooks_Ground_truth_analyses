set grid_mode,1
set surface_quality, 0
load AARNA483688W_A_BP.pdb
hide everything, AARNA483688W_A_BP
show cartoon, AARNA483688W_A_BP
color white, AARNA483688W_A_BP
spectrum b, rainbow, AARNA483688W_A_BP
set grid_slot, 1, AARNA483688W_A_BP
center AARNA483688W_A_BP
orient AARNA483688W_A_BP
show surface, AARNA483688W_A_BP
load AARNA483688W_A_EC.pdb
hide everything, AARNA483688W_A_EC
show cartoon, AARNA483688W_A_EC
color white, AARNA483688W_A_EC
spectrum b, rainbow, AARNA483688W_A_EC
set grid_slot, 2, AARNA483688W_A_EC
center AARNA483688W_A_EC
orient AARNA483688W_A_EC
show surface, AARNA483688W_A_EC
load AARNA483688W_A_LNMAX.pdb
hide everything, AARNA483688W_A_LNMAX
show cartoon, AARNA483688W_A_LNMAX
color white, AARNA483688W_A_LNMAX
spectrum b, rainbow, AARNA483688W_A_LNMAX
set grid_slot, 3, AARNA483688W_A_LNMAX
center AARNA483688W_A_LNMAX
orient AARNA483688W_A_LNMAX
show surface, AARNA483688W_A_LNMAX
load AARNA483688W_A_LNMIN.pdb
hide everything, AARNA483688W_A_LNMIN
show cartoon, AARNA483688W_A_LNMIN
color white, AARNA483688W_A_LNMIN
spectrum b, rainbow, AARNA483688W_A_LNMIN
set grid_slot, 4, AARNA483688W_A_LNMIN
center AARNA483688W_A_LNMIN
orient AARNA483688W_A_LNMIN
show surface, AARNA483688W_A_LNMIN
set seq_view, 1
set transparency, 0.0
