set grid_mode,1
set surface_quality, 0
load AARNA4884048W_2_BP.pdb
hide everything, AARNA4884048W_2_BP
show cartoon, AARNA4884048W_2_BP
color white, AARNA4884048W_2_BP
spectrum b, rainbow, AARNA4884048W_2_BP
set grid_slot, 1, AARNA4884048W_2_BP
center AARNA4884048W_2_BP
orient AARNA4884048W_2_BP
show surface, AARNA4884048W_2_BP
load AARNA4884048W_2_EC.pdb
hide everything, AARNA4884048W_2_EC
show cartoon, AARNA4884048W_2_EC
color white, AARNA4884048W_2_EC
spectrum b, rainbow, AARNA4884048W_2_EC
set grid_slot, 2, AARNA4884048W_2_EC
center AARNA4884048W_2_EC
orient AARNA4884048W_2_EC
show surface, AARNA4884048W_2_EC
load AARNA4884048W_2_LNMAX.pdb
hide everything, AARNA4884048W_2_LNMAX
show cartoon, AARNA4884048W_2_LNMAX
color white, AARNA4884048W_2_LNMAX
spectrum b, rainbow, AARNA4884048W_2_LNMAX
set grid_slot, 3, AARNA4884048W_2_LNMAX
center AARNA4884048W_2_LNMAX
orient AARNA4884048W_2_LNMAX
show surface, AARNA4884048W_2_LNMAX
load AARNA4884048W_2_LNMIN.pdb
hide everything, AARNA4884048W_2_LNMIN
show cartoon, AARNA4884048W_2_LNMIN
color white, AARNA4884048W_2_LNMIN
spectrum b, rainbow, AARNA4884048W_2_LNMIN
set grid_slot, 4, AARNA4884048W_2_LNMIN
center AARNA4884048W_2_LNMIN
orient AARNA4884048W_2_LNMIN
show surface, AARNA4884048W_2_LNMIN
set seq_view, 1
set transparency, 0.0
