set grid_mode,1
set surface_quality, 0
load AARNA4744741W_X_BP.pdb
hide everything, AARNA4744741W_X_BP
show cartoon, AARNA4744741W_X_BP
color white, AARNA4744741W_X_BP
spectrum b, rainbow, AARNA4744741W_X_BP
set grid_slot, 1, AARNA4744741W_X_BP
center AARNA4744741W_X_BP
orient AARNA4744741W_X_BP
show surface, AARNA4744741W_X_BP
load AARNA4744741W_X_EC.pdb
hide everything, AARNA4744741W_X_EC
show cartoon, AARNA4744741W_X_EC
color white, AARNA4744741W_X_EC
spectrum b, rainbow, AARNA4744741W_X_EC
set grid_slot, 2, AARNA4744741W_X_EC
center AARNA4744741W_X_EC
orient AARNA4744741W_X_EC
show surface, AARNA4744741W_X_EC
load AARNA4744741W_X_LNMAX.pdb
hide everything, AARNA4744741W_X_LNMAX
show cartoon, AARNA4744741W_X_LNMAX
color white, AARNA4744741W_X_LNMAX
spectrum b, rainbow, AARNA4744741W_X_LNMAX
set grid_slot, 3, AARNA4744741W_X_LNMAX
center AARNA4744741W_X_LNMAX
orient AARNA4744741W_X_LNMAX
show surface, AARNA4744741W_X_LNMAX
load AARNA4744741W_X_LNMIN.pdb
hide everything, AARNA4744741W_X_LNMIN
show cartoon, AARNA4744741W_X_LNMIN
color white, AARNA4744741W_X_LNMIN
spectrum b, rainbow, AARNA4744741W_X_LNMIN
set grid_slot, 4, AARNA4744741W_X_LNMIN
center AARNA4744741W_X_LNMIN
orient AARNA4744741W_X_LNMIN
show surface, AARNA4744741W_X_LNMIN
set seq_view, 1
set transparency, 0.0
