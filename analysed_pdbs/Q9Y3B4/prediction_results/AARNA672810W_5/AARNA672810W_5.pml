set grid_mode,1
set surface_quality, 0
load AARNA672810W_5_BP.pdb
hide everything, AARNA672810W_5_BP
show cartoon, AARNA672810W_5_BP
color white, AARNA672810W_5_BP
spectrum b, rainbow, AARNA672810W_5_BP
set grid_slot, 1, AARNA672810W_5_BP
center AARNA672810W_5_BP
orient AARNA672810W_5_BP
show surface, AARNA672810W_5_BP
load AARNA672810W_5_EC.pdb
hide everything, AARNA672810W_5_EC
show cartoon, AARNA672810W_5_EC
color white, AARNA672810W_5_EC
spectrum b, rainbow, AARNA672810W_5_EC
set grid_slot, 2, AARNA672810W_5_EC
center AARNA672810W_5_EC
orient AARNA672810W_5_EC
show surface, AARNA672810W_5_EC
load AARNA672810W_5_LNMAX.pdb
hide everything, AARNA672810W_5_LNMAX
show cartoon, AARNA672810W_5_LNMAX
color white, AARNA672810W_5_LNMAX
spectrum b, rainbow, AARNA672810W_5_LNMAX
set grid_slot, 3, AARNA672810W_5_LNMAX
center AARNA672810W_5_LNMAX
orient AARNA672810W_5_LNMAX
show surface, AARNA672810W_5_LNMAX
load AARNA672810W_5_LNMIN.pdb
hide everything, AARNA672810W_5_LNMIN
show cartoon, AARNA672810W_5_LNMIN
color white, AARNA672810W_5_LNMIN
spectrum b, rainbow, AARNA672810W_5_LNMIN
set grid_slot, 4, AARNA672810W_5_LNMIN
center AARNA672810W_5_LNMIN
orient AARNA672810W_5_LNMIN
show surface, AARNA672810W_5_LNMIN
set seq_view, 1
set transparency, 0.0
