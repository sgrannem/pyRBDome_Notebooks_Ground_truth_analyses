set grid_mode,1
set surface_quality, 0
load AARNA5012480W_j_BP.pdb
hide everything, AARNA5012480W_j_BP
show cartoon, AARNA5012480W_j_BP
color white, AARNA5012480W_j_BP
spectrum b, rainbow, AARNA5012480W_j_BP
set grid_slot, 1, AARNA5012480W_j_BP
center AARNA5012480W_j_BP
orient AARNA5012480W_j_BP
show surface, AARNA5012480W_j_BP
load AARNA5012480W_j_EC.pdb
hide everything, AARNA5012480W_j_EC
show cartoon, AARNA5012480W_j_EC
color white, AARNA5012480W_j_EC
spectrum b, rainbow, AARNA5012480W_j_EC
set grid_slot, 2, AARNA5012480W_j_EC
center AARNA5012480W_j_EC
orient AARNA5012480W_j_EC
show surface, AARNA5012480W_j_EC
load AARNA5012480W_j_LNMAX.pdb
hide everything, AARNA5012480W_j_LNMAX
show cartoon, AARNA5012480W_j_LNMAX
color white, AARNA5012480W_j_LNMAX
spectrum b, rainbow, AARNA5012480W_j_LNMAX
set grid_slot, 3, AARNA5012480W_j_LNMAX
center AARNA5012480W_j_LNMAX
orient AARNA5012480W_j_LNMAX
show surface, AARNA5012480W_j_LNMAX
load AARNA5012480W_j_LNMIN.pdb
hide everything, AARNA5012480W_j_LNMIN
show cartoon, AARNA5012480W_j_LNMIN
color white, AARNA5012480W_j_LNMIN
spectrum b, rainbow, AARNA5012480W_j_LNMIN
set grid_slot, 4, AARNA5012480W_j_LNMIN
center AARNA5012480W_j_LNMIN
orient AARNA5012480W_j_LNMIN
show surface, AARNA5012480W_j_LNMIN
set seq_view, 1
set transparency, 0.0
