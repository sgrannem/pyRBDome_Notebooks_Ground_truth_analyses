RESNR	RESTYPE	RESCHAIN	PROT_IDX_LIST	RESNR_LIG	RESTYPE_LIG	RESCHAIN_LIG	DIST	PROTISPOS	LIG_GROUP	LIG_IDX_LIST	LIGCOO	PROTCOO
49	ASP	C	1958,1959	3	G	D	3.43	False	Guanidine	3176,3178,3179	(61.907, -3.864, 52.652)	(63.701499999999996, -1.209, 51.4345)
18	GLU	C	1692,1693	4	G	D	3.57	False	Guanidine	3199,3201,3202	(62.163, 0.538, 54.579)	(61.5035, 3.7824999999999998, 53.2505)
49	ASP	C	1958,1959	4	G	D	3.91	False	Guanidine	3199,3201,3202	(62.163, 0.538, 54.579)	(63.701499999999996, -1.209, 51.4345)
