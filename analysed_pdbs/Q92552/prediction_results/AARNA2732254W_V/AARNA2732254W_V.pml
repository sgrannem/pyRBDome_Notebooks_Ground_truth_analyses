set grid_mode,1
set surface_quality, 0
load AARNA2732254W_V_BP.pdb
hide everything, AARNA2732254W_V_BP
show cartoon, AARNA2732254W_V_BP
color white, AARNA2732254W_V_BP
spectrum b, rainbow, AARNA2732254W_V_BP
set grid_slot, 1, AARNA2732254W_V_BP
center AARNA2732254W_V_BP
orient AARNA2732254W_V_BP
show surface, AARNA2732254W_V_BP
load AARNA2732254W_V_EC.pdb
hide everything, AARNA2732254W_V_EC
show cartoon, AARNA2732254W_V_EC
color white, AARNA2732254W_V_EC
spectrum b, rainbow, AARNA2732254W_V_EC
set grid_slot, 2, AARNA2732254W_V_EC
center AARNA2732254W_V_EC
orient AARNA2732254W_V_EC
show surface, AARNA2732254W_V_EC
load AARNA2732254W_V_LNMAX.pdb
hide everything, AARNA2732254W_V_LNMAX
show cartoon, AARNA2732254W_V_LNMAX
color white, AARNA2732254W_V_LNMAX
spectrum b, rainbow, AARNA2732254W_V_LNMAX
set grid_slot, 3, AARNA2732254W_V_LNMAX
center AARNA2732254W_V_LNMAX
orient AARNA2732254W_V_LNMAX
show surface, AARNA2732254W_V_LNMAX
load AARNA2732254W_V_LNMIN.pdb
hide everything, AARNA2732254W_V_LNMIN
show cartoon, AARNA2732254W_V_LNMIN
color white, AARNA2732254W_V_LNMIN
spectrum b, rainbow, AARNA2732254W_V_LNMIN
set grid_slot, 4, AARNA2732254W_V_LNMIN
center AARNA2732254W_V_LNMIN
orient AARNA2732254W_V_LNMIN
show surface, AARNA2732254W_V_LNMIN
set seq_view, 1
set transparency, 0.0
