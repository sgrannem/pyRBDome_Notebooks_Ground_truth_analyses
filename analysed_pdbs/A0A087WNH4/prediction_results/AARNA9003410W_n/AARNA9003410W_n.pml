set grid_mode,1
set surface_quality, 0
load AARNA9003410W_n_BP.pdb
hide everything, AARNA9003410W_n_BP
show cartoon, AARNA9003410W_n_BP
color white, AARNA9003410W_n_BP
spectrum b, rainbow, AARNA9003410W_n_BP
set grid_slot, 1, AARNA9003410W_n_BP
center AARNA9003410W_n_BP
orient AARNA9003410W_n_BP
show surface, AARNA9003410W_n_BP
load AARNA9003410W_n_EC.pdb
hide everything, AARNA9003410W_n_EC
show cartoon, AARNA9003410W_n_EC
color white, AARNA9003410W_n_EC
spectrum b, rainbow, AARNA9003410W_n_EC
set grid_slot, 2, AARNA9003410W_n_EC
center AARNA9003410W_n_EC
orient AARNA9003410W_n_EC
show surface, AARNA9003410W_n_EC
load AARNA9003410W_n_LNMAX.pdb
hide everything, AARNA9003410W_n_LNMAX
show cartoon, AARNA9003410W_n_LNMAX
color white, AARNA9003410W_n_LNMAX
spectrum b, rainbow, AARNA9003410W_n_LNMAX
set grid_slot, 3, AARNA9003410W_n_LNMAX
center AARNA9003410W_n_LNMAX
orient AARNA9003410W_n_LNMAX
show surface, AARNA9003410W_n_LNMAX
load AARNA9003410W_n_LNMIN.pdb
hide everything, AARNA9003410W_n_LNMIN
show cartoon, AARNA9003410W_n_LNMIN
color white, AARNA9003410W_n_LNMIN
spectrum b, rainbow, AARNA9003410W_n_LNMIN
set grid_slot, 4, AARNA9003410W_n_LNMIN
center AARNA9003410W_n_LNMIN
orient AARNA9003410W_n_LNMIN
show surface, AARNA9003410W_n_LNMIN
set seq_view, 1
set transparency, 0.0
