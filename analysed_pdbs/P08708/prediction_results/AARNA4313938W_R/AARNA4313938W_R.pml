set grid_mode,1
set surface_quality, 0
load AARNA4313938W_R_BP.pdb
hide everything, AARNA4313938W_R_BP
show cartoon, AARNA4313938W_R_BP
color white, AARNA4313938W_R_BP
spectrum b, rainbow, AARNA4313938W_R_BP
set grid_slot, 1, AARNA4313938W_R_BP
center AARNA4313938W_R_BP
orient AARNA4313938W_R_BP
show surface, AARNA4313938W_R_BP
load AARNA4313938W_R_EC.pdb
hide everything, AARNA4313938W_R_EC
show cartoon, AARNA4313938W_R_EC
color white, AARNA4313938W_R_EC
spectrum b, rainbow, AARNA4313938W_R_EC
set grid_slot, 2, AARNA4313938W_R_EC
center AARNA4313938W_R_EC
orient AARNA4313938W_R_EC
show surface, AARNA4313938W_R_EC
load AARNA4313938W_R_LNMAX.pdb
hide everything, AARNA4313938W_R_LNMAX
show cartoon, AARNA4313938W_R_LNMAX
color white, AARNA4313938W_R_LNMAX
spectrum b, rainbow, AARNA4313938W_R_LNMAX
set grid_slot, 3, AARNA4313938W_R_LNMAX
center AARNA4313938W_R_LNMAX
orient AARNA4313938W_R_LNMAX
show surface, AARNA4313938W_R_LNMAX
load AARNA4313938W_R_LNMIN.pdb
hide everything, AARNA4313938W_R_LNMIN
show cartoon, AARNA4313938W_R_LNMIN
color white, AARNA4313938W_R_LNMIN
spectrum b, rainbow, AARNA4313938W_R_LNMIN
set grid_slot, 4, AARNA4313938W_R_LNMIN
center AARNA4313938W_R_LNMIN
orient AARNA4313938W_R_LNMIN
show surface, AARNA4313938W_R_LNMIN
set seq_view, 1
set transparency, 0.0
