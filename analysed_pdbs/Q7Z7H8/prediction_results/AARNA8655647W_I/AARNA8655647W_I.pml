set grid_mode,1
set surface_quality, 0
load AARNA8655647W_I_BP.pdb
hide everything, AARNA8655647W_I_BP
show cartoon, AARNA8655647W_I_BP
color white, AARNA8655647W_I_BP
spectrum b, rainbow, AARNA8655647W_I_BP
set grid_slot, 1, AARNA8655647W_I_BP
center AARNA8655647W_I_BP
orient AARNA8655647W_I_BP
show surface, AARNA8655647W_I_BP
load AARNA8655647W_I_EC.pdb
hide everything, AARNA8655647W_I_EC
show cartoon, AARNA8655647W_I_EC
color white, AARNA8655647W_I_EC
spectrum b, rainbow, AARNA8655647W_I_EC
set grid_slot, 2, AARNA8655647W_I_EC
center AARNA8655647W_I_EC
orient AARNA8655647W_I_EC
show surface, AARNA8655647W_I_EC
load AARNA8655647W_I_LNMAX.pdb
hide everything, AARNA8655647W_I_LNMAX
show cartoon, AARNA8655647W_I_LNMAX
color white, AARNA8655647W_I_LNMAX
spectrum b, rainbow, AARNA8655647W_I_LNMAX
set grid_slot, 3, AARNA8655647W_I_LNMAX
center AARNA8655647W_I_LNMAX
orient AARNA8655647W_I_LNMAX
show surface, AARNA8655647W_I_LNMAX
load AARNA8655647W_I_LNMIN.pdb
hide everything, AARNA8655647W_I_LNMIN
show cartoon, AARNA8655647W_I_LNMIN
color white, AARNA8655647W_I_LNMIN
spectrum b, rainbow, AARNA8655647W_I_LNMIN
set grid_slot, 4, AARNA8655647W_I_LNMIN
center AARNA8655647W_I_LNMIN
orient AARNA8655647W_I_LNMIN
show surface, AARNA8655647W_I_LNMIN
set seq_view, 1
set transparency, 0.0
