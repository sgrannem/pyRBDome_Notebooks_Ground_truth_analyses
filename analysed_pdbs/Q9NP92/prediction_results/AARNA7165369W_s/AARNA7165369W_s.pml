set grid_mode,1
set surface_quality, 0
load AARNA7165369W_s_BP.pdb
hide everything, AARNA7165369W_s_BP
show cartoon, AARNA7165369W_s_BP
color white, AARNA7165369W_s_BP
spectrum b, rainbow, AARNA7165369W_s_BP
set grid_slot, 1, AARNA7165369W_s_BP
center AARNA7165369W_s_BP
orient AARNA7165369W_s_BP
show surface, AARNA7165369W_s_BP
load AARNA7165369W_s_EC.pdb
hide everything, AARNA7165369W_s_EC
show cartoon, AARNA7165369W_s_EC
color white, AARNA7165369W_s_EC
spectrum b, rainbow, AARNA7165369W_s_EC
set grid_slot, 2, AARNA7165369W_s_EC
center AARNA7165369W_s_EC
orient AARNA7165369W_s_EC
show surface, AARNA7165369W_s_EC
load AARNA7165369W_s_LNMAX.pdb
hide everything, AARNA7165369W_s_LNMAX
show cartoon, AARNA7165369W_s_LNMAX
color white, AARNA7165369W_s_LNMAX
spectrum b, rainbow, AARNA7165369W_s_LNMAX
set grid_slot, 3, AARNA7165369W_s_LNMAX
center AARNA7165369W_s_LNMAX
orient AARNA7165369W_s_LNMAX
show surface, AARNA7165369W_s_LNMAX
load AARNA7165369W_s_LNMIN.pdb
hide everything, AARNA7165369W_s_LNMIN
show cartoon, AARNA7165369W_s_LNMIN
color white, AARNA7165369W_s_LNMIN
spectrum b, rainbow, AARNA7165369W_s_LNMIN
set grid_slot, 4, AARNA7165369W_s_LNMIN
center AARNA7165369W_s_LNMIN
orient AARNA7165369W_s_LNMIN
show surface, AARNA7165369W_s_LNMIN
set seq_view, 1
set transparency, 0.0
