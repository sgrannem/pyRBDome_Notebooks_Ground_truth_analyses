set grid_mode,1
set surface_quality, 0
load AARNA2326464W_L_BP.pdb
hide everything, AARNA2326464W_L_BP
show cartoon, AARNA2326464W_L_BP
color white, AARNA2326464W_L_BP
spectrum b, rainbow, AARNA2326464W_L_BP
set grid_slot, 1, AARNA2326464W_L_BP
center AARNA2326464W_L_BP
orient AARNA2326464W_L_BP
show surface, AARNA2326464W_L_BP
load AARNA2326464W_L_EC.pdb
hide everything, AARNA2326464W_L_EC
show cartoon, AARNA2326464W_L_EC
color white, AARNA2326464W_L_EC
spectrum b, rainbow, AARNA2326464W_L_EC
set grid_slot, 2, AARNA2326464W_L_EC
center AARNA2326464W_L_EC
orient AARNA2326464W_L_EC
show surface, AARNA2326464W_L_EC
load AARNA2326464W_L_LNMAX.pdb
hide everything, AARNA2326464W_L_LNMAX
show cartoon, AARNA2326464W_L_LNMAX
color white, AARNA2326464W_L_LNMAX
spectrum b, rainbow, AARNA2326464W_L_LNMAX
set grid_slot, 3, AARNA2326464W_L_LNMAX
center AARNA2326464W_L_LNMAX
orient AARNA2326464W_L_LNMAX
show surface, AARNA2326464W_L_LNMAX
load AARNA2326464W_L_LNMIN.pdb
hide everything, AARNA2326464W_L_LNMIN
show cartoon, AARNA2326464W_L_LNMIN
color white, AARNA2326464W_L_LNMIN
spectrum b, rainbow, AARNA2326464W_L_LNMIN
set grid_slot, 4, AARNA2326464W_L_LNMIN
center AARNA2326464W_L_LNMIN
orient AARNA2326464W_L_LNMIN
show surface, AARNA2326464W_L_LNMIN
set seq_view, 1
set transparency, 0.0
