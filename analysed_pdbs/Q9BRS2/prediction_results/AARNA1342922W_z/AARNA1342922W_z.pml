set grid_mode,1
set surface_quality, 0
load AARNA1342922W_z_BP.pdb
hide everything, AARNA1342922W_z_BP
show cartoon, AARNA1342922W_z_BP
color white, AARNA1342922W_z_BP
spectrum b, rainbow, AARNA1342922W_z_BP
set grid_slot, 1, AARNA1342922W_z_BP
center AARNA1342922W_z_BP
orient AARNA1342922W_z_BP
show surface, AARNA1342922W_z_BP
load AARNA1342922W_z_EC.pdb
hide everything, AARNA1342922W_z_EC
show cartoon, AARNA1342922W_z_EC
color white, AARNA1342922W_z_EC
spectrum b, rainbow, AARNA1342922W_z_EC
set grid_slot, 2, AARNA1342922W_z_EC
center AARNA1342922W_z_EC
orient AARNA1342922W_z_EC
show surface, AARNA1342922W_z_EC
load AARNA1342922W_z_LNMAX.pdb
hide everything, AARNA1342922W_z_LNMAX
show cartoon, AARNA1342922W_z_LNMAX
color white, AARNA1342922W_z_LNMAX
spectrum b, rainbow, AARNA1342922W_z_LNMAX
set grid_slot, 3, AARNA1342922W_z_LNMAX
center AARNA1342922W_z_LNMAX
orient AARNA1342922W_z_LNMAX
show surface, AARNA1342922W_z_LNMAX
load AARNA1342922W_z_LNMIN.pdb
hide everything, AARNA1342922W_z_LNMIN
show cartoon, AARNA1342922W_z_LNMIN
color white, AARNA1342922W_z_LNMIN
spectrum b, rainbow, AARNA1342922W_z_LNMIN
set grid_slot, 4, AARNA1342922W_z_LNMIN
center AARNA1342922W_z_LNMIN
orient AARNA1342922W_z_LNMIN
show surface, AARNA1342922W_z_LNMIN
set seq_view, 1
set transparency, 0.0
