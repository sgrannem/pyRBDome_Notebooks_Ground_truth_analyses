set grid_mode,1
set surface_quality, 0
load AARNA886220W_h_BP.pdb
hide everything, AARNA886220W_h_BP
show cartoon, AARNA886220W_h_BP
color white, AARNA886220W_h_BP
spectrum b, rainbow, AARNA886220W_h_BP
set grid_slot, 1, AARNA886220W_h_BP
center AARNA886220W_h_BP
orient AARNA886220W_h_BP
show surface, AARNA886220W_h_BP
load AARNA886220W_h_EC.pdb
hide everything, AARNA886220W_h_EC
show cartoon, AARNA886220W_h_EC
color white, AARNA886220W_h_EC
spectrum b, rainbow, AARNA886220W_h_EC
set grid_slot, 2, AARNA886220W_h_EC
center AARNA886220W_h_EC
orient AARNA886220W_h_EC
show surface, AARNA886220W_h_EC
load AARNA886220W_h_LNMAX.pdb
hide everything, AARNA886220W_h_LNMAX
show cartoon, AARNA886220W_h_LNMAX
color white, AARNA886220W_h_LNMAX
spectrum b, rainbow, AARNA886220W_h_LNMAX
set grid_slot, 3, AARNA886220W_h_LNMAX
center AARNA886220W_h_LNMAX
orient AARNA886220W_h_LNMAX
show surface, AARNA886220W_h_LNMAX
load AARNA886220W_h_LNMIN.pdb
hide everything, AARNA886220W_h_LNMIN
show cartoon, AARNA886220W_h_LNMIN
color white, AARNA886220W_h_LNMIN
spectrum b, rainbow, AARNA886220W_h_LNMIN
set grid_slot, 4, AARNA886220W_h_LNMIN
center AARNA886220W_h_LNMIN
orient AARNA886220W_h_LNMIN
show surface, AARNA886220W_h_LNMIN
set seq_view, 1
set transparency, 0.0
