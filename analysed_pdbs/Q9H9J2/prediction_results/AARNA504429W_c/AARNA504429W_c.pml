set grid_mode,1
set surface_quality, 0
load AARNA504429W_c_BP.pdb
hide everything, AARNA504429W_c_BP
show cartoon, AARNA504429W_c_BP
color white, AARNA504429W_c_BP
spectrum b, rainbow, AARNA504429W_c_BP
set grid_slot, 1, AARNA504429W_c_BP
center AARNA504429W_c_BP
orient AARNA504429W_c_BP
show surface, AARNA504429W_c_BP
load AARNA504429W_c_EC.pdb
hide everything, AARNA504429W_c_EC
show cartoon, AARNA504429W_c_EC
color white, AARNA504429W_c_EC
spectrum b, rainbow, AARNA504429W_c_EC
set grid_slot, 2, AARNA504429W_c_EC
center AARNA504429W_c_EC
orient AARNA504429W_c_EC
show surface, AARNA504429W_c_EC
load AARNA504429W_c_LNMAX.pdb
hide everything, AARNA504429W_c_LNMAX
show cartoon, AARNA504429W_c_LNMAX
color white, AARNA504429W_c_LNMAX
spectrum b, rainbow, AARNA504429W_c_LNMAX
set grid_slot, 3, AARNA504429W_c_LNMAX
center AARNA504429W_c_LNMAX
orient AARNA504429W_c_LNMAX
show surface, AARNA504429W_c_LNMAX
load AARNA504429W_c_LNMIN.pdb
hide everything, AARNA504429W_c_LNMIN
show cartoon, AARNA504429W_c_LNMIN
color white, AARNA504429W_c_LNMIN
spectrum b, rainbow, AARNA504429W_c_LNMIN
set grid_slot, 4, AARNA504429W_c_LNMIN
center AARNA504429W_c_LNMIN
orient AARNA504429W_c_LNMIN
show surface, AARNA504429W_c_LNMIN
set seq_view, 1
set transparency, 0.0
