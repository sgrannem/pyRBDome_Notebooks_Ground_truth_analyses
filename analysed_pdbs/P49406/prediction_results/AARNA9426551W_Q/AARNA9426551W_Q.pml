set grid_mode,1
set surface_quality, 0
load AARNA9426551W_Q_BP.pdb
hide everything, AARNA9426551W_Q_BP
show cartoon, AARNA9426551W_Q_BP
color white, AARNA9426551W_Q_BP
spectrum b, rainbow, AARNA9426551W_Q_BP
set grid_slot, 1, AARNA9426551W_Q_BP
center AARNA9426551W_Q_BP
orient AARNA9426551W_Q_BP
show surface, AARNA9426551W_Q_BP
load AARNA9426551W_Q_EC.pdb
hide everything, AARNA9426551W_Q_EC
show cartoon, AARNA9426551W_Q_EC
color white, AARNA9426551W_Q_EC
spectrum b, rainbow, AARNA9426551W_Q_EC
set grid_slot, 2, AARNA9426551W_Q_EC
center AARNA9426551W_Q_EC
orient AARNA9426551W_Q_EC
show surface, AARNA9426551W_Q_EC
load AARNA9426551W_Q_LNMAX.pdb
hide everything, AARNA9426551W_Q_LNMAX
show cartoon, AARNA9426551W_Q_LNMAX
color white, AARNA9426551W_Q_LNMAX
spectrum b, rainbow, AARNA9426551W_Q_LNMAX
set grid_slot, 3, AARNA9426551W_Q_LNMAX
center AARNA9426551W_Q_LNMAX
orient AARNA9426551W_Q_LNMAX
show surface, AARNA9426551W_Q_LNMAX
load AARNA9426551W_Q_LNMIN.pdb
hide everything, AARNA9426551W_Q_LNMIN
show cartoon, AARNA9426551W_Q_LNMIN
color white, AARNA9426551W_Q_LNMIN
spectrum b, rainbow, AARNA9426551W_Q_LNMIN
set grid_slot, 4, AARNA9426551W_Q_LNMIN
center AARNA9426551W_Q_LNMIN
orient AARNA9426551W_Q_LNMIN
show surface, AARNA9426551W_Q_LNMIN
set seq_view, 1
set transparency, 0.0
