set grid_mode,1
set surface_quality, 0
load AARNA5659543W_T_BP.pdb
hide everything, AARNA5659543W_T_BP
show cartoon, AARNA5659543W_T_BP
color white, AARNA5659543W_T_BP
spectrum b, rainbow, AARNA5659543W_T_BP
set grid_slot, 1, AARNA5659543W_T_BP
center AARNA5659543W_T_BP
orient AARNA5659543W_T_BP
show surface, AARNA5659543W_T_BP
load AARNA5659543W_T_EC.pdb
hide everything, AARNA5659543W_T_EC
show cartoon, AARNA5659543W_T_EC
color white, AARNA5659543W_T_EC
spectrum b, rainbow, AARNA5659543W_T_EC
set grid_slot, 2, AARNA5659543W_T_EC
center AARNA5659543W_T_EC
orient AARNA5659543W_T_EC
show surface, AARNA5659543W_T_EC
load AARNA5659543W_T_LNMAX.pdb
hide everything, AARNA5659543W_T_LNMAX
show cartoon, AARNA5659543W_T_LNMAX
color white, AARNA5659543W_T_LNMAX
spectrum b, rainbow, AARNA5659543W_T_LNMAX
set grid_slot, 3, AARNA5659543W_T_LNMAX
center AARNA5659543W_T_LNMAX
orient AARNA5659543W_T_LNMAX
show surface, AARNA5659543W_T_LNMAX
load AARNA5659543W_T_LNMIN.pdb
hide everything, AARNA5659543W_T_LNMIN
show cartoon, AARNA5659543W_T_LNMIN
color white, AARNA5659543W_T_LNMIN
spectrum b, rainbow, AARNA5659543W_T_LNMIN
set grid_slot, 4, AARNA5659543W_T_LNMIN
center AARNA5659543W_T_LNMIN
orient AARNA5659543W_T_LNMIN
show surface, AARNA5659543W_T_LNMIN
set seq_view, 1
set transparency, 0.0
