set grid_mode,1
set surface_quality, 0
load AARNA7626165W_L_BP.pdb
hide everything, AARNA7626165W_L_BP
show cartoon, AARNA7626165W_L_BP
color white, AARNA7626165W_L_BP
spectrum b, rainbow, AARNA7626165W_L_BP
set grid_slot, 1, AARNA7626165W_L_BP
center AARNA7626165W_L_BP
orient AARNA7626165W_L_BP
show surface, AARNA7626165W_L_BP
load AARNA7626165W_L_EC.pdb
hide everything, AARNA7626165W_L_EC
show cartoon, AARNA7626165W_L_EC
color white, AARNA7626165W_L_EC
spectrum b, rainbow, AARNA7626165W_L_EC
set grid_slot, 2, AARNA7626165W_L_EC
center AARNA7626165W_L_EC
orient AARNA7626165W_L_EC
show surface, AARNA7626165W_L_EC
load AARNA7626165W_L_LNMAX.pdb
hide everything, AARNA7626165W_L_LNMAX
show cartoon, AARNA7626165W_L_LNMAX
color white, AARNA7626165W_L_LNMAX
spectrum b, rainbow, AARNA7626165W_L_LNMAX
set grid_slot, 3, AARNA7626165W_L_LNMAX
center AARNA7626165W_L_LNMAX
orient AARNA7626165W_L_LNMAX
show surface, AARNA7626165W_L_LNMAX
load AARNA7626165W_L_LNMIN.pdb
hide everything, AARNA7626165W_L_LNMIN
show cartoon, AARNA7626165W_L_LNMIN
color white, AARNA7626165W_L_LNMIN
spectrum b, rainbow, AARNA7626165W_L_LNMIN
set grid_slot, 4, AARNA7626165W_L_LNMIN
center AARNA7626165W_L_LNMIN
orient AARNA7626165W_L_LNMIN
show surface, AARNA7626165W_L_LNMIN
set seq_view, 1
set transparency, 0.0
