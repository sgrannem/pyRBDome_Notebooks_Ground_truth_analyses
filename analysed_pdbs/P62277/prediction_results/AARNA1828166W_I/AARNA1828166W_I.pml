set grid_mode,1
set surface_quality, 0
load AARNA1828166W_I_BP.pdb
hide everything, AARNA1828166W_I_BP
show cartoon, AARNA1828166W_I_BP
color white, AARNA1828166W_I_BP
spectrum b, rainbow, AARNA1828166W_I_BP
set grid_slot, 1, AARNA1828166W_I_BP
center AARNA1828166W_I_BP
orient AARNA1828166W_I_BP
show surface, AARNA1828166W_I_BP
load AARNA1828166W_I_EC.pdb
hide everything, AARNA1828166W_I_EC
show cartoon, AARNA1828166W_I_EC
color white, AARNA1828166W_I_EC
spectrum b, rainbow, AARNA1828166W_I_EC
set grid_slot, 2, AARNA1828166W_I_EC
center AARNA1828166W_I_EC
orient AARNA1828166W_I_EC
show surface, AARNA1828166W_I_EC
load AARNA1828166W_I_LNMAX.pdb
hide everything, AARNA1828166W_I_LNMAX
show cartoon, AARNA1828166W_I_LNMAX
color white, AARNA1828166W_I_LNMAX
spectrum b, rainbow, AARNA1828166W_I_LNMAX
set grid_slot, 3, AARNA1828166W_I_LNMAX
center AARNA1828166W_I_LNMAX
orient AARNA1828166W_I_LNMAX
show surface, AARNA1828166W_I_LNMAX
load AARNA1828166W_I_LNMIN.pdb
hide everything, AARNA1828166W_I_LNMIN
show cartoon, AARNA1828166W_I_LNMIN
color white, AARNA1828166W_I_LNMIN
spectrum b, rainbow, AARNA1828166W_I_LNMIN
set grid_slot, 4, AARNA1828166W_I_LNMIN
center AARNA1828166W_I_LNMIN
orient AARNA1828166W_I_LNMIN
show surface, AARNA1828166W_I_LNMIN
set seq_view, 1
set transparency, 0.0
