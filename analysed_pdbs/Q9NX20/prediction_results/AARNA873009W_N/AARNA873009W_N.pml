set grid_mode,1
set surface_quality, 0
load AARNA873009W_N_BP.pdb
hide everything, AARNA873009W_N_BP
show cartoon, AARNA873009W_N_BP
color white, AARNA873009W_N_BP
spectrum b, rainbow, AARNA873009W_N_BP
set grid_slot, 1, AARNA873009W_N_BP
center AARNA873009W_N_BP
orient AARNA873009W_N_BP
show surface, AARNA873009W_N_BP
load AARNA873009W_N_EC.pdb
hide everything, AARNA873009W_N_EC
show cartoon, AARNA873009W_N_EC
color white, AARNA873009W_N_EC
spectrum b, rainbow, AARNA873009W_N_EC
set grid_slot, 2, AARNA873009W_N_EC
center AARNA873009W_N_EC
orient AARNA873009W_N_EC
show surface, AARNA873009W_N_EC
load AARNA873009W_N_LNMAX.pdb
hide everything, AARNA873009W_N_LNMAX
show cartoon, AARNA873009W_N_LNMAX
color white, AARNA873009W_N_LNMAX
spectrum b, rainbow, AARNA873009W_N_LNMAX
set grid_slot, 3, AARNA873009W_N_LNMAX
center AARNA873009W_N_LNMAX
orient AARNA873009W_N_LNMAX
show surface, AARNA873009W_N_LNMAX
load AARNA873009W_N_LNMIN.pdb
hide everything, AARNA873009W_N_LNMIN
show cartoon, AARNA873009W_N_LNMIN
color white, AARNA873009W_N_LNMIN
spectrum b, rainbow, AARNA873009W_N_LNMIN
set grid_slot, 4, AARNA873009W_N_LNMIN
center AARNA873009W_N_LNMIN
orient AARNA873009W_N_LNMIN
show surface, AARNA873009W_N_LNMIN
set seq_view, 1
set transparency, 0.0
