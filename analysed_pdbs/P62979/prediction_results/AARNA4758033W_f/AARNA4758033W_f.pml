set grid_mode,1
set surface_quality, 0
load AARNA4758033W_f_BP.pdb
hide everything, AARNA4758033W_f_BP
show cartoon, AARNA4758033W_f_BP
color white, AARNA4758033W_f_BP
spectrum b, rainbow, AARNA4758033W_f_BP
set grid_slot, 1, AARNA4758033W_f_BP
center AARNA4758033W_f_BP
orient AARNA4758033W_f_BP
show surface, AARNA4758033W_f_BP
load AARNA4758033W_f_EC.pdb
hide everything, AARNA4758033W_f_EC
show cartoon, AARNA4758033W_f_EC
color white, AARNA4758033W_f_EC
spectrum b, rainbow, AARNA4758033W_f_EC
set grid_slot, 2, AARNA4758033W_f_EC
center AARNA4758033W_f_EC
orient AARNA4758033W_f_EC
show surface, AARNA4758033W_f_EC
load AARNA4758033W_f_LNMAX.pdb
hide everything, AARNA4758033W_f_LNMAX
show cartoon, AARNA4758033W_f_LNMAX
color white, AARNA4758033W_f_LNMAX
spectrum b, rainbow, AARNA4758033W_f_LNMAX
set grid_slot, 3, AARNA4758033W_f_LNMAX
center AARNA4758033W_f_LNMAX
orient AARNA4758033W_f_LNMAX
show surface, AARNA4758033W_f_LNMAX
load AARNA4758033W_f_LNMIN.pdb
hide everything, AARNA4758033W_f_LNMIN
show cartoon, AARNA4758033W_f_LNMIN
color white, AARNA4758033W_f_LNMIN
spectrum b, rainbow, AARNA4758033W_f_LNMIN
set grid_slot, 4, AARNA4758033W_f_LNMIN
center AARNA4758033W_f_LNMIN
orient AARNA4758033W_f_LNMIN
show surface, AARNA4758033W_f_LNMIN
set seq_view, 1
set transparency, 0.0
