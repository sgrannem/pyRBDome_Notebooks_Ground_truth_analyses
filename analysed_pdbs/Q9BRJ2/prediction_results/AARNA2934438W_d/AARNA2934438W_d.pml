set grid_mode,1
set surface_quality, 0
load AARNA2934438W_d_BP.pdb
hide everything, AARNA2934438W_d_BP
show cartoon, AARNA2934438W_d_BP
color white, AARNA2934438W_d_BP
spectrum b, rainbow, AARNA2934438W_d_BP
set grid_slot, 1, AARNA2934438W_d_BP
center AARNA2934438W_d_BP
orient AARNA2934438W_d_BP
show surface, AARNA2934438W_d_BP
load AARNA2934438W_d_EC.pdb
hide everything, AARNA2934438W_d_EC
show cartoon, AARNA2934438W_d_EC
color white, AARNA2934438W_d_EC
spectrum b, rainbow, AARNA2934438W_d_EC
set grid_slot, 2, AARNA2934438W_d_EC
center AARNA2934438W_d_EC
orient AARNA2934438W_d_EC
show surface, AARNA2934438W_d_EC
load AARNA2934438W_d_LNMAX.pdb
hide everything, AARNA2934438W_d_LNMAX
show cartoon, AARNA2934438W_d_LNMAX
color white, AARNA2934438W_d_LNMAX
spectrum b, rainbow, AARNA2934438W_d_LNMAX
set grid_slot, 3, AARNA2934438W_d_LNMAX
center AARNA2934438W_d_LNMAX
orient AARNA2934438W_d_LNMAX
show surface, AARNA2934438W_d_LNMAX
load AARNA2934438W_d_LNMIN.pdb
hide everything, AARNA2934438W_d_LNMIN
show cartoon, AARNA2934438W_d_LNMIN
color white, AARNA2934438W_d_LNMIN
spectrum b, rainbow, AARNA2934438W_d_LNMIN
set grid_slot, 4, AARNA2934438W_d_LNMIN
center AARNA2934438W_d_LNMIN
orient AARNA2934438W_d_LNMIN
show surface, AARNA2934438W_d_LNMIN
set seq_view, 1
set transparency, 0.0
