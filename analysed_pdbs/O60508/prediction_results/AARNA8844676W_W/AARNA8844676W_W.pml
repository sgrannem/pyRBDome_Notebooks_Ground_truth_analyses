set grid_mode,1
set surface_quality, 0
load AARNA8844676W_W_BP.pdb
hide everything, AARNA8844676W_W_BP
show cartoon, AARNA8844676W_W_BP
color white, AARNA8844676W_W_BP
spectrum b, rainbow, AARNA8844676W_W_BP
set grid_slot, 1, AARNA8844676W_W_BP
center AARNA8844676W_W_BP
orient AARNA8844676W_W_BP
show surface, AARNA8844676W_W_BP
load AARNA8844676W_W_EC.pdb
hide everything, AARNA8844676W_W_EC
show cartoon, AARNA8844676W_W_EC
color white, AARNA8844676W_W_EC
spectrum b, rainbow, AARNA8844676W_W_EC
set grid_slot, 2, AARNA8844676W_W_EC
center AARNA8844676W_W_EC
orient AARNA8844676W_W_EC
show surface, AARNA8844676W_W_EC
load AARNA8844676W_W_LNMAX.pdb
hide everything, AARNA8844676W_W_LNMAX
show cartoon, AARNA8844676W_W_LNMAX
color white, AARNA8844676W_W_LNMAX
spectrum b, rainbow, AARNA8844676W_W_LNMAX
set grid_slot, 3, AARNA8844676W_W_LNMAX
center AARNA8844676W_W_LNMAX
orient AARNA8844676W_W_LNMAX
show surface, AARNA8844676W_W_LNMAX
load AARNA8844676W_W_LNMIN.pdb
hide everything, AARNA8844676W_W_LNMIN
show cartoon, AARNA8844676W_W_LNMIN
color white, AARNA8844676W_W_LNMIN
spectrum b, rainbow, AARNA8844676W_W_LNMIN
set grid_slot, 4, AARNA8844676W_W_LNMIN
center AARNA8844676W_W_LNMIN
orient AARNA8844676W_W_LNMIN
show surface, AARNA8844676W_W_LNMIN
set seq_view, 1
set transparency, 0.0
