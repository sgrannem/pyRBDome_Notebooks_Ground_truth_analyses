set grid_mode,1
set surface_quality, 0
load AARNA4730857W_v_BP.pdb
hide everything, AARNA4730857W_v_BP
show cartoon, AARNA4730857W_v_BP
color white, AARNA4730857W_v_BP
spectrum b, rainbow, AARNA4730857W_v_BP
set grid_slot, 1, AARNA4730857W_v_BP
center AARNA4730857W_v_BP
orient AARNA4730857W_v_BP
show surface, AARNA4730857W_v_BP
load AARNA4730857W_v_EC.pdb
hide everything, AARNA4730857W_v_EC
show cartoon, AARNA4730857W_v_EC
color white, AARNA4730857W_v_EC
spectrum b, rainbow, AARNA4730857W_v_EC
set grid_slot, 2, AARNA4730857W_v_EC
center AARNA4730857W_v_EC
orient AARNA4730857W_v_EC
show surface, AARNA4730857W_v_EC
load AARNA4730857W_v_LNMAX.pdb
hide everything, AARNA4730857W_v_LNMAX
show cartoon, AARNA4730857W_v_LNMAX
color white, AARNA4730857W_v_LNMAX
spectrum b, rainbow, AARNA4730857W_v_LNMAX
set grid_slot, 3, AARNA4730857W_v_LNMAX
center AARNA4730857W_v_LNMAX
orient AARNA4730857W_v_LNMAX
show surface, AARNA4730857W_v_LNMAX
load AARNA4730857W_v_LNMIN.pdb
hide everything, AARNA4730857W_v_LNMIN
show cartoon, AARNA4730857W_v_LNMIN
color white, AARNA4730857W_v_LNMIN
spectrum b, rainbow, AARNA4730857W_v_LNMIN
set grid_slot, 4, AARNA4730857W_v_LNMIN
center AARNA4730857W_v_LNMIN
orient AARNA4730857W_v_LNMIN
show surface, AARNA4730857W_v_LNMIN
set seq_view, 1
set transparency, 0.0
