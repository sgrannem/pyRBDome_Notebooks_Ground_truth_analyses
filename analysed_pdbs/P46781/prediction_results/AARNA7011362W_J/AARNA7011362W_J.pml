set grid_mode,1
set surface_quality, 0
load AARNA7011362W_J_BP.pdb
hide everything, AARNA7011362W_J_BP
show cartoon, AARNA7011362W_J_BP
color white, AARNA7011362W_J_BP
spectrum b, rainbow, AARNA7011362W_J_BP
set grid_slot, 1, AARNA7011362W_J_BP
center AARNA7011362W_J_BP
orient AARNA7011362W_J_BP
show surface, AARNA7011362W_J_BP
load AARNA7011362W_J_EC.pdb
hide everything, AARNA7011362W_J_EC
show cartoon, AARNA7011362W_J_EC
color white, AARNA7011362W_J_EC
spectrum b, rainbow, AARNA7011362W_J_EC
set grid_slot, 2, AARNA7011362W_J_EC
center AARNA7011362W_J_EC
orient AARNA7011362W_J_EC
show surface, AARNA7011362W_J_EC
load AARNA7011362W_J_LNMAX.pdb
hide everything, AARNA7011362W_J_LNMAX
show cartoon, AARNA7011362W_J_LNMAX
color white, AARNA7011362W_J_LNMAX
spectrum b, rainbow, AARNA7011362W_J_LNMAX
set grid_slot, 3, AARNA7011362W_J_LNMAX
center AARNA7011362W_J_LNMAX
orient AARNA7011362W_J_LNMAX
show surface, AARNA7011362W_J_LNMAX
load AARNA7011362W_J_LNMIN.pdb
hide everything, AARNA7011362W_J_LNMIN
show cartoon, AARNA7011362W_J_LNMIN
color white, AARNA7011362W_J_LNMIN
spectrum b, rainbow, AARNA7011362W_J_LNMIN
set grid_slot, 4, AARNA7011362W_J_LNMIN
center AARNA7011362W_J_LNMIN
orient AARNA7011362W_J_LNMIN
show surface, AARNA7011362W_J_LNMIN
set seq_view, 1
set transparency, 0.0
