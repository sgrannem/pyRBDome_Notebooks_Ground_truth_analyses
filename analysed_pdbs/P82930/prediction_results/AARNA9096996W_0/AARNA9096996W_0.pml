set grid_mode,1
set surface_quality, 0
load AARNA9096996W_0_BP.pdb
hide everything, AARNA9096996W_0_BP
show cartoon, AARNA9096996W_0_BP
color white, AARNA9096996W_0_BP
spectrum b, rainbow, AARNA9096996W_0_BP
set grid_slot, 1, AARNA9096996W_0_BP
center AARNA9096996W_0_BP
orient AARNA9096996W_0_BP
show surface, AARNA9096996W_0_BP
load AARNA9096996W_0_EC.pdb
hide everything, AARNA9096996W_0_EC
show cartoon, AARNA9096996W_0_EC
color white, AARNA9096996W_0_EC
spectrum b, rainbow, AARNA9096996W_0_EC
set grid_slot, 2, AARNA9096996W_0_EC
center AARNA9096996W_0_EC
orient AARNA9096996W_0_EC
show surface, AARNA9096996W_0_EC
load AARNA9096996W_0_LNMAX.pdb
hide everything, AARNA9096996W_0_LNMAX
show cartoon, AARNA9096996W_0_LNMAX
color white, AARNA9096996W_0_LNMAX
spectrum b, rainbow, AARNA9096996W_0_LNMAX
set grid_slot, 3, AARNA9096996W_0_LNMAX
center AARNA9096996W_0_LNMAX
orient AARNA9096996W_0_LNMAX
show surface, AARNA9096996W_0_LNMAX
load AARNA9096996W_0_LNMIN.pdb
hide everything, AARNA9096996W_0_LNMIN
show cartoon, AARNA9096996W_0_LNMIN
color white, AARNA9096996W_0_LNMIN
spectrum b, rainbow, AARNA9096996W_0_LNMIN
set grid_slot, 4, AARNA9096996W_0_LNMIN
center AARNA9096996W_0_LNMIN
orient AARNA9096996W_0_LNMIN
show surface, AARNA9096996W_0_LNMIN
set seq_view, 1
set transparency, 0.0
