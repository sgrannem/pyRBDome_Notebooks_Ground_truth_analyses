set grid_mode,1
set surface_quality, 0
load AARNA6035346W_O_BP.pdb
hide everything, AARNA6035346W_O_BP
show cartoon, AARNA6035346W_O_BP
color white, AARNA6035346W_O_BP
spectrum b, rainbow, AARNA6035346W_O_BP
set grid_slot, 1, AARNA6035346W_O_BP
center AARNA6035346W_O_BP
orient AARNA6035346W_O_BP
show surface, AARNA6035346W_O_BP
load AARNA6035346W_O_EC.pdb
hide everything, AARNA6035346W_O_EC
show cartoon, AARNA6035346W_O_EC
color white, AARNA6035346W_O_EC
spectrum b, rainbow, AARNA6035346W_O_EC
set grid_slot, 2, AARNA6035346W_O_EC
center AARNA6035346W_O_EC
orient AARNA6035346W_O_EC
show surface, AARNA6035346W_O_EC
load AARNA6035346W_O_LNMAX.pdb
hide everything, AARNA6035346W_O_LNMAX
show cartoon, AARNA6035346W_O_LNMAX
color white, AARNA6035346W_O_LNMAX
spectrum b, rainbow, AARNA6035346W_O_LNMAX
set grid_slot, 3, AARNA6035346W_O_LNMAX
center AARNA6035346W_O_LNMAX
orient AARNA6035346W_O_LNMAX
show surface, AARNA6035346W_O_LNMAX
load AARNA6035346W_O_LNMIN.pdb
hide everything, AARNA6035346W_O_LNMIN
show cartoon, AARNA6035346W_O_LNMIN
color white, AARNA6035346W_O_LNMIN
spectrum b, rainbow, AARNA6035346W_O_LNMIN
set grid_slot, 4, AARNA6035346W_O_LNMIN
center AARNA6035346W_O_LNMIN
orient AARNA6035346W_O_LNMIN
show surface, AARNA6035346W_O_LNMIN
set seq_view, 1
set transparency, 0.0
