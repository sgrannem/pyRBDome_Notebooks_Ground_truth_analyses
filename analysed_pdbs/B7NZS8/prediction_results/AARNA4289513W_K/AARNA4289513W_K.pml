set grid_mode,1
set surface_quality, 0
load AARNA4289513W_K_BP.pdb
hide everything, AARNA4289513W_K_BP
show cartoon, AARNA4289513W_K_BP
color white, AARNA4289513W_K_BP
spectrum b, rainbow, AARNA4289513W_K_BP
set grid_slot, 1, AARNA4289513W_K_BP
center AARNA4289513W_K_BP
orient AARNA4289513W_K_BP
show surface, AARNA4289513W_K_BP
load AARNA4289513W_K_EC.pdb
hide everything, AARNA4289513W_K_EC
show cartoon, AARNA4289513W_K_EC
color white, AARNA4289513W_K_EC
spectrum b, rainbow, AARNA4289513W_K_EC
set grid_slot, 2, AARNA4289513W_K_EC
center AARNA4289513W_K_EC
orient AARNA4289513W_K_EC
show surface, AARNA4289513W_K_EC
load AARNA4289513W_K_LNMAX.pdb
hide everything, AARNA4289513W_K_LNMAX
show cartoon, AARNA4289513W_K_LNMAX
color white, AARNA4289513W_K_LNMAX
spectrum b, rainbow, AARNA4289513W_K_LNMAX
set grid_slot, 3, AARNA4289513W_K_LNMAX
center AARNA4289513W_K_LNMAX
orient AARNA4289513W_K_LNMAX
show surface, AARNA4289513W_K_LNMAX
load AARNA4289513W_K_LNMIN.pdb
hide everything, AARNA4289513W_K_LNMIN
show cartoon, AARNA4289513W_K_LNMIN
color white, AARNA4289513W_K_LNMIN
spectrum b, rainbow, AARNA4289513W_K_LNMIN
set grid_slot, 4, AARNA4289513W_K_LNMIN
center AARNA4289513W_K_LNMIN
orient AARNA4289513W_K_LNMIN
show surface, AARNA4289513W_K_LNMIN
set seq_view, 1
set transparency, 0.0
