set grid_mode,1
set surface_quality, 0
load AARNA2599356W_R_BP.pdb
hide everything, AARNA2599356W_R_BP
show cartoon, AARNA2599356W_R_BP
color white, AARNA2599356W_R_BP
spectrum b, rainbow, AARNA2599356W_R_BP
set grid_slot, 1, AARNA2599356W_R_BP
center AARNA2599356W_R_BP
orient AARNA2599356W_R_BP
show surface, AARNA2599356W_R_BP
load AARNA2599356W_R_EC.pdb
hide everything, AARNA2599356W_R_EC
show cartoon, AARNA2599356W_R_EC
color white, AARNA2599356W_R_EC
spectrum b, rainbow, AARNA2599356W_R_EC
set grid_slot, 2, AARNA2599356W_R_EC
center AARNA2599356W_R_EC
orient AARNA2599356W_R_EC
show surface, AARNA2599356W_R_EC
load AARNA2599356W_R_LNMAX.pdb
hide everything, AARNA2599356W_R_LNMAX
show cartoon, AARNA2599356W_R_LNMAX
color white, AARNA2599356W_R_LNMAX
spectrum b, rainbow, AARNA2599356W_R_LNMAX
set grid_slot, 3, AARNA2599356W_R_LNMAX
center AARNA2599356W_R_LNMAX
orient AARNA2599356W_R_LNMAX
show surface, AARNA2599356W_R_LNMAX
load AARNA2599356W_R_LNMIN.pdb
hide everything, AARNA2599356W_R_LNMIN
show cartoon, AARNA2599356W_R_LNMIN
color white, AARNA2599356W_R_LNMIN
spectrum b, rainbow, AARNA2599356W_R_LNMIN
set grid_slot, 4, AARNA2599356W_R_LNMIN
center AARNA2599356W_R_LNMIN
orient AARNA2599356W_R_LNMIN
show surface, AARNA2599356W_R_LNMIN
set seq_view, 1
set transparency, 0.0
