set grid_mode,1
set surface_quality, 0
load AARNA8709330W_6_BP.pdb
hide everything, AARNA8709330W_6_BP
show cartoon, AARNA8709330W_6_BP
color white, AARNA8709330W_6_BP
spectrum b, rainbow, AARNA8709330W_6_BP
set grid_slot, 1, AARNA8709330W_6_BP
center AARNA8709330W_6_BP
orient AARNA8709330W_6_BP
show surface, AARNA8709330W_6_BP
load AARNA8709330W_6_EC.pdb
hide everything, AARNA8709330W_6_EC
show cartoon, AARNA8709330W_6_EC
color white, AARNA8709330W_6_EC
spectrum b, rainbow, AARNA8709330W_6_EC
set grid_slot, 2, AARNA8709330W_6_EC
center AARNA8709330W_6_EC
orient AARNA8709330W_6_EC
show surface, AARNA8709330W_6_EC
load AARNA8709330W_6_LNMAX.pdb
hide everything, AARNA8709330W_6_LNMAX
show cartoon, AARNA8709330W_6_LNMAX
color white, AARNA8709330W_6_LNMAX
spectrum b, rainbow, AARNA8709330W_6_LNMAX
set grid_slot, 3, AARNA8709330W_6_LNMAX
center AARNA8709330W_6_LNMAX
orient AARNA8709330W_6_LNMAX
show surface, AARNA8709330W_6_LNMAX
load AARNA8709330W_6_LNMIN.pdb
hide everything, AARNA8709330W_6_LNMIN
show cartoon, AARNA8709330W_6_LNMIN
color white, AARNA8709330W_6_LNMIN
spectrum b, rainbow, AARNA8709330W_6_LNMIN
set grid_slot, 4, AARNA8709330W_6_LNMIN
center AARNA8709330W_6_LNMIN
orient AARNA8709330W_6_LNMIN
show surface, AARNA8709330W_6_LNMIN
set seq_view, 1
set transparency, 0.0
