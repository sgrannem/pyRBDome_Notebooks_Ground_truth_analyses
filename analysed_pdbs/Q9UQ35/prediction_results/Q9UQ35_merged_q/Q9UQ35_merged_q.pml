set grid_mode,1
set surface_quality, 0
load AARNA5200378W_q_BP.pdb
hide everything, AARNA5200378W_q_BP
show cartoon, AARNA5200378W_q_BP
color white, AARNA5200378W_q_BP
spectrum b, rainbow, AARNA5200378W_q_BP
set grid_slot, 1, AARNA5200378W_q_BP
center AARNA5200378W_q_BP
orient AARNA5200378W_q_BP
show surface, AARNA5200378W_q_BP
load AARNA5200378W_q_EC.pdb
hide everything, AARNA5200378W_q_EC
show cartoon, AARNA5200378W_q_EC
color white, AARNA5200378W_q_EC
spectrum b, rainbow, AARNA5200378W_q_EC
set grid_slot, 2, AARNA5200378W_q_EC
center AARNA5200378W_q_EC
orient AARNA5200378W_q_EC
show surface, AARNA5200378W_q_EC
load AARNA5200378W_q_LNMAX.pdb
hide everything, AARNA5200378W_q_LNMAX
show cartoon, AARNA5200378W_q_LNMAX
color white, AARNA5200378W_q_LNMAX
spectrum b, rainbow, AARNA5200378W_q_LNMAX
set grid_slot, 3, AARNA5200378W_q_LNMAX
center AARNA5200378W_q_LNMAX
orient AARNA5200378W_q_LNMAX
show surface, AARNA5200378W_q_LNMAX
load AARNA5200378W_q_LNMIN.pdb
hide everything, AARNA5200378W_q_LNMIN
show cartoon, AARNA5200378W_q_LNMIN
color white, AARNA5200378W_q_LNMIN
spectrum b, rainbow, AARNA5200378W_q_LNMIN
set grid_slot, 4, AARNA5200378W_q_LNMIN
center AARNA5200378W_q_LNMIN
orient AARNA5200378W_q_LNMIN
show surface, AARNA5200378W_q_LNMIN
set seq_view, 1
set transparency, 0.0
