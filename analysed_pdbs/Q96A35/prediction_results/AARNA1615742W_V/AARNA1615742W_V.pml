set grid_mode,1
set surface_quality, 0
load AARNA1615742W_V_BP.pdb
hide everything, AARNA1615742W_V_BP
show cartoon, AARNA1615742W_V_BP
color white, AARNA1615742W_V_BP
spectrum b, rainbow, AARNA1615742W_V_BP
set grid_slot, 1, AARNA1615742W_V_BP
center AARNA1615742W_V_BP
orient AARNA1615742W_V_BP
show surface, AARNA1615742W_V_BP
load AARNA1615742W_V_EC.pdb
hide everything, AARNA1615742W_V_EC
show cartoon, AARNA1615742W_V_EC
color white, AARNA1615742W_V_EC
spectrum b, rainbow, AARNA1615742W_V_EC
set grid_slot, 2, AARNA1615742W_V_EC
center AARNA1615742W_V_EC
orient AARNA1615742W_V_EC
show surface, AARNA1615742W_V_EC
load AARNA1615742W_V_LNMAX.pdb
hide everything, AARNA1615742W_V_LNMAX
show cartoon, AARNA1615742W_V_LNMAX
color white, AARNA1615742W_V_LNMAX
spectrum b, rainbow, AARNA1615742W_V_LNMAX
set grid_slot, 3, AARNA1615742W_V_LNMAX
center AARNA1615742W_V_LNMAX
orient AARNA1615742W_V_LNMAX
show surface, AARNA1615742W_V_LNMAX
load AARNA1615742W_V_LNMIN.pdb
hide everything, AARNA1615742W_V_LNMIN
show cartoon, AARNA1615742W_V_LNMIN
color white, AARNA1615742W_V_LNMIN
spectrum b, rainbow, AARNA1615742W_V_LNMIN
set grid_slot, 4, AARNA1615742W_V_LNMIN
center AARNA1615742W_V_LNMIN
orient AARNA1615742W_V_LNMIN
show surface, AARNA1615742W_V_LNMIN
set seq_view, 1
set transparency, 0.0
