set grid_mode,1
set surface_quality, 0
load AARNA6757043W_Z_BP.pdb
hide everything, AARNA6757043W_Z_BP
show cartoon, AARNA6757043W_Z_BP
color white, AARNA6757043W_Z_BP
spectrum b, rainbow, AARNA6757043W_Z_BP
set grid_slot, 1, AARNA6757043W_Z_BP
center AARNA6757043W_Z_BP
orient AARNA6757043W_Z_BP
show surface, AARNA6757043W_Z_BP
load AARNA6757043W_Z_EC.pdb
hide everything, AARNA6757043W_Z_EC
show cartoon, AARNA6757043W_Z_EC
color white, AARNA6757043W_Z_EC
spectrum b, rainbow, AARNA6757043W_Z_EC
set grid_slot, 2, AARNA6757043W_Z_EC
center AARNA6757043W_Z_EC
orient AARNA6757043W_Z_EC
show surface, AARNA6757043W_Z_EC
load AARNA6757043W_Z_LNMAX.pdb
hide everything, AARNA6757043W_Z_LNMAX
show cartoon, AARNA6757043W_Z_LNMAX
color white, AARNA6757043W_Z_LNMAX
spectrum b, rainbow, AARNA6757043W_Z_LNMAX
set grid_slot, 3, AARNA6757043W_Z_LNMAX
center AARNA6757043W_Z_LNMAX
orient AARNA6757043W_Z_LNMAX
show surface, AARNA6757043W_Z_LNMAX
load AARNA6757043W_Z_LNMIN.pdb
hide everything, AARNA6757043W_Z_LNMIN
show cartoon, AARNA6757043W_Z_LNMIN
color white, AARNA6757043W_Z_LNMIN
spectrum b, rainbow, AARNA6757043W_Z_LNMIN
set grid_slot, 4, AARNA6757043W_Z_LNMIN
center AARNA6757043W_Z_LNMIN
orient AARNA6757043W_Z_LNMIN
show surface, AARNA6757043W_Z_LNMIN
set seq_view, 1
set transparency, 0.0
