set grid_mode,1
set surface_quality, 0
load AARNA2694147W_8_BP.pdb
hide everything, AARNA2694147W_8_BP
show cartoon, AARNA2694147W_8_BP
color white, AARNA2694147W_8_BP
spectrum b, rainbow, AARNA2694147W_8_BP
set grid_slot, 1, AARNA2694147W_8_BP
center AARNA2694147W_8_BP
orient AARNA2694147W_8_BP
show surface, AARNA2694147W_8_BP
load AARNA2694147W_8_EC.pdb
hide everything, AARNA2694147W_8_EC
show cartoon, AARNA2694147W_8_EC
color white, AARNA2694147W_8_EC
spectrum b, rainbow, AARNA2694147W_8_EC
set grid_slot, 2, AARNA2694147W_8_EC
center AARNA2694147W_8_EC
orient AARNA2694147W_8_EC
show surface, AARNA2694147W_8_EC
load AARNA2694147W_8_LNMAX.pdb
hide everything, AARNA2694147W_8_LNMAX
show cartoon, AARNA2694147W_8_LNMAX
color white, AARNA2694147W_8_LNMAX
spectrum b, rainbow, AARNA2694147W_8_LNMAX
set grid_slot, 3, AARNA2694147W_8_LNMAX
center AARNA2694147W_8_LNMAX
orient AARNA2694147W_8_LNMAX
show surface, AARNA2694147W_8_LNMAX
load AARNA2694147W_8_LNMIN.pdb
hide everything, AARNA2694147W_8_LNMIN
show cartoon, AARNA2694147W_8_LNMIN
color white, AARNA2694147W_8_LNMIN
spectrum b, rainbow, AARNA2694147W_8_LNMIN
set grid_slot, 4, AARNA2694147W_8_LNMIN
center AARNA2694147W_8_LNMIN
orient AARNA2694147W_8_LNMIN
show surface, AARNA2694147W_8_LNMIN
set seq_view, 1
set transparency, 0.0
