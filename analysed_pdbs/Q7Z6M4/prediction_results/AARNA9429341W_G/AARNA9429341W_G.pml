set grid_mode,1
set surface_quality, 0
load AARNA9429341W_G_BP.pdb
hide everything, AARNA9429341W_G_BP
show cartoon, AARNA9429341W_G_BP
color white, AARNA9429341W_G_BP
spectrum b, rainbow, AARNA9429341W_G_BP
set grid_slot, 1, AARNA9429341W_G_BP
center AARNA9429341W_G_BP
orient AARNA9429341W_G_BP
show surface, AARNA9429341W_G_BP
load AARNA9429341W_G_EC.pdb
hide everything, AARNA9429341W_G_EC
show cartoon, AARNA9429341W_G_EC
color white, AARNA9429341W_G_EC
spectrum b, rainbow, AARNA9429341W_G_EC
set grid_slot, 2, AARNA9429341W_G_EC
center AARNA9429341W_G_EC
orient AARNA9429341W_G_EC
show surface, AARNA9429341W_G_EC
load AARNA9429341W_G_LNMAX.pdb
hide everything, AARNA9429341W_G_LNMAX
show cartoon, AARNA9429341W_G_LNMAX
color white, AARNA9429341W_G_LNMAX
spectrum b, rainbow, AARNA9429341W_G_LNMAX
set grid_slot, 3, AARNA9429341W_G_LNMAX
center AARNA9429341W_G_LNMAX
orient AARNA9429341W_G_LNMAX
show surface, AARNA9429341W_G_LNMAX
load AARNA9429341W_G_LNMIN.pdb
hide everything, AARNA9429341W_G_LNMIN
show cartoon, AARNA9429341W_G_LNMIN
color white, AARNA9429341W_G_LNMIN
spectrum b, rainbow, AARNA9429341W_G_LNMIN
set grid_slot, 4, AARNA9429341W_G_LNMIN
center AARNA9429341W_G_LNMIN
orient AARNA9429341W_G_LNMIN
show surface, AARNA9429341W_G_LNMIN
set seq_view, 1
set transparency, 0.0
