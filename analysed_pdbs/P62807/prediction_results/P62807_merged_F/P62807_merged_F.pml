set grid_mode,1
set surface_quality, 0
load AARNA603966W_F_BP.pdb
hide everything, AARNA603966W_F_BP
show cartoon, AARNA603966W_F_BP
color white, AARNA603966W_F_BP
spectrum b, rainbow, AARNA603966W_F_BP
set grid_slot, 1, AARNA603966W_F_BP
center AARNA603966W_F_BP
orient AARNA603966W_F_BP
show surface, AARNA603966W_F_BP
load AARNA603966W_F_EC.pdb
hide everything, AARNA603966W_F_EC
show cartoon, AARNA603966W_F_EC
color white, AARNA603966W_F_EC
spectrum b, rainbow, AARNA603966W_F_EC
set grid_slot, 2, AARNA603966W_F_EC
center AARNA603966W_F_EC
orient AARNA603966W_F_EC
show surface, AARNA603966W_F_EC
load AARNA603966W_F_LNMAX.pdb
hide everything, AARNA603966W_F_LNMAX
show cartoon, AARNA603966W_F_LNMAX
color white, AARNA603966W_F_LNMAX
spectrum b, rainbow, AARNA603966W_F_LNMAX
set grid_slot, 3, AARNA603966W_F_LNMAX
center AARNA603966W_F_LNMAX
orient AARNA603966W_F_LNMAX
show surface, AARNA603966W_F_LNMAX
load AARNA603966W_F_LNMIN.pdb
hide everything, AARNA603966W_F_LNMIN
show cartoon, AARNA603966W_F_LNMIN
color white, AARNA603966W_F_LNMIN
spectrum b, rainbow, AARNA603966W_F_LNMIN
set grid_slot, 4, AARNA603966W_F_LNMIN
center AARNA603966W_F_LNMIN
orient AARNA603966W_F_LNMIN
show surface, AARNA603966W_F_LNMIN
set seq_view, 1
set transparency, 0.0
