set grid_mode,1
set surface_quality, 0
load AARNA5120042W_a_BP.pdb
hide everything, AARNA5120042W_a_BP
show cartoon, AARNA5120042W_a_BP
color white, AARNA5120042W_a_BP
spectrum b, rainbow, AARNA5120042W_a_BP
set grid_slot, 1, AARNA5120042W_a_BP
center AARNA5120042W_a_BP
orient AARNA5120042W_a_BP
show surface, AARNA5120042W_a_BP
load AARNA5120042W_a_EC.pdb
hide everything, AARNA5120042W_a_EC
show cartoon, AARNA5120042W_a_EC
color white, AARNA5120042W_a_EC
spectrum b, rainbow, AARNA5120042W_a_EC
set grid_slot, 2, AARNA5120042W_a_EC
center AARNA5120042W_a_EC
orient AARNA5120042W_a_EC
show surface, AARNA5120042W_a_EC
load AARNA5120042W_a_LNMAX.pdb
hide everything, AARNA5120042W_a_LNMAX
show cartoon, AARNA5120042W_a_LNMAX
color white, AARNA5120042W_a_LNMAX
spectrum b, rainbow, AARNA5120042W_a_LNMAX
set grid_slot, 3, AARNA5120042W_a_LNMAX
center AARNA5120042W_a_LNMAX
orient AARNA5120042W_a_LNMAX
show surface, AARNA5120042W_a_LNMAX
load AARNA5120042W_a_LNMIN.pdb
hide everything, AARNA5120042W_a_LNMIN
show cartoon, AARNA5120042W_a_LNMIN
color white, AARNA5120042W_a_LNMIN
spectrum b, rainbow, AARNA5120042W_a_LNMIN
set grid_slot, 4, AARNA5120042W_a_LNMIN
center AARNA5120042W_a_LNMIN
orient AARNA5120042W_a_LNMIN
show surface, AARNA5120042W_a_LNMIN
set seq_view, 1
set transparency, 0.0
