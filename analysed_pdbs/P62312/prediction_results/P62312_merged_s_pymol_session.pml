set surface_quality, 0
bg_color white
load P62312_merged_s_RNABindRPlus.pdb,object=RNABindRPlus
hide everything, RNABindRPlus
show cartoon, RNABindRPlus
color white, RNABindRPlus
spectrum b, rainbow, RNABindRPlus
show surface, RNABindRPlus
load P62312_merged_s_DisoRDPbind.pdb,object=DisoRDPbind
hide everything, DisoRDPbind
show cartoon, DisoRDPbind
color white, DisoRDPbind
spectrum b, rainbow, DisoRDPbind
show surface, DisoRDPbind
load P62312_merged_s_HydRa.pdb,object=HydRa
hide everything, HydRa
show cartoon, HydRa
color white, HydRa
spectrum b, rainbow, HydRa
show surface, HydRa
load P62312_merged_s_model_predictions.pdb,object=prediction
hide everything, prediction
show cartoon, prediction
color white, prediction
spectrum b, rainbow, prediction
show surface, prediction
load ../distances_merged/P62312_merged.pdb,object=RBD
hide everything, RBD
show cartoon, RBD
show sticks, RBDcolor white, RBD
color white, full_structure
color white, RBD
spectrum b, rainbow_rev, RBD, minimum=2, maximum=4.2
show surface, RBD
set seq_view, 1
align all