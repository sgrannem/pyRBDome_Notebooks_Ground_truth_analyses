set grid_mode,1
set surface_quality, 0
load AARNA1039998W_x_BP.pdb
hide everything, AARNA1039998W_x_BP
show cartoon, AARNA1039998W_x_BP
color white, AARNA1039998W_x_BP
spectrum b, rainbow, AARNA1039998W_x_BP
set grid_slot, 1, AARNA1039998W_x_BP
center AARNA1039998W_x_BP
orient AARNA1039998W_x_BP
show surface, AARNA1039998W_x_BP
load AARNA1039998W_x_EC.pdb
hide everything, AARNA1039998W_x_EC
show cartoon, AARNA1039998W_x_EC
color white, AARNA1039998W_x_EC
spectrum b, rainbow, AARNA1039998W_x_EC
set grid_slot, 2, AARNA1039998W_x_EC
center AARNA1039998W_x_EC
orient AARNA1039998W_x_EC
show surface, AARNA1039998W_x_EC
load AARNA1039998W_x_LNMAX.pdb
hide everything, AARNA1039998W_x_LNMAX
show cartoon, AARNA1039998W_x_LNMAX
color white, AARNA1039998W_x_LNMAX
spectrum b, rainbow, AARNA1039998W_x_LNMAX
set grid_slot, 3, AARNA1039998W_x_LNMAX
center AARNA1039998W_x_LNMAX
orient AARNA1039998W_x_LNMAX
show surface, AARNA1039998W_x_LNMAX
load AARNA1039998W_x_LNMIN.pdb
hide everything, AARNA1039998W_x_LNMIN
show cartoon, AARNA1039998W_x_LNMIN
color white, AARNA1039998W_x_LNMIN
spectrum b, rainbow, AARNA1039998W_x_LNMIN
set grid_slot, 4, AARNA1039998W_x_LNMIN
center AARNA1039998W_x_LNMIN
orient AARNA1039998W_x_LNMIN
show surface, AARNA1039998W_x_LNMIN
set seq_view, 1
set transparency, 0.0
