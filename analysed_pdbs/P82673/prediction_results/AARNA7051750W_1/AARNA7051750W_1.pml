set grid_mode,1
set surface_quality, 0
load AARNA7051750W_1_BP.pdb
hide everything, AARNA7051750W_1_BP
show cartoon, AARNA7051750W_1_BP
color white, AARNA7051750W_1_BP
spectrum b, rainbow, AARNA7051750W_1_BP
set grid_slot, 1, AARNA7051750W_1_BP
center AARNA7051750W_1_BP
orient AARNA7051750W_1_BP
show surface, AARNA7051750W_1_BP
load AARNA7051750W_1_EC.pdb
hide everything, AARNA7051750W_1_EC
show cartoon, AARNA7051750W_1_EC
color white, AARNA7051750W_1_EC
spectrum b, rainbow, AARNA7051750W_1_EC
set grid_slot, 2, AARNA7051750W_1_EC
center AARNA7051750W_1_EC
orient AARNA7051750W_1_EC
show surface, AARNA7051750W_1_EC
load AARNA7051750W_1_LNMAX.pdb
hide everything, AARNA7051750W_1_LNMAX
show cartoon, AARNA7051750W_1_LNMAX
color white, AARNA7051750W_1_LNMAX
spectrum b, rainbow, AARNA7051750W_1_LNMAX
set grid_slot, 3, AARNA7051750W_1_LNMAX
center AARNA7051750W_1_LNMAX
orient AARNA7051750W_1_LNMAX
show surface, AARNA7051750W_1_LNMAX
load AARNA7051750W_1_LNMIN.pdb
hide everything, AARNA7051750W_1_LNMIN
show cartoon, AARNA7051750W_1_LNMIN
color white, AARNA7051750W_1_LNMIN
spectrum b, rainbow, AARNA7051750W_1_LNMIN
set grid_slot, 4, AARNA7051750W_1_LNMIN
center AARNA7051750W_1_LNMIN
orient AARNA7051750W_1_LNMIN
show surface, AARNA7051750W_1_LNMIN
set seq_view, 1
set transparency, 0.0
