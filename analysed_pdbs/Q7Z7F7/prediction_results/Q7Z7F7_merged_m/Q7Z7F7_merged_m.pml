set grid_mode,1
set surface_quality, 0
load AARNA7787928W_m_BP.pdb
hide everything, AARNA7787928W_m_BP
show cartoon, AARNA7787928W_m_BP
color white, AARNA7787928W_m_BP
spectrum b, rainbow, AARNA7787928W_m_BP
set grid_slot, 1, AARNA7787928W_m_BP
center AARNA7787928W_m_BP
orient AARNA7787928W_m_BP
show surface, AARNA7787928W_m_BP
load AARNA7787928W_m_EC.pdb
hide everything, AARNA7787928W_m_EC
show cartoon, AARNA7787928W_m_EC
color white, AARNA7787928W_m_EC
spectrum b, rainbow, AARNA7787928W_m_EC
set grid_slot, 2, AARNA7787928W_m_EC
center AARNA7787928W_m_EC
orient AARNA7787928W_m_EC
show surface, AARNA7787928W_m_EC
load AARNA7787928W_m_LNMAX.pdb
hide everything, AARNA7787928W_m_LNMAX
show cartoon, AARNA7787928W_m_LNMAX
color white, AARNA7787928W_m_LNMAX
spectrum b, rainbow, AARNA7787928W_m_LNMAX
set grid_slot, 3, AARNA7787928W_m_LNMAX
center AARNA7787928W_m_LNMAX
orient AARNA7787928W_m_LNMAX
show surface, AARNA7787928W_m_LNMAX
load AARNA7787928W_m_LNMIN.pdb
hide everything, AARNA7787928W_m_LNMIN
show cartoon, AARNA7787928W_m_LNMIN
color white, AARNA7787928W_m_LNMIN
spectrum b, rainbow, AARNA7787928W_m_LNMIN
set grid_slot, 4, AARNA7787928W_m_LNMIN
center AARNA7787928W_m_LNMIN
orient AARNA7787928W_m_LNMIN
show surface, AARNA7787928W_m_LNMIN
set seq_view, 1
set transparency, 0.0
