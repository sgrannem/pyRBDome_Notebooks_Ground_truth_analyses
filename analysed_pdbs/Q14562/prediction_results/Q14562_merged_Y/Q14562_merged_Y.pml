set grid_mode,1
set surface_quality, 0
load AARNA6988920W_Y_BP.pdb
hide everything, AARNA6988920W_Y_BP
show cartoon, AARNA6988920W_Y_BP
color white, AARNA6988920W_Y_BP
spectrum b, rainbow, AARNA6988920W_Y_BP
set grid_slot, 1, AARNA6988920W_Y_BP
center AARNA6988920W_Y_BP
orient AARNA6988920W_Y_BP
show surface, AARNA6988920W_Y_BP
load AARNA6988920W_Y_EC.pdb
hide everything, AARNA6988920W_Y_EC
show cartoon, AARNA6988920W_Y_EC
color white, AARNA6988920W_Y_EC
spectrum b, rainbow, AARNA6988920W_Y_EC
set grid_slot, 2, AARNA6988920W_Y_EC
center AARNA6988920W_Y_EC
orient AARNA6988920W_Y_EC
show surface, AARNA6988920W_Y_EC
load AARNA6988920W_Y_LNMAX.pdb
hide everything, AARNA6988920W_Y_LNMAX
show cartoon, AARNA6988920W_Y_LNMAX
color white, AARNA6988920W_Y_LNMAX
spectrum b, rainbow, AARNA6988920W_Y_LNMAX
set grid_slot, 3, AARNA6988920W_Y_LNMAX
center AARNA6988920W_Y_LNMAX
orient AARNA6988920W_Y_LNMAX
show surface, AARNA6988920W_Y_LNMAX
load AARNA6988920W_Y_LNMIN.pdb
hide everything, AARNA6988920W_Y_LNMIN
show cartoon, AARNA6988920W_Y_LNMIN
color white, AARNA6988920W_Y_LNMIN
spectrum b, rainbow, AARNA6988920W_Y_LNMIN
set grid_slot, 4, AARNA6988920W_Y_LNMIN
center AARNA6988920W_Y_LNMIN
orient AARNA6988920W_Y_LNMIN
show surface, AARNA6988920W_Y_LNMIN
set seq_view, 1
set transparency, 0.0
