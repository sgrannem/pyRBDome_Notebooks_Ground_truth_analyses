RESNR	RESTYPE	RESCHAIN	RESNR_LIG	RESTYPE_LIG	RESCHAIN_LIG	SIDECHAIN	DIST_H-A	DIST_D-A	DON_ANGLE	PROTISDON	DONORIDX	DONORTYPE	ACCEPTORIDX	ACCEPTORTYPE	LIGCOO	PROTCOO
56	ASN	A	-3	U	C	False	1.94	2.91	169.15	True	424	Nam	3126	O2	(28.813, -6.1, -3.948)	(26.702, -4.32, -4.86)
24	TYR	A	-2	U	C	True	3.22	4.06	149.31	True	157	O3	3140	O3	(25.878, -9.769, -5.877)	(23.17, -7.209, -4.269)
20	GLN	A	-2	U	C	True	1.89	2.87	171.31	True	116	Nam	3146	O2	(22.009, -12.227, -7.185)	(21.071, -10.047, -5.57)
140	ILE	A	-2	U	C	False	2.08	2.91	140.81	True	1097	Nam	3149	O2	(21.602, -16.678, -7.911)	(22.084, -19.401, -8.827)
57	ARG	A	-1	U	C	False	1.92	2.88	164.55	True	432	Nam	3153	O3	(25.754, -7.389, -5.157)	(24.798, -5.402, -7.01)
33	ASP	A	-1	U	C	True	1.89	2.85	167.87	False	3162	O3	232	O3	(29.523, -10.454, 0.713)	(28.196, -12.134, 2.594)
38	GLU	A	-4	A	C	True	2.28	3.05	134.34	False	3107	Npl	278	O2	(34.441, -9.709, 6.304)	(33.964, -9.754, 9.318)
54	LYS	A	-3	U	C	False	1.85	2.84	179.47	False	3127	Nar	407	O2	(30.31, -4.745, -2.925)	(28.276, -2.786, -2.637)
