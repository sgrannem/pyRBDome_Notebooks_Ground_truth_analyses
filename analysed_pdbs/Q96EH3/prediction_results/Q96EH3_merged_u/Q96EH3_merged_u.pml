set grid_mode,1
set surface_quality, 0
load AARNA240111W_u_BP.pdb
hide everything, AARNA240111W_u_BP
show cartoon, AARNA240111W_u_BP
color white, AARNA240111W_u_BP
spectrum b, rainbow, AARNA240111W_u_BP
set grid_slot, 1, AARNA240111W_u_BP
center AARNA240111W_u_BP
orient AARNA240111W_u_BP
show surface, AARNA240111W_u_BP
load AARNA240111W_u_EC.pdb
hide everything, AARNA240111W_u_EC
show cartoon, AARNA240111W_u_EC
color white, AARNA240111W_u_EC
spectrum b, rainbow, AARNA240111W_u_EC
set grid_slot, 2, AARNA240111W_u_EC
center AARNA240111W_u_EC
orient AARNA240111W_u_EC
show surface, AARNA240111W_u_EC
load AARNA240111W_u_LNMAX.pdb
hide everything, AARNA240111W_u_LNMAX
show cartoon, AARNA240111W_u_LNMAX
color white, AARNA240111W_u_LNMAX
spectrum b, rainbow, AARNA240111W_u_LNMAX
set grid_slot, 3, AARNA240111W_u_LNMAX
center AARNA240111W_u_LNMAX
orient AARNA240111W_u_LNMAX
show surface, AARNA240111W_u_LNMAX
load AARNA240111W_u_LNMIN.pdb
hide everything, AARNA240111W_u_LNMIN
show cartoon, AARNA240111W_u_LNMIN
color white, AARNA240111W_u_LNMIN
spectrum b, rainbow, AARNA240111W_u_LNMIN
set grid_slot, 4, AARNA240111W_u_LNMIN
center AARNA240111W_u_LNMIN
orient AARNA240111W_u_LNMIN
show surface, AARNA240111W_u_LNMIN
set seq_view, 1
set transparency, 0.0
