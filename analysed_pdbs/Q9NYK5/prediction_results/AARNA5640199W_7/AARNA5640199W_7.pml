set grid_mode,1
set surface_quality, 0
load AARNA5640199W_7_BP.pdb
hide everything, AARNA5640199W_7_BP
show cartoon, AARNA5640199W_7_BP
color white, AARNA5640199W_7_BP
spectrum b, rainbow, AARNA5640199W_7_BP
set grid_slot, 1, AARNA5640199W_7_BP
center AARNA5640199W_7_BP
orient AARNA5640199W_7_BP
show surface, AARNA5640199W_7_BP
load AARNA5640199W_7_EC.pdb
hide everything, AARNA5640199W_7_EC
show cartoon, AARNA5640199W_7_EC
color white, AARNA5640199W_7_EC
spectrum b, rainbow, AARNA5640199W_7_EC
set grid_slot, 2, AARNA5640199W_7_EC
center AARNA5640199W_7_EC
orient AARNA5640199W_7_EC
show surface, AARNA5640199W_7_EC
load AARNA5640199W_7_LNMAX.pdb
hide everything, AARNA5640199W_7_LNMAX
show cartoon, AARNA5640199W_7_LNMAX
color white, AARNA5640199W_7_LNMAX
spectrum b, rainbow, AARNA5640199W_7_LNMAX
set grid_slot, 3, AARNA5640199W_7_LNMAX
center AARNA5640199W_7_LNMAX
orient AARNA5640199W_7_LNMAX
show surface, AARNA5640199W_7_LNMAX
load AARNA5640199W_7_LNMIN.pdb
hide everything, AARNA5640199W_7_LNMIN
show cartoon, AARNA5640199W_7_LNMIN
color white, AARNA5640199W_7_LNMIN
spectrum b, rainbow, AARNA5640199W_7_LNMIN
set grid_slot, 4, AARNA5640199W_7_LNMIN
center AARNA5640199W_7_LNMIN
orient AARNA5640199W_7_LNMIN
show surface, AARNA5640199W_7_LNMIN
set seq_view, 1
set transparency, 0.0
