set surface_quality, 0
bg_color white
load Q15459_merged_p_aaRNA.pdb,object=aaRNA
hide everything, aaRNA
show cartoon, aaRNA
color white, aaRNA
spectrum b, rainbow, aaRNA
show surface, aaRNA
load Q15459_merged_p_PST_PRNA.pdb,object=PST_PRNA
hide everything, PST_PRNA
show cartoon, PST_PRNA
color white, PST_PRNA
spectrum b, rainbow, PST_PRNA
show surface, PST_PRNA
load Q15459_merged_p_FTMap_distances.pdb,object=FTMap
hide everything, FTMap
show cartoon, FTMap
color white, FTMap
spectrum b, rainbow_rev, FTMap, minimum=2, maximum=4.2
show surface, FTMap
load Q15459_merged_p_RNABindRPlus.pdb,object=RNABindRPlus
hide everything, RNABindRPlus
show cartoon, RNABindRPlus
color white, RNABindRPlus
spectrum b, rainbow, RNABindRPlus
show surface, RNABindRPlus
load Q15459_merged_p_DisoRDPbind.pdb,object=DisoRDPbind
hide everything, DisoRDPbind
show cartoon, DisoRDPbind
color white, DisoRDPbind
spectrum b, rainbow, DisoRDPbind
show surface, DisoRDPbind
load Q15459_merged_p_HydRa.pdb,object=HydRa
hide everything, HydRa
show cartoon, HydRa
color white, HydRa
spectrum b, rainbow, HydRa
show surface, HydRa
load Q15459_merged_p_model_predictions.pdb,object=prediction
hide everything, prediction
show cartoon, prediction
color white, prediction
spectrum b, rainbow, prediction
show surface, prediction
load ../distances_merged/Q15459_plip_merged_all.pdb,object=PLIP
hide everything, PLIP
show cartoon, PLIP
show sticks, PLIPcolor white, PLIP
color white, full_structure
color white, PLIP
spectrum b, rainbow, PLIP
show surface, PLIP
load ../distances_merged/Q15459_merged.pdb,object=RBD
hide everything, RBD
show cartoon, RBD
show sticks, RBDcolor white, RBD
color white, full_structure
color white, RBD
spectrum b, rainbow_rev, RBD, minimum=2, maximum=4.2
show surface, RBD
set seq_view, 1
align all