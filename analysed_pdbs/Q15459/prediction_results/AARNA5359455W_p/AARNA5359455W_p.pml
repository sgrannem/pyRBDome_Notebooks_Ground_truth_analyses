set grid_mode,1
set surface_quality, 0
load AARNA5359455W_p_BP.pdb
hide everything, AARNA5359455W_p_BP
show cartoon, AARNA5359455W_p_BP
color white, AARNA5359455W_p_BP
spectrum b, rainbow, AARNA5359455W_p_BP
set grid_slot, 1, AARNA5359455W_p_BP
center AARNA5359455W_p_BP
orient AARNA5359455W_p_BP
show surface, AARNA5359455W_p_BP
load AARNA5359455W_p_EC.pdb
hide everything, AARNA5359455W_p_EC
show cartoon, AARNA5359455W_p_EC
color white, AARNA5359455W_p_EC
spectrum b, rainbow, AARNA5359455W_p_EC
set grid_slot, 2, AARNA5359455W_p_EC
center AARNA5359455W_p_EC
orient AARNA5359455W_p_EC
show surface, AARNA5359455W_p_EC
load AARNA5359455W_p_LNMAX.pdb
hide everything, AARNA5359455W_p_LNMAX
show cartoon, AARNA5359455W_p_LNMAX
color white, AARNA5359455W_p_LNMAX
spectrum b, rainbow, AARNA5359455W_p_LNMAX
set grid_slot, 3, AARNA5359455W_p_LNMAX
center AARNA5359455W_p_LNMAX
orient AARNA5359455W_p_LNMAX
show surface, AARNA5359455W_p_LNMAX
load AARNA5359455W_p_LNMIN.pdb
hide everything, AARNA5359455W_p_LNMIN
show cartoon, AARNA5359455W_p_LNMIN
color white, AARNA5359455W_p_LNMIN
spectrum b, rainbow, AARNA5359455W_p_LNMIN
set grid_slot, 4, AARNA5359455W_p_LNMIN
center AARNA5359455W_p_LNMIN
orient AARNA5359455W_p_LNMIN
show surface, AARNA5359455W_p_LNMIN
set seq_view, 1
set transparency, 0.0
