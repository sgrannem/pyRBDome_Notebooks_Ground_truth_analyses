set grid_mode,1
set surface_quality, 0
load AARNA9312388W_S_BP.pdb
hide everything, AARNA9312388W_S_BP
show cartoon, AARNA9312388W_S_BP
color white, AARNA9312388W_S_BP
spectrum b, rainbow, AARNA9312388W_S_BP
set grid_slot, 1, AARNA9312388W_S_BP
center AARNA9312388W_S_BP
orient AARNA9312388W_S_BP
show surface, AARNA9312388W_S_BP
load AARNA9312388W_S_EC.pdb
hide everything, AARNA9312388W_S_EC
show cartoon, AARNA9312388W_S_EC
color white, AARNA9312388W_S_EC
spectrum b, rainbow, AARNA9312388W_S_EC
set grid_slot, 2, AARNA9312388W_S_EC
center AARNA9312388W_S_EC
orient AARNA9312388W_S_EC
show surface, AARNA9312388W_S_EC
load AARNA9312388W_S_LNMAX.pdb
hide everything, AARNA9312388W_S_LNMAX
show cartoon, AARNA9312388W_S_LNMAX
color white, AARNA9312388W_S_LNMAX
spectrum b, rainbow, AARNA9312388W_S_LNMAX
set grid_slot, 3, AARNA9312388W_S_LNMAX
center AARNA9312388W_S_LNMAX
orient AARNA9312388W_S_LNMAX
show surface, AARNA9312388W_S_LNMAX
load AARNA9312388W_S_LNMIN.pdb
hide everything, AARNA9312388W_S_LNMIN
show cartoon, AARNA9312388W_S_LNMIN
color white, AARNA9312388W_S_LNMIN
spectrum b, rainbow, AARNA9312388W_S_LNMIN
set grid_slot, 4, AARNA9312388W_S_LNMIN
center AARNA9312388W_S_LNMIN
orient AARNA9312388W_S_LNMIN
show surface, AARNA9312388W_S_LNMIN
set seq_view, 1
set transparency, 0.0
