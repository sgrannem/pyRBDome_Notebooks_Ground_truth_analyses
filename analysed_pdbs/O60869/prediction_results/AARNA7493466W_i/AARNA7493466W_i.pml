set grid_mode,1
set surface_quality, 0
load AARNA7493466W_i_BP.pdb
hide everything, AARNA7493466W_i_BP
show cartoon, AARNA7493466W_i_BP
color white, AARNA7493466W_i_BP
spectrum b, rainbow, AARNA7493466W_i_BP
set grid_slot, 1, AARNA7493466W_i_BP
center AARNA7493466W_i_BP
orient AARNA7493466W_i_BP
show surface, AARNA7493466W_i_BP
load AARNA7493466W_i_EC.pdb
hide everything, AARNA7493466W_i_EC
show cartoon, AARNA7493466W_i_EC
color white, AARNA7493466W_i_EC
spectrum b, rainbow, AARNA7493466W_i_EC
set grid_slot, 2, AARNA7493466W_i_EC
center AARNA7493466W_i_EC
orient AARNA7493466W_i_EC
show surface, AARNA7493466W_i_EC
load AARNA7493466W_i_LNMAX.pdb
hide everything, AARNA7493466W_i_LNMAX
show cartoon, AARNA7493466W_i_LNMAX
color white, AARNA7493466W_i_LNMAX
spectrum b, rainbow, AARNA7493466W_i_LNMAX
set grid_slot, 3, AARNA7493466W_i_LNMAX
center AARNA7493466W_i_LNMAX
orient AARNA7493466W_i_LNMAX
show surface, AARNA7493466W_i_LNMAX
load AARNA7493466W_i_LNMIN.pdb
hide everything, AARNA7493466W_i_LNMIN
show cartoon, AARNA7493466W_i_LNMIN
color white, AARNA7493466W_i_LNMIN
spectrum b, rainbow, AARNA7493466W_i_LNMIN
set grid_slot, 4, AARNA7493466W_i_LNMIN
center AARNA7493466W_i_LNMIN
orient AARNA7493466W_i_LNMIN
show surface, AARNA7493466W_i_LNMIN
set seq_view, 1
set transparency, 0.0
