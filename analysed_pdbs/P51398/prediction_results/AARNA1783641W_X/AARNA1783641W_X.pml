set grid_mode,1
set surface_quality, 0
load AARNA1783641W_X_BP.pdb
hide everything, AARNA1783641W_X_BP
show cartoon, AARNA1783641W_X_BP
color white, AARNA1783641W_X_BP
spectrum b, rainbow, AARNA1783641W_X_BP
set grid_slot, 1, AARNA1783641W_X_BP
center AARNA1783641W_X_BP
orient AARNA1783641W_X_BP
show surface, AARNA1783641W_X_BP
load AARNA1783641W_X_EC.pdb
hide everything, AARNA1783641W_X_EC
show cartoon, AARNA1783641W_X_EC
color white, AARNA1783641W_X_EC
spectrum b, rainbow, AARNA1783641W_X_EC
set grid_slot, 2, AARNA1783641W_X_EC
center AARNA1783641W_X_EC
orient AARNA1783641W_X_EC
show surface, AARNA1783641W_X_EC
load AARNA1783641W_X_LNMAX.pdb
hide everything, AARNA1783641W_X_LNMAX
show cartoon, AARNA1783641W_X_LNMAX
color white, AARNA1783641W_X_LNMAX
spectrum b, rainbow, AARNA1783641W_X_LNMAX
set grid_slot, 3, AARNA1783641W_X_LNMAX
center AARNA1783641W_X_LNMAX
orient AARNA1783641W_X_LNMAX
show surface, AARNA1783641W_X_LNMAX
load AARNA1783641W_X_LNMIN.pdb
hide everything, AARNA1783641W_X_LNMIN
show cartoon, AARNA1783641W_X_LNMIN
color white, AARNA1783641W_X_LNMIN
spectrum b, rainbow, AARNA1783641W_X_LNMIN
set grid_slot, 4, AARNA1783641W_X_LNMIN
center AARNA1783641W_X_LNMIN
orient AARNA1783641W_X_LNMIN
show surface, AARNA1783641W_X_LNMIN
set seq_view, 1
set transparency, 0.0
