set grid_mode,1
set surface_quality, 0
load AARNA8887424W_q_BP.pdb
hide everything, AARNA8887424W_q_BP
show cartoon, AARNA8887424W_q_BP
color white, AARNA8887424W_q_BP
spectrum b, rainbow, AARNA8887424W_q_BP
set grid_slot, 1, AARNA8887424W_q_BP
center AARNA8887424W_q_BP
orient AARNA8887424W_q_BP
show surface, AARNA8887424W_q_BP
load AARNA8887424W_q_EC.pdb
hide everything, AARNA8887424W_q_EC
show cartoon, AARNA8887424W_q_EC
color white, AARNA8887424W_q_EC
spectrum b, rainbow, AARNA8887424W_q_EC
set grid_slot, 2, AARNA8887424W_q_EC
center AARNA8887424W_q_EC
orient AARNA8887424W_q_EC
show surface, AARNA8887424W_q_EC
load AARNA8887424W_q_LNMAX.pdb
hide everything, AARNA8887424W_q_LNMAX
show cartoon, AARNA8887424W_q_LNMAX
color white, AARNA8887424W_q_LNMAX
spectrum b, rainbow, AARNA8887424W_q_LNMAX
set grid_slot, 3, AARNA8887424W_q_LNMAX
center AARNA8887424W_q_LNMAX
orient AARNA8887424W_q_LNMAX
show surface, AARNA8887424W_q_LNMAX
load AARNA8887424W_q_LNMIN.pdb
hide everything, AARNA8887424W_q_LNMIN
show cartoon, AARNA8887424W_q_LNMIN
color white, AARNA8887424W_q_LNMIN
spectrum b, rainbow, AARNA8887424W_q_LNMIN
set grid_slot, 4, AARNA8887424W_q_LNMIN
center AARNA8887424W_q_LNMIN
orient AARNA8887424W_q_LNMIN
show surface, AARNA8887424W_q_LNMIN
set seq_view, 1
set transparency, 0.0
