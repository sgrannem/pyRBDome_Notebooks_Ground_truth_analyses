set grid_mode,1
set surface_quality, 0
load AARNA8431893W_V_BP.pdb
hide everything, AARNA8431893W_V_BP
show cartoon, AARNA8431893W_V_BP
color white, AARNA8431893W_V_BP
spectrum b, rainbow, AARNA8431893W_V_BP
set grid_slot, 1, AARNA8431893W_V_BP
center AARNA8431893W_V_BP
orient AARNA8431893W_V_BP
show surface, AARNA8431893W_V_BP
load AARNA8431893W_V_EC.pdb
hide everything, AARNA8431893W_V_EC
show cartoon, AARNA8431893W_V_EC
color white, AARNA8431893W_V_EC
spectrum b, rainbow, AARNA8431893W_V_EC
set grid_slot, 2, AARNA8431893W_V_EC
center AARNA8431893W_V_EC
orient AARNA8431893W_V_EC
show surface, AARNA8431893W_V_EC
load AARNA8431893W_V_LNMAX.pdb
hide everything, AARNA8431893W_V_LNMAX
show cartoon, AARNA8431893W_V_LNMAX
color white, AARNA8431893W_V_LNMAX
spectrum b, rainbow, AARNA8431893W_V_LNMAX
set grid_slot, 3, AARNA8431893W_V_LNMAX
center AARNA8431893W_V_LNMAX
orient AARNA8431893W_V_LNMAX
show surface, AARNA8431893W_V_LNMAX
load AARNA8431893W_V_LNMIN.pdb
hide everything, AARNA8431893W_V_LNMIN
show cartoon, AARNA8431893W_V_LNMIN
color white, AARNA8431893W_V_LNMIN
spectrum b, rainbow, AARNA8431893W_V_LNMIN
set grid_slot, 4, AARNA8431893W_V_LNMIN
center AARNA8431893W_V_LNMIN
orient AARNA8431893W_V_LNMIN
show surface, AARNA8431893W_V_LNMIN
set seq_view, 1
set transparency, 0.0
