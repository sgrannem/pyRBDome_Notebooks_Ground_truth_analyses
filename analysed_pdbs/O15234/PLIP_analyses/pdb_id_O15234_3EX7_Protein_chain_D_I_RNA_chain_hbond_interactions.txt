RESNR	RESTYPE	RESCHAIN	RESNR_LIG	RESTYPE_LIG	RESCHAIN_LIG	SIDECHAIN	DIST_H-A	DIST_D-A	DON_ANGLE	PROTISDON	DONORIDX	DONORTYPE	ACCEPTORIDX	ACCEPTORTYPE	LIGCOO	PROTCOO
177	LYS	I	2	U	F	True	2.26	2.96	124.27	True	586	N3	1059	O2	(-17.7, 31.654, 32.293)	(-16.285, 34.202, 31.794)
169	HIS	I	5	U	F	False	1.94	2.90	166.13	True	508	Nam	1119	O2	(-20.937, 34.146, 46.429)	(-19.157, 35.509, 48.272)
167	PRO	I	6	U	F	False	1.54	2.48	157.51	False	1137	Nar	495	O2	(-23.486, 37.306, 47.075)	(-22.227, 39.403, 47.494)
169	HIS	I	5	U	F	True	2.84	3.78	158.95	False	1117	Nar	517	Nar	(-22.674, 32.999, 47.331)	(-22.304, 33.556, 51.049)
169	HIS	I	6	U	F	True	3.02	3.70	127.80	False	1132	O3	517	Nar	(-25.793, 34.377, 50.134)	(-22.304, 33.556, 51.049)
177	LYS	I	1	U	F	True	2.83	3.58	134.25	False	1025	O3	586	N3	(-16.718, 36.342, 28.961)	(-16.285, 34.202, 31.794)
