set grid_mode,1
set surface_quality, 0
load AARNA4427083W_h_BP.pdb
hide everything, AARNA4427083W_h_BP
show cartoon, AARNA4427083W_h_BP
color white, AARNA4427083W_h_BP
spectrum b, rainbow, AARNA4427083W_h_BP
set grid_slot, 1, AARNA4427083W_h_BP
center AARNA4427083W_h_BP
orient AARNA4427083W_h_BP
show surface, AARNA4427083W_h_BP
load AARNA4427083W_h_EC.pdb
hide everything, AARNA4427083W_h_EC
show cartoon, AARNA4427083W_h_EC
color white, AARNA4427083W_h_EC
spectrum b, rainbow, AARNA4427083W_h_EC
set grid_slot, 2, AARNA4427083W_h_EC
center AARNA4427083W_h_EC
orient AARNA4427083W_h_EC
show surface, AARNA4427083W_h_EC
load AARNA4427083W_h_LNMAX.pdb
hide everything, AARNA4427083W_h_LNMAX
show cartoon, AARNA4427083W_h_LNMAX
color white, AARNA4427083W_h_LNMAX
spectrum b, rainbow, AARNA4427083W_h_LNMAX
set grid_slot, 3, AARNA4427083W_h_LNMAX
center AARNA4427083W_h_LNMAX
orient AARNA4427083W_h_LNMAX
show surface, AARNA4427083W_h_LNMAX
load AARNA4427083W_h_LNMIN.pdb
hide everything, AARNA4427083W_h_LNMIN
show cartoon, AARNA4427083W_h_LNMIN
color white, AARNA4427083W_h_LNMIN
spectrum b, rainbow, AARNA4427083W_h_LNMIN
set grid_slot, 4, AARNA4427083W_h_LNMIN
center AARNA4427083W_h_LNMIN
orient AARNA4427083W_h_LNMIN
show surface, AARNA4427083W_h_LNMIN
set seq_view, 1
set transparency, 0.0
