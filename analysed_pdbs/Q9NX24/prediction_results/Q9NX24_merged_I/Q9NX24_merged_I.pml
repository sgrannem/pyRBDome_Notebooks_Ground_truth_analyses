set grid_mode,1
set surface_quality, 0
load AARNA6008355W_I_BP.pdb
hide everything, AARNA6008355W_I_BP
show cartoon, AARNA6008355W_I_BP
color white, AARNA6008355W_I_BP
spectrum b, rainbow, AARNA6008355W_I_BP
set grid_slot, 1, AARNA6008355W_I_BP
center AARNA6008355W_I_BP
orient AARNA6008355W_I_BP
show surface, AARNA6008355W_I_BP
load AARNA6008355W_I_EC.pdb
hide everything, AARNA6008355W_I_EC
show cartoon, AARNA6008355W_I_EC
color white, AARNA6008355W_I_EC
spectrum b, rainbow, AARNA6008355W_I_EC
set grid_slot, 2, AARNA6008355W_I_EC
center AARNA6008355W_I_EC
orient AARNA6008355W_I_EC
show surface, AARNA6008355W_I_EC
load AARNA6008355W_I_LNMAX.pdb
hide everything, AARNA6008355W_I_LNMAX
show cartoon, AARNA6008355W_I_LNMAX
color white, AARNA6008355W_I_LNMAX
spectrum b, rainbow, AARNA6008355W_I_LNMAX
set grid_slot, 3, AARNA6008355W_I_LNMAX
center AARNA6008355W_I_LNMAX
orient AARNA6008355W_I_LNMAX
show surface, AARNA6008355W_I_LNMAX
load AARNA6008355W_I_LNMIN.pdb
hide everything, AARNA6008355W_I_LNMIN
show cartoon, AARNA6008355W_I_LNMIN
color white, AARNA6008355W_I_LNMIN
spectrum b, rainbow, AARNA6008355W_I_LNMIN
set grid_slot, 4, AARNA6008355W_I_LNMIN
center AARNA6008355W_I_LNMIN
orient AARNA6008355W_I_LNMIN
show surface, AARNA6008355W_I_LNMIN
set seq_view, 1
set transparency, 0.0
