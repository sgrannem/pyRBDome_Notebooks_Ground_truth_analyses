set grid_mode,1
set surface_quality, 0
load AARNA5649859W_H_BP.pdb
hide everything, AARNA5649859W_H_BP
show cartoon, AARNA5649859W_H_BP
color white, AARNA5649859W_H_BP
spectrum b, rainbow, AARNA5649859W_H_BP
set grid_slot, 1, AARNA5649859W_H_BP
center AARNA5649859W_H_BP
orient AARNA5649859W_H_BP
show surface, AARNA5649859W_H_BP
load AARNA5649859W_H_EC.pdb
hide everything, AARNA5649859W_H_EC
show cartoon, AARNA5649859W_H_EC
color white, AARNA5649859W_H_EC
spectrum b, rainbow, AARNA5649859W_H_EC
set grid_slot, 2, AARNA5649859W_H_EC
center AARNA5649859W_H_EC
orient AARNA5649859W_H_EC
show surface, AARNA5649859W_H_EC
load AARNA5649859W_H_LNMAX.pdb
hide everything, AARNA5649859W_H_LNMAX
show cartoon, AARNA5649859W_H_LNMAX
color white, AARNA5649859W_H_LNMAX
spectrum b, rainbow, AARNA5649859W_H_LNMAX
set grid_slot, 3, AARNA5649859W_H_LNMAX
center AARNA5649859W_H_LNMAX
orient AARNA5649859W_H_LNMAX
show surface, AARNA5649859W_H_LNMAX
load AARNA5649859W_H_LNMIN.pdb
hide everything, AARNA5649859W_H_LNMIN
show cartoon, AARNA5649859W_H_LNMIN
color white, AARNA5649859W_H_LNMIN
spectrum b, rainbow, AARNA5649859W_H_LNMIN
set grid_slot, 4, AARNA5649859W_H_LNMIN
center AARNA5649859W_H_LNMIN
orient AARNA5649859W_H_LNMIN
show surface, AARNA5649859W_H_LNMIN
set seq_view, 1
set transparency, 0.0
