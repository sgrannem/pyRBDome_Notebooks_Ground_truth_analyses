set grid_mode,1
set surface_quality, 0
load AARNA1634525W_L_BP.pdb
hide everything, AARNA1634525W_L_BP
show cartoon, AARNA1634525W_L_BP
color white, AARNA1634525W_L_BP
spectrum b, rainbow, AARNA1634525W_L_BP
set grid_slot, 1, AARNA1634525W_L_BP
center AARNA1634525W_L_BP
orient AARNA1634525W_L_BP
show surface, AARNA1634525W_L_BP
load AARNA1634525W_L_EC.pdb
hide everything, AARNA1634525W_L_EC
show cartoon, AARNA1634525W_L_EC
color white, AARNA1634525W_L_EC
spectrum b, rainbow, AARNA1634525W_L_EC
set grid_slot, 2, AARNA1634525W_L_EC
center AARNA1634525W_L_EC
orient AARNA1634525W_L_EC
show surface, AARNA1634525W_L_EC
load AARNA1634525W_L_LNMAX.pdb
hide everything, AARNA1634525W_L_LNMAX
show cartoon, AARNA1634525W_L_LNMAX
color white, AARNA1634525W_L_LNMAX
spectrum b, rainbow, AARNA1634525W_L_LNMAX
set grid_slot, 3, AARNA1634525W_L_LNMAX
center AARNA1634525W_L_LNMAX
orient AARNA1634525W_L_LNMAX
show surface, AARNA1634525W_L_LNMAX
load AARNA1634525W_L_LNMIN.pdb
hide everything, AARNA1634525W_L_LNMIN
show cartoon, AARNA1634525W_L_LNMIN
color white, AARNA1634525W_L_LNMIN
spectrum b, rainbow, AARNA1634525W_L_LNMIN
set grid_slot, 4, AARNA1634525W_L_LNMIN
center AARNA1634525W_L_LNMIN
orient AARNA1634525W_L_LNMIN
show surface, AARNA1634525W_L_LNMIN
set seq_view, 1
set transparency, 0.0
