set grid_mode,1
set surface_quality, 0
load AARNA606412W_A_BP.pdb
hide everything, AARNA606412W_A_BP
show cartoon, AARNA606412W_A_BP
color white, AARNA606412W_A_BP
spectrum b, rainbow, AARNA606412W_A_BP
set grid_slot, 1, AARNA606412W_A_BP
center AARNA606412W_A_BP
orient AARNA606412W_A_BP
show surface, AARNA606412W_A_BP
load AARNA606412W_A_EC.pdb
hide everything, AARNA606412W_A_EC
show cartoon, AARNA606412W_A_EC
color white, AARNA606412W_A_EC
spectrum b, rainbow, AARNA606412W_A_EC
set grid_slot, 2, AARNA606412W_A_EC
center AARNA606412W_A_EC
orient AARNA606412W_A_EC
show surface, AARNA606412W_A_EC
load AARNA606412W_A_LNMAX.pdb
hide everything, AARNA606412W_A_LNMAX
show cartoon, AARNA606412W_A_LNMAX
color white, AARNA606412W_A_LNMAX
spectrum b, rainbow, AARNA606412W_A_LNMAX
set grid_slot, 3, AARNA606412W_A_LNMAX
center AARNA606412W_A_LNMAX
orient AARNA606412W_A_LNMAX
show surface, AARNA606412W_A_LNMAX
load AARNA606412W_A_LNMIN.pdb
hide everything, AARNA606412W_A_LNMIN
show cartoon, AARNA606412W_A_LNMIN
color white, AARNA606412W_A_LNMIN
spectrum b, rainbow, AARNA606412W_A_LNMIN
set grid_slot, 4, AARNA606412W_A_LNMIN
center AARNA606412W_A_LNMIN
orient AARNA606412W_A_LNMIN
show surface, AARNA606412W_A_LNMIN
set seq_view, 1
set transparency, 0.0
