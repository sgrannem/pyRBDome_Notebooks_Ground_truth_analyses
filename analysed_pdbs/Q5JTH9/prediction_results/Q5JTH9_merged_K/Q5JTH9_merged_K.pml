set grid_mode,1
set surface_quality, 0
load AARNA6165033W_K_BP.pdb
hide everything, AARNA6165033W_K_BP
show cartoon, AARNA6165033W_K_BP
color white, AARNA6165033W_K_BP
spectrum b, rainbow, AARNA6165033W_K_BP
set grid_slot, 1, AARNA6165033W_K_BP
center AARNA6165033W_K_BP
orient AARNA6165033W_K_BP
show surface, AARNA6165033W_K_BP
load AARNA6165033W_K_EC.pdb
hide everything, AARNA6165033W_K_EC
show cartoon, AARNA6165033W_K_EC
color white, AARNA6165033W_K_EC
spectrum b, rainbow, AARNA6165033W_K_EC
set grid_slot, 2, AARNA6165033W_K_EC
center AARNA6165033W_K_EC
orient AARNA6165033W_K_EC
show surface, AARNA6165033W_K_EC
load AARNA6165033W_K_LNMAX.pdb
hide everything, AARNA6165033W_K_LNMAX
show cartoon, AARNA6165033W_K_LNMAX
color white, AARNA6165033W_K_LNMAX
spectrum b, rainbow, AARNA6165033W_K_LNMAX
set grid_slot, 3, AARNA6165033W_K_LNMAX
center AARNA6165033W_K_LNMAX
orient AARNA6165033W_K_LNMAX
show surface, AARNA6165033W_K_LNMAX
load AARNA6165033W_K_LNMIN.pdb
hide everything, AARNA6165033W_K_LNMIN
show cartoon, AARNA6165033W_K_LNMIN
color white, AARNA6165033W_K_LNMIN
spectrum b, rainbow, AARNA6165033W_K_LNMIN
set grid_slot, 4, AARNA6165033W_K_LNMIN
center AARNA6165033W_K_LNMIN
orient AARNA6165033W_K_LNMIN
show surface, AARNA6165033W_K_LNMIN
set seq_view, 1
set transparency, 0.0
