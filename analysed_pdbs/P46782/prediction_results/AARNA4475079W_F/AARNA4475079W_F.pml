set grid_mode,1
set surface_quality, 0
load AARNA4475079W_F_BP.pdb
hide everything, AARNA4475079W_F_BP
show cartoon, AARNA4475079W_F_BP
color white, AARNA4475079W_F_BP
spectrum b, rainbow, AARNA4475079W_F_BP
set grid_slot, 1, AARNA4475079W_F_BP
center AARNA4475079W_F_BP
orient AARNA4475079W_F_BP
show surface, AARNA4475079W_F_BP
load AARNA4475079W_F_EC.pdb
hide everything, AARNA4475079W_F_EC
show cartoon, AARNA4475079W_F_EC
color white, AARNA4475079W_F_EC
spectrum b, rainbow, AARNA4475079W_F_EC
set grid_slot, 2, AARNA4475079W_F_EC
center AARNA4475079W_F_EC
orient AARNA4475079W_F_EC
show surface, AARNA4475079W_F_EC
load AARNA4475079W_F_LNMAX.pdb
hide everything, AARNA4475079W_F_LNMAX
show cartoon, AARNA4475079W_F_LNMAX
color white, AARNA4475079W_F_LNMAX
spectrum b, rainbow, AARNA4475079W_F_LNMAX
set grid_slot, 3, AARNA4475079W_F_LNMAX
center AARNA4475079W_F_LNMAX
orient AARNA4475079W_F_LNMAX
show surface, AARNA4475079W_F_LNMAX
load AARNA4475079W_F_LNMIN.pdb
hide everything, AARNA4475079W_F_LNMIN
show cartoon, AARNA4475079W_F_LNMIN
color white, AARNA4475079W_F_LNMIN
spectrum b, rainbow, AARNA4475079W_F_LNMIN
set grid_slot, 4, AARNA4475079W_F_LNMIN
center AARNA4475079W_F_LNMIN
orient AARNA4475079W_F_LNMIN
show surface, AARNA4475079W_F_LNMIN
set seq_view, 1
set transparency, 0.0
