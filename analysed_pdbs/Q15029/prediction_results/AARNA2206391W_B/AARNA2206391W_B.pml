set grid_mode,1
set surface_quality, 0
load AARNA2206391W_B_BP.pdb
hide everything, AARNA2206391W_B_BP
show cartoon, AARNA2206391W_B_BP
color white, AARNA2206391W_B_BP
spectrum b, rainbow, AARNA2206391W_B_BP
set grid_slot, 1, AARNA2206391W_B_BP
center AARNA2206391W_B_BP
orient AARNA2206391W_B_BP
show surface, AARNA2206391W_B_BP
load AARNA2206391W_B_EC.pdb
hide everything, AARNA2206391W_B_EC
show cartoon, AARNA2206391W_B_EC
color white, AARNA2206391W_B_EC
spectrum b, rainbow, AARNA2206391W_B_EC
set grid_slot, 2, AARNA2206391W_B_EC
center AARNA2206391W_B_EC
orient AARNA2206391W_B_EC
show surface, AARNA2206391W_B_EC
load AARNA2206391W_B_LNMAX.pdb
hide everything, AARNA2206391W_B_LNMAX
show cartoon, AARNA2206391W_B_LNMAX
color white, AARNA2206391W_B_LNMAX
spectrum b, rainbow, AARNA2206391W_B_LNMAX
set grid_slot, 3, AARNA2206391W_B_LNMAX
center AARNA2206391W_B_LNMAX
orient AARNA2206391W_B_LNMAX
show surface, AARNA2206391W_B_LNMAX
load AARNA2206391W_B_LNMIN.pdb
hide everything, AARNA2206391W_B_LNMIN
show cartoon, AARNA2206391W_B_LNMIN
color white, AARNA2206391W_B_LNMIN
spectrum b, rainbow, AARNA2206391W_B_LNMIN
set grid_slot, 4, AARNA2206391W_B_LNMIN
center AARNA2206391W_B_LNMIN
orient AARNA2206391W_B_LNMIN
show surface, AARNA2206391W_B_LNMIN
set seq_view, 1
set transparency, 0.0
