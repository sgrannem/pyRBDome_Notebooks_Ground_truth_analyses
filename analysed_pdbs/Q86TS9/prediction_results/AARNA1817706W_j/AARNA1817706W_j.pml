set grid_mode,1
set surface_quality, 0
load AARNA1817706W_j_BP.pdb
hide everything, AARNA1817706W_j_BP
show cartoon, AARNA1817706W_j_BP
color white, AARNA1817706W_j_BP
spectrum b, rainbow, AARNA1817706W_j_BP
set grid_slot, 1, AARNA1817706W_j_BP
center AARNA1817706W_j_BP
orient AARNA1817706W_j_BP
show surface, AARNA1817706W_j_BP
load AARNA1817706W_j_EC.pdb
hide everything, AARNA1817706W_j_EC
show cartoon, AARNA1817706W_j_EC
color white, AARNA1817706W_j_EC
spectrum b, rainbow, AARNA1817706W_j_EC
set grid_slot, 2, AARNA1817706W_j_EC
center AARNA1817706W_j_EC
orient AARNA1817706W_j_EC
show surface, AARNA1817706W_j_EC
load AARNA1817706W_j_LNMAX.pdb
hide everything, AARNA1817706W_j_LNMAX
show cartoon, AARNA1817706W_j_LNMAX
color white, AARNA1817706W_j_LNMAX
spectrum b, rainbow, AARNA1817706W_j_LNMAX
set grid_slot, 3, AARNA1817706W_j_LNMAX
center AARNA1817706W_j_LNMAX
orient AARNA1817706W_j_LNMAX
show surface, AARNA1817706W_j_LNMAX
load AARNA1817706W_j_LNMIN.pdb
hide everything, AARNA1817706W_j_LNMIN
show cartoon, AARNA1817706W_j_LNMIN
color white, AARNA1817706W_j_LNMIN
spectrum b, rainbow, AARNA1817706W_j_LNMIN
set grid_slot, 4, AARNA1817706W_j_LNMIN
center AARNA1817706W_j_LNMIN
orient AARNA1817706W_j_LNMIN
show surface, AARNA1817706W_j_LNMIN
set seq_view, 1
set transparency, 0.0
