set grid_mode,1
set surface_quality, 0
load AARNA1074262W_b_BP.pdb
hide everything, AARNA1074262W_b_BP
show cartoon, AARNA1074262W_b_BP
color white, AARNA1074262W_b_BP
spectrum b, rainbow, AARNA1074262W_b_BP
set grid_slot, 1, AARNA1074262W_b_BP
center AARNA1074262W_b_BP
orient AARNA1074262W_b_BP
show surface, AARNA1074262W_b_BP
load AARNA1074262W_b_EC.pdb
hide everything, AARNA1074262W_b_EC
show cartoon, AARNA1074262W_b_EC
color white, AARNA1074262W_b_EC
spectrum b, rainbow, AARNA1074262W_b_EC
set grid_slot, 2, AARNA1074262W_b_EC
center AARNA1074262W_b_EC
orient AARNA1074262W_b_EC
show surface, AARNA1074262W_b_EC
load AARNA1074262W_b_LNMAX.pdb
hide everything, AARNA1074262W_b_LNMAX
show cartoon, AARNA1074262W_b_LNMAX
color white, AARNA1074262W_b_LNMAX
spectrum b, rainbow, AARNA1074262W_b_LNMAX
set grid_slot, 3, AARNA1074262W_b_LNMAX
center AARNA1074262W_b_LNMAX
orient AARNA1074262W_b_LNMAX
show surface, AARNA1074262W_b_LNMAX
load AARNA1074262W_b_LNMIN.pdb
hide everything, AARNA1074262W_b_LNMIN
show cartoon, AARNA1074262W_b_LNMIN
color white, AARNA1074262W_b_LNMIN
spectrum b, rainbow, AARNA1074262W_b_LNMIN
set grid_slot, 4, AARNA1074262W_b_LNMIN
center AARNA1074262W_b_LNMIN
orient AARNA1074262W_b_LNMIN
show surface, AARNA1074262W_b_LNMIN
set seq_view, 1
set transparency, 0.0
