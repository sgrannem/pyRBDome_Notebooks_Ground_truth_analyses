set grid_mode,1
set surface_quality, 0
load AARNA183423W_x_BP.pdb
hide everything, AARNA183423W_x_BP
show cartoon, AARNA183423W_x_BP
color white, AARNA183423W_x_BP
spectrum b, rainbow, AARNA183423W_x_BP
set grid_slot, 1, AARNA183423W_x_BP
center AARNA183423W_x_BP
orient AARNA183423W_x_BP
show surface, AARNA183423W_x_BP
load AARNA183423W_x_EC.pdb
hide everything, AARNA183423W_x_EC
show cartoon, AARNA183423W_x_EC
color white, AARNA183423W_x_EC
spectrum b, rainbow, AARNA183423W_x_EC
set grid_slot, 2, AARNA183423W_x_EC
center AARNA183423W_x_EC
orient AARNA183423W_x_EC
show surface, AARNA183423W_x_EC
load AARNA183423W_x_LNMAX.pdb
hide everything, AARNA183423W_x_LNMAX
show cartoon, AARNA183423W_x_LNMAX
color white, AARNA183423W_x_LNMAX
spectrum b, rainbow, AARNA183423W_x_LNMAX
set grid_slot, 3, AARNA183423W_x_LNMAX
center AARNA183423W_x_LNMAX
orient AARNA183423W_x_LNMAX
show surface, AARNA183423W_x_LNMAX
load AARNA183423W_x_LNMIN.pdb
hide everything, AARNA183423W_x_LNMIN
show cartoon, AARNA183423W_x_LNMIN
color white, AARNA183423W_x_LNMIN
spectrum b, rainbow, AARNA183423W_x_LNMIN
set grid_slot, 4, AARNA183423W_x_LNMIN
center AARNA183423W_x_LNMIN
orient AARNA183423W_x_LNMIN
show surface, AARNA183423W_x_LNMIN
set seq_view, 1
set transparency, 0.0
