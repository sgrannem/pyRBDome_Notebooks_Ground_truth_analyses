set grid_mode,1
set surface_quality, 0
load AARNA7235871W_S_BP.pdb
hide everything, AARNA7235871W_S_BP
show cartoon, AARNA7235871W_S_BP
color white, AARNA7235871W_S_BP
spectrum b, rainbow, AARNA7235871W_S_BP
set grid_slot, 1, AARNA7235871W_S_BP
center AARNA7235871W_S_BP
orient AARNA7235871W_S_BP
show surface, AARNA7235871W_S_BP
load AARNA7235871W_S_EC.pdb
hide everything, AARNA7235871W_S_EC
show cartoon, AARNA7235871W_S_EC
color white, AARNA7235871W_S_EC
spectrum b, rainbow, AARNA7235871W_S_EC
set grid_slot, 2, AARNA7235871W_S_EC
center AARNA7235871W_S_EC
orient AARNA7235871W_S_EC
show surface, AARNA7235871W_S_EC
load AARNA7235871W_S_LNMAX.pdb
hide everything, AARNA7235871W_S_LNMAX
show cartoon, AARNA7235871W_S_LNMAX
color white, AARNA7235871W_S_LNMAX
spectrum b, rainbow, AARNA7235871W_S_LNMAX
set grid_slot, 3, AARNA7235871W_S_LNMAX
center AARNA7235871W_S_LNMAX
orient AARNA7235871W_S_LNMAX
show surface, AARNA7235871W_S_LNMAX
load AARNA7235871W_S_LNMIN.pdb
hide everything, AARNA7235871W_S_LNMIN
show cartoon, AARNA7235871W_S_LNMIN
color white, AARNA7235871W_S_LNMIN
spectrum b, rainbow, AARNA7235871W_S_LNMIN
set grid_slot, 4, AARNA7235871W_S_LNMIN
center AARNA7235871W_S_LNMIN
orient AARNA7235871W_S_LNMIN
show surface, AARNA7235871W_S_LNMIN
set seq_view, 1
set transparency, 0.0
