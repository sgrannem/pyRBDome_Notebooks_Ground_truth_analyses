set grid_mode,1
set surface_quality, 0
load AARNA6330873W_H_BP.pdb
hide everything, AARNA6330873W_H_BP
show cartoon, AARNA6330873W_H_BP
color white, AARNA6330873W_H_BP
spectrum b, rainbow, AARNA6330873W_H_BP
set grid_slot, 1, AARNA6330873W_H_BP
center AARNA6330873W_H_BP
orient AARNA6330873W_H_BP
show surface, AARNA6330873W_H_BP
load AARNA6330873W_H_EC.pdb
hide everything, AARNA6330873W_H_EC
show cartoon, AARNA6330873W_H_EC
color white, AARNA6330873W_H_EC
spectrum b, rainbow, AARNA6330873W_H_EC
set grid_slot, 2, AARNA6330873W_H_EC
center AARNA6330873W_H_EC
orient AARNA6330873W_H_EC
show surface, AARNA6330873W_H_EC
load AARNA6330873W_H_LNMAX.pdb
hide everything, AARNA6330873W_H_LNMAX
show cartoon, AARNA6330873W_H_LNMAX
color white, AARNA6330873W_H_LNMAX
spectrum b, rainbow, AARNA6330873W_H_LNMAX
set grid_slot, 3, AARNA6330873W_H_LNMAX
center AARNA6330873W_H_LNMAX
orient AARNA6330873W_H_LNMAX
show surface, AARNA6330873W_H_LNMAX
load AARNA6330873W_H_LNMIN.pdb
hide everything, AARNA6330873W_H_LNMIN
show cartoon, AARNA6330873W_H_LNMIN
color white, AARNA6330873W_H_LNMIN
spectrum b, rainbow, AARNA6330873W_H_LNMIN
set grid_slot, 4, AARNA6330873W_H_LNMIN
center AARNA6330873W_H_LNMIN
orient AARNA6330873W_H_LNMIN
show surface, AARNA6330873W_H_LNMIN
set seq_view, 1
set transparency, 0.0
