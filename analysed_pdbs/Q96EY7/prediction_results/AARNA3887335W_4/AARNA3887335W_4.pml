set grid_mode,1
set surface_quality, 0
load AARNA3887335W_4_BP.pdb
hide everything, AARNA3887335W_4_BP
show cartoon, AARNA3887335W_4_BP
color white, AARNA3887335W_4_BP
spectrum b, rainbow, AARNA3887335W_4_BP
set grid_slot, 1, AARNA3887335W_4_BP
center AARNA3887335W_4_BP
orient AARNA3887335W_4_BP
show surface, AARNA3887335W_4_BP
load AARNA3887335W_4_EC.pdb
hide everything, AARNA3887335W_4_EC
show cartoon, AARNA3887335W_4_EC
color white, AARNA3887335W_4_EC
spectrum b, rainbow, AARNA3887335W_4_EC
set grid_slot, 2, AARNA3887335W_4_EC
center AARNA3887335W_4_EC
orient AARNA3887335W_4_EC
show surface, AARNA3887335W_4_EC
load AARNA3887335W_4_LNMAX.pdb
hide everything, AARNA3887335W_4_LNMAX
show cartoon, AARNA3887335W_4_LNMAX
color white, AARNA3887335W_4_LNMAX
spectrum b, rainbow, AARNA3887335W_4_LNMAX
set grid_slot, 3, AARNA3887335W_4_LNMAX
center AARNA3887335W_4_LNMAX
orient AARNA3887335W_4_LNMAX
show surface, AARNA3887335W_4_LNMAX
load AARNA3887335W_4_LNMIN.pdb
hide everything, AARNA3887335W_4_LNMIN
show cartoon, AARNA3887335W_4_LNMIN
color white, AARNA3887335W_4_LNMIN
spectrum b, rainbow, AARNA3887335W_4_LNMIN
set grid_slot, 4, AARNA3887335W_4_LNMIN
center AARNA3887335W_4_LNMIN
orient AARNA3887335W_4_LNMIN
show surface, AARNA3887335W_4_LNMIN
set seq_view, 1
set transparency, 0.0
