set grid_mode,1
set surface_quality, 0
load AARNA1318926W_Z_BP.pdb
hide everything, AARNA1318926W_Z_BP
show cartoon, AARNA1318926W_Z_BP
color white, AARNA1318926W_Z_BP
spectrum b, rainbow, AARNA1318926W_Z_BP
set grid_slot, 1, AARNA1318926W_Z_BP
center AARNA1318926W_Z_BP
orient AARNA1318926W_Z_BP
show surface, AARNA1318926W_Z_BP
load AARNA1318926W_Z_EC.pdb
hide everything, AARNA1318926W_Z_EC
show cartoon, AARNA1318926W_Z_EC
color white, AARNA1318926W_Z_EC
spectrum b, rainbow, AARNA1318926W_Z_EC
set grid_slot, 2, AARNA1318926W_Z_EC
center AARNA1318926W_Z_EC
orient AARNA1318926W_Z_EC
show surface, AARNA1318926W_Z_EC
load AARNA1318926W_Z_LNMAX.pdb
hide everything, AARNA1318926W_Z_LNMAX
show cartoon, AARNA1318926W_Z_LNMAX
color white, AARNA1318926W_Z_LNMAX
spectrum b, rainbow, AARNA1318926W_Z_LNMAX
set grid_slot, 3, AARNA1318926W_Z_LNMAX
center AARNA1318926W_Z_LNMAX
orient AARNA1318926W_Z_LNMAX
show surface, AARNA1318926W_Z_LNMAX
load AARNA1318926W_Z_LNMIN.pdb
hide everything, AARNA1318926W_Z_LNMIN
show cartoon, AARNA1318926W_Z_LNMIN
color white, AARNA1318926W_Z_LNMIN
spectrum b, rainbow, AARNA1318926W_Z_LNMIN
set grid_slot, 4, AARNA1318926W_Z_LNMIN
center AARNA1318926W_Z_LNMIN
orient AARNA1318926W_Z_LNMIN
show surface, AARNA1318926W_Z_LNMIN
set seq_view, 1
set transparency, 0.0
