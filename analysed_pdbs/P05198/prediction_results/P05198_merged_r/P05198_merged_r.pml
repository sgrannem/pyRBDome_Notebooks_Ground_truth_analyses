set grid_mode,1
set surface_quality, 0
load AARNA2125386W_r_BP.pdb
hide everything, AARNA2125386W_r_BP
show cartoon, AARNA2125386W_r_BP
color white, AARNA2125386W_r_BP
spectrum b, rainbow, AARNA2125386W_r_BP
set grid_slot, 1, AARNA2125386W_r_BP
center AARNA2125386W_r_BP
orient AARNA2125386W_r_BP
show surface, AARNA2125386W_r_BP
load AARNA2125386W_r_EC.pdb
hide everything, AARNA2125386W_r_EC
show cartoon, AARNA2125386W_r_EC
color white, AARNA2125386W_r_EC
spectrum b, rainbow, AARNA2125386W_r_EC
set grid_slot, 2, AARNA2125386W_r_EC
center AARNA2125386W_r_EC
orient AARNA2125386W_r_EC
show surface, AARNA2125386W_r_EC
load AARNA2125386W_r_LNMAX.pdb
hide everything, AARNA2125386W_r_LNMAX
show cartoon, AARNA2125386W_r_LNMAX
color white, AARNA2125386W_r_LNMAX
spectrum b, rainbow, AARNA2125386W_r_LNMAX
set grid_slot, 3, AARNA2125386W_r_LNMAX
center AARNA2125386W_r_LNMAX
orient AARNA2125386W_r_LNMAX
show surface, AARNA2125386W_r_LNMAX
load AARNA2125386W_r_LNMIN.pdb
hide everything, AARNA2125386W_r_LNMIN
show cartoon, AARNA2125386W_r_LNMIN
color white, AARNA2125386W_r_LNMIN
spectrum b, rainbow, AARNA2125386W_r_LNMIN
set grid_slot, 4, AARNA2125386W_r_LNMIN
center AARNA2125386W_r_LNMIN
orient AARNA2125386W_r_LNMIN
show surface, AARNA2125386W_r_LNMIN
set seq_view, 1
set transparency, 0.0
