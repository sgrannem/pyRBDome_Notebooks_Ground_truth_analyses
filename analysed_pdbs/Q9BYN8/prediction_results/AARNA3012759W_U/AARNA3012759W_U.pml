set grid_mode,1
set surface_quality, 0
load AARNA3012759W_U_BP.pdb
hide everything, AARNA3012759W_U_BP
show cartoon, AARNA3012759W_U_BP
color white, AARNA3012759W_U_BP
spectrum b, rainbow, AARNA3012759W_U_BP
set grid_slot, 1, AARNA3012759W_U_BP
center AARNA3012759W_U_BP
orient AARNA3012759W_U_BP
show surface, AARNA3012759W_U_BP
load AARNA3012759W_U_EC.pdb
hide everything, AARNA3012759W_U_EC
show cartoon, AARNA3012759W_U_EC
color white, AARNA3012759W_U_EC
spectrum b, rainbow, AARNA3012759W_U_EC
set grid_slot, 2, AARNA3012759W_U_EC
center AARNA3012759W_U_EC
orient AARNA3012759W_U_EC
show surface, AARNA3012759W_U_EC
load AARNA3012759W_U_LNMAX.pdb
hide everything, AARNA3012759W_U_LNMAX
show cartoon, AARNA3012759W_U_LNMAX
color white, AARNA3012759W_U_LNMAX
spectrum b, rainbow, AARNA3012759W_U_LNMAX
set grid_slot, 3, AARNA3012759W_U_LNMAX
center AARNA3012759W_U_LNMAX
orient AARNA3012759W_U_LNMAX
show surface, AARNA3012759W_U_LNMAX
load AARNA3012759W_U_LNMIN.pdb
hide everything, AARNA3012759W_U_LNMIN
show cartoon, AARNA3012759W_U_LNMIN
color white, AARNA3012759W_U_LNMIN
spectrum b, rainbow, AARNA3012759W_U_LNMIN
set grid_slot, 4, AARNA3012759W_U_LNMIN
center AARNA3012759W_U_LNMIN
orient AARNA3012759W_U_LNMIN
show surface, AARNA3012759W_U_LNMIN
set seq_view, 1
set transparency, 0.0
