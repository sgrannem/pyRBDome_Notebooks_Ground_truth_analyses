set grid_mode,1
set surface_quality, 0
load AARNA2599776W_y_BP.pdb
hide everything, AARNA2599776W_y_BP
show cartoon, AARNA2599776W_y_BP
color white, AARNA2599776W_y_BP
spectrum b, rainbow, AARNA2599776W_y_BP
set grid_slot, 1, AARNA2599776W_y_BP
center AARNA2599776W_y_BP
orient AARNA2599776W_y_BP
show surface, AARNA2599776W_y_BP
load AARNA2599776W_y_EC.pdb
hide everything, AARNA2599776W_y_EC
show cartoon, AARNA2599776W_y_EC
color white, AARNA2599776W_y_EC
spectrum b, rainbow, AARNA2599776W_y_EC
set grid_slot, 2, AARNA2599776W_y_EC
center AARNA2599776W_y_EC
orient AARNA2599776W_y_EC
show surface, AARNA2599776W_y_EC
load AARNA2599776W_y_LNMAX.pdb
hide everything, AARNA2599776W_y_LNMAX
show cartoon, AARNA2599776W_y_LNMAX
color white, AARNA2599776W_y_LNMAX
spectrum b, rainbow, AARNA2599776W_y_LNMAX
set grid_slot, 3, AARNA2599776W_y_LNMAX
center AARNA2599776W_y_LNMAX
orient AARNA2599776W_y_LNMAX
show surface, AARNA2599776W_y_LNMAX
load AARNA2599776W_y_LNMIN.pdb
hide everything, AARNA2599776W_y_LNMIN
show cartoon, AARNA2599776W_y_LNMIN
color white, AARNA2599776W_y_LNMIN
spectrum b, rainbow, AARNA2599776W_y_LNMIN
set grid_slot, 4, AARNA2599776W_y_LNMIN
center AARNA2599776W_y_LNMIN
orient AARNA2599776W_y_LNMIN
show surface, AARNA2599776W_y_LNMIN
set seq_view, 1
set transparency, 0.0
