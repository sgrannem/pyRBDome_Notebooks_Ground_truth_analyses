set grid_mode,1
set surface_quality, 0
load AARNA1443116W_F_BP.pdb
hide everything, AARNA1443116W_F_BP
show cartoon, AARNA1443116W_F_BP
color white, AARNA1443116W_F_BP
spectrum b, rainbow, AARNA1443116W_F_BP
set grid_slot, 1, AARNA1443116W_F_BP
center AARNA1443116W_F_BP
orient AARNA1443116W_F_BP
show surface, AARNA1443116W_F_BP
load AARNA1443116W_F_EC.pdb
hide everything, AARNA1443116W_F_EC
show cartoon, AARNA1443116W_F_EC
color white, AARNA1443116W_F_EC
spectrum b, rainbow, AARNA1443116W_F_EC
set grid_slot, 2, AARNA1443116W_F_EC
center AARNA1443116W_F_EC
orient AARNA1443116W_F_EC
show surface, AARNA1443116W_F_EC
load AARNA1443116W_F_LNMAX.pdb
hide everything, AARNA1443116W_F_LNMAX
show cartoon, AARNA1443116W_F_LNMAX
color white, AARNA1443116W_F_LNMAX
spectrum b, rainbow, AARNA1443116W_F_LNMAX
set grid_slot, 3, AARNA1443116W_F_LNMAX
center AARNA1443116W_F_LNMAX
orient AARNA1443116W_F_LNMAX
show surface, AARNA1443116W_F_LNMAX
load AARNA1443116W_F_LNMIN.pdb
hide everything, AARNA1443116W_F_LNMIN
show cartoon, AARNA1443116W_F_LNMIN
color white, AARNA1443116W_F_LNMIN
spectrum b, rainbow, AARNA1443116W_F_LNMIN
set grid_slot, 4, AARNA1443116W_F_LNMIN
center AARNA1443116W_F_LNMIN
orient AARNA1443116W_F_LNMIN
show surface, AARNA1443116W_F_LNMIN
set seq_view, 1
set transparency, 0.0
