set grid_mode,1
set surface_quality, 0
load AARNA183158W_z_BP.pdb
hide everything, AARNA183158W_z_BP
show cartoon, AARNA183158W_z_BP
color white, AARNA183158W_z_BP
spectrum b, rainbow, AARNA183158W_z_BP
set grid_slot, 1, AARNA183158W_z_BP
center AARNA183158W_z_BP
orient AARNA183158W_z_BP
show surface, AARNA183158W_z_BP
load AARNA183158W_z_EC.pdb
hide everything, AARNA183158W_z_EC
show cartoon, AARNA183158W_z_EC
color white, AARNA183158W_z_EC
spectrum b, rainbow, AARNA183158W_z_EC
set grid_slot, 2, AARNA183158W_z_EC
center AARNA183158W_z_EC
orient AARNA183158W_z_EC
show surface, AARNA183158W_z_EC
load AARNA183158W_z_LNMAX.pdb
hide everything, AARNA183158W_z_LNMAX
show cartoon, AARNA183158W_z_LNMAX
color white, AARNA183158W_z_LNMAX
spectrum b, rainbow, AARNA183158W_z_LNMAX
set grid_slot, 3, AARNA183158W_z_LNMAX
center AARNA183158W_z_LNMAX
orient AARNA183158W_z_LNMAX
show surface, AARNA183158W_z_LNMAX
load AARNA183158W_z_LNMIN.pdb
hide everything, AARNA183158W_z_LNMIN
show cartoon, AARNA183158W_z_LNMIN
color white, AARNA183158W_z_LNMIN
spectrum b, rainbow, AARNA183158W_z_LNMIN
set grid_slot, 4, AARNA183158W_z_LNMIN
center AARNA183158W_z_LNMIN
orient AARNA183158W_z_LNMIN
show surface, AARNA183158W_z_LNMIN
set seq_view, 1
set transparency, 0.0
