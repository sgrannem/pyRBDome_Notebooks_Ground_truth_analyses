set grid_mode,1
set surface_quality, 0
load AARNA9468673W_P_BP.pdb
hide everything, AARNA9468673W_P_BP
show cartoon, AARNA9468673W_P_BP
color white, AARNA9468673W_P_BP
spectrum b, rainbow, AARNA9468673W_P_BP
set grid_slot, 1, AARNA9468673W_P_BP
center AARNA9468673W_P_BP
orient AARNA9468673W_P_BP
show surface, AARNA9468673W_P_BP
load AARNA9468673W_P_EC.pdb
hide everything, AARNA9468673W_P_EC
show cartoon, AARNA9468673W_P_EC
color white, AARNA9468673W_P_EC
spectrum b, rainbow, AARNA9468673W_P_EC
set grid_slot, 2, AARNA9468673W_P_EC
center AARNA9468673W_P_EC
orient AARNA9468673W_P_EC
show surface, AARNA9468673W_P_EC
load AARNA9468673W_P_LNMAX.pdb
hide everything, AARNA9468673W_P_LNMAX
show cartoon, AARNA9468673W_P_LNMAX
color white, AARNA9468673W_P_LNMAX
spectrum b, rainbow, AARNA9468673W_P_LNMAX
set grid_slot, 3, AARNA9468673W_P_LNMAX
center AARNA9468673W_P_LNMAX
orient AARNA9468673W_P_LNMAX
show surface, AARNA9468673W_P_LNMAX
load AARNA9468673W_P_LNMIN.pdb
hide everything, AARNA9468673W_P_LNMIN
show cartoon, AARNA9468673W_P_LNMIN
color white, AARNA9468673W_P_LNMIN
spectrum b, rainbow, AARNA9468673W_P_LNMIN
set grid_slot, 4, AARNA9468673W_P_LNMIN
center AARNA9468673W_P_LNMIN
orient AARNA9468673W_P_LNMIN
show surface, AARNA9468673W_P_LNMIN
set seq_view, 1
set transparency, 0.0
