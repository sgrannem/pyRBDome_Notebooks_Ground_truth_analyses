set grid_mode,1
set surface_quality, 0
load AARNA1744182W_U_BP.pdb
hide everything, AARNA1744182W_U_BP
show cartoon, AARNA1744182W_U_BP
color white, AARNA1744182W_U_BP
spectrum b, rainbow, AARNA1744182W_U_BP
set grid_slot, 1, AARNA1744182W_U_BP
center AARNA1744182W_U_BP
orient AARNA1744182W_U_BP
show surface, AARNA1744182W_U_BP
load AARNA1744182W_U_EC.pdb
hide everything, AARNA1744182W_U_EC
show cartoon, AARNA1744182W_U_EC
color white, AARNA1744182W_U_EC
spectrum b, rainbow, AARNA1744182W_U_EC
set grid_slot, 2, AARNA1744182W_U_EC
center AARNA1744182W_U_EC
orient AARNA1744182W_U_EC
show surface, AARNA1744182W_U_EC
load AARNA1744182W_U_LNMAX.pdb
hide everything, AARNA1744182W_U_LNMAX
show cartoon, AARNA1744182W_U_LNMAX
color white, AARNA1744182W_U_LNMAX
spectrum b, rainbow, AARNA1744182W_U_LNMAX
set grid_slot, 3, AARNA1744182W_U_LNMAX
center AARNA1744182W_U_LNMAX
orient AARNA1744182W_U_LNMAX
show surface, AARNA1744182W_U_LNMAX
load AARNA1744182W_U_LNMIN.pdb
hide everything, AARNA1744182W_U_LNMIN
show cartoon, AARNA1744182W_U_LNMIN
color white, AARNA1744182W_U_LNMIN
spectrum b, rainbow, AARNA1744182W_U_LNMIN
set grid_slot, 4, AARNA1744182W_U_LNMIN
center AARNA1744182W_U_LNMIN
orient AARNA1744182W_U_LNMIN
show surface, AARNA1744182W_U_LNMIN
set seq_view, 1
set transparency, 0.0
