set grid_mode,1
set surface_quality, 0
load AARNA6116016W_o_BP.pdb
hide everything, AARNA6116016W_o_BP
show cartoon, AARNA6116016W_o_BP
color white, AARNA6116016W_o_BP
spectrum b, rainbow, AARNA6116016W_o_BP
set grid_slot, 1, AARNA6116016W_o_BP
center AARNA6116016W_o_BP
orient AARNA6116016W_o_BP
show surface, AARNA6116016W_o_BP
load AARNA6116016W_o_EC.pdb
hide everything, AARNA6116016W_o_EC
show cartoon, AARNA6116016W_o_EC
color white, AARNA6116016W_o_EC
spectrum b, rainbow, AARNA6116016W_o_EC
set grid_slot, 2, AARNA6116016W_o_EC
center AARNA6116016W_o_EC
orient AARNA6116016W_o_EC
show surface, AARNA6116016W_o_EC
load AARNA6116016W_o_LNMAX.pdb
hide everything, AARNA6116016W_o_LNMAX
show cartoon, AARNA6116016W_o_LNMAX
color white, AARNA6116016W_o_LNMAX
spectrum b, rainbow, AARNA6116016W_o_LNMAX
set grid_slot, 3, AARNA6116016W_o_LNMAX
center AARNA6116016W_o_LNMAX
orient AARNA6116016W_o_LNMAX
show surface, AARNA6116016W_o_LNMAX
load AARNA6116016W_o_LNMIN.pdb
hide everything, AARNA6116016W_o_LNMIN
show cartoon, AARNA6116016W_o_LNMIN
color white, AARNA6116016W_o_LNMIN
spectrum b, rainbow, AARNA6116016W_o_LNMIN
set grid_slot, 4, AARNA6116016W_o_LNMIN
center AARNA6116016W_o_LNMIN
orient AARNA6116016W_o_LNMIN
show surface, AARNA6116016W_o_LNMIN
set seq_view, 1
set transparency, 0.0
