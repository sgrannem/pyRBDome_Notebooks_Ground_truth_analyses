set grid_mode,1
set surface_quality, 0
load AARNA5719826W_B_BP.pdb
hide everything, AARNA5719826W_B_BP
show cartoon, AARNA5719826W_B_BP
color white, AARNA5719826W_B_BP
spectrum b, rainbow, AARNA5719826W_B_BP
set grid_slot, 1, AARNA5719826W_B_BP
center AARNA5719826W_B_BP
orient AARNA5719826W_B_BP
show surface, AARNA5719826W_B_BP
load AARNA5719826W_B_EC.pdb
hide everything, AARNA5719826W_B_EC
show cartoon, AARNA5719826W_B_EC
color white, AARNA5719826W_B_EC
spectrum b, rainbow, AARNA5719826W_B_EC
set grid_slot, 2, AARNA5719826W_B_EC
center AARNA5719826W_B_EC
orient AARNA5719826W_B_EC
show surface, AARNA5719826W_B_EC
load AARNA5719826W_B_LNMAX.pdb
hide everything, AARNA5719826W_B_LNMAX
show cartoon, AARNA5719826W_B_LNMAX
color white, AARNA5719826W_B_LNMAX
spectrum b, rainbow, AARNA5719826W_B_LNMAX
set grid_slot, 3, AARNA5719826W_B_LNMAX
center AARNA5719826W_B_LNMAX
orient AARNA5719826W_B_LNMAX
show surface, AARNA5719826W_B_LNMAX
load AARNA5719826W_B_LNMIN.pdb
hide everything, AARNA5719826W_B_LNMIN
show cartoon, AARNA5719826W_B_LNMIN
color white, AARNA5719826W_B_LNMIN
spectrum b, rainbow, AARNA5719826W_B_LNMIN
set grid_slot, 4, AARNA5719826W_B_LNMIN
center AARNA5719826W_B_LNMIN
orient AARNA5719826W_B_LNMIN
show surface, AARNA5719826W_B_LNMIN
set seq_view, 1
set transparency, 0.0
