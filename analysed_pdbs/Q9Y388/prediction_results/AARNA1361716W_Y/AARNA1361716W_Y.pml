set grid_mode,1
set surface_quality, 0
load AARNA1361716W_Y_BP.pdb
hide everything, AARNA1361716W_Y_BP
show cartoon, AARNA1361716W_Y_BP
color white, AARNA1361716W_Y_BP
spectrum b, rainbow, AARNA1361716W_Y_BP
set grid_slot, 1, AARNA1361716W_Y_BP
center AARNA1361716W_Y_BP
orient AARNA1361716W_Y_BP
show surface, AARNA1361716W_Y_BP
load AARNA1361716W_Y_EC.pdb
hide everything, AARNA1361716W_Y_EC
show cartoon, AARNA1361716W_Y_EC
color white, AARNA1361716W_Y_EC
spectrum b, rainbow, AARNA1361716W_Y_EC
set grid_slot, 2, AARNA1361716W_Y_EC
center AARNA1361716W_Y_EC
orient AARNA1361716W_Y_EC
show surface, AARNA1361716W_Y_EC
load AARNA1361716W_Y_LNMAX.pdb
hide everything, AARNA1361716W_Y_LNMAX
show cartoon, AARNA1361716W_Y_LNMAX
color white, AARNA1361716W_Y_LNMAX
spectrum b, rainbow, AARNA1361716W_Y_LNMAX
set grid_slot, 3, AARNA1361716W_Y_LNMAX
center AARNA1361716W_Y_LNMAX
orient AARNA1361716W_Y_LNMAX
show surface, AARNA1361716W_Y_LNMAX
load AARNA1361716W_Y_LNMIN.pdb
hide everything, AARNA1361716W_Y_LNMIN
show cartoon, AARNA1361716W_Y_LNMIN
color white, AARNA1361716W_Y_LNMIN
spectrum b, rainbow, AARNA1361716W_Y_LNMIN
set grid_slot, 4, AARNA1361716W_Y_LNMIN
center AARNA1361716W_Y_LNMIN
orient AARNA1361716W_Y_LNMIN
show surface, AARNA1361716W_Y_LNMIN
set seq_view, 1
set transparency, 0.0
