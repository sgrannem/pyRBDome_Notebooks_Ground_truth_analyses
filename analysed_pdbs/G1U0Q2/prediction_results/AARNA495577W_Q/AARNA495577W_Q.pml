set grid_mode,1
set surface_quality, 0
load AARNA495577W_Q_BP.pdb
hide everything, AARNA495577W_Q_BP
show cartoon, AARNA495577W_Q_BP
color white, AARNA495577W_Q_BP
spectrum b, rainbow, AARNA495577W_Q_BP
set grid_slot, 1, AARNA495577W_Q_BP
center AARNA495577W_Q_BP
orient AARNA495577W_Q_BP
show surface, AARNA495577W_Q_BP
load AARNA495577W_Q_EC.pdb
hide everything, AARNA495577W_Q_EC
show cartoon, AARNA495577W_Q_EC
color white, AARNA495577W_Q_EC
spectrum b, rainbow, AARNA495577W_Q_EC
set grid_slot, 2, AARNA495577W_Q_EC
center AARNA495577W_Q_EC
orient AARNA495577W_Q_EC
show surface, AARNA495577W_Q_EC
load AARNA495577W_Q_LNMAX.pdb
hide everything, AARNA495577W_Q_LNMAX
show cartoon, AARNA495577W_Q_LNMAX
color white, AARNA495577W_Q_LNMAX
spectrum b, rainbow, AARNA495577W_Q_LNMAX
set grid_slot, 3, AARNA495577W_Q_LNMAX
center AARNA495577W_Q_LNMAX
orient AARNA495577W_Q_LNMAX
show surface, AARNA495577W_Q_LNMAX
load AARNA495577W_Q_LNMIN.pdb
hide everything, AARNA495577W_Q_LNMIN
show cartoon, AARNA495577W_Q_LNMIN
color white, AARNA495577W_Q_LNMIN
spectrum b, rainbow, AARNA495577W_Q_LNMIN
set grid_slot, 4, AARNA495577W_Q_LNMIN
center AARNA495577W_Q_LNMIN
orient AARNA495577W_Q_LNMIN
show surface, AARNA495577W_Q_LNMIN
set seq_view, 1
set transparency, 0.0
