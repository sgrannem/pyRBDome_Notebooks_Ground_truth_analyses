set grid_mode,1
set surface_quality, 0
load AARNA4158683W_M_BP.pdb
hide everything, AARNA4158683W_M_BP
show cartoon, AARNA4158683W_M_BP
color white, AARNA4158683W_M_BP
spectrum b, rainbow, AARNA4158683W_M_BP
set grid_slot, 1, AARNA4158683W_M_BP
center AARNA4158683W_M_BP
orient AARNA4158683W_M_BP
show surface, AARNA4158683W_M_BP
load AARNA4158683W_M_EC.pdb
hide everything, AARNA4158683W_M_EC
show cartoon, AARNA4158683W_M_EC
color white, AARNA4158683W_M_EC
spectrum b, rainbow, AARNA4158683W_M_EC
set grid_slot, 2, AARNA4158683W_M_EC
center AARNA4158683W_M_EC
orient AARNA4158683W_M_EC
show surface, AARNA4158683W_M_EC
load AARNA4158683W_M_LNMAX.pdb
hide everything, AARNA4158683W_M_LNMAX
show cartoon, AARNA4158683W_M_LNMAX
color white, AARNA4158683W_M_LNMAX
spectrum b, rainbow, AARNA4158683W_M_LNMAX
set grid_slot, 3, AARNA4158683W_M_LNMAX
center AARNA4158683W_M_LNMAX
orient AARNA4158683W_M_LNMAX
show surface, AARNA4158683W_M_LNMAX
load AARNA4158683W_M_LNMIN.pdb
hide everything, AARNA4158683W_M_LNMIN
show cartoon, AARNA4158683W_M_LNMIN
color white, AARNA4158683W_M_LNMIN
spectrum b, rainbow, AARNA4158683W_M_LNMIN
set grid_slot, 4, AARNA4158683W_M_LNMIN
center AARNA4158683W_M_LNMIN
orient AARNA4158683W_M_LNMIN
show surface, AARNA4158683W_M_LNMIN
set seq_view, 1
set transparency, 0.0
