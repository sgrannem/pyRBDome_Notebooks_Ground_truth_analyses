set grid_mode,1
set surface_quality, 0
load AARNA3274465W_d_BP.pdb
hide everything, AARNA3274465W_d_BP
show cartoon, AARNA3274465W_d_BP
color white, AARNA3274465W_d_BP
spectrum b, rainbow, AARNA3274465W_d_BP
set grid_slot, 1, AARNA3274465W_d_BP
center AARNA3274465W_d_BP
orient AARNA3274465W_d_BP
show surface, AARNA3274465W_d_BP
load AARNA3274465W_d_EC.pdb
hide everything, AARNA3274465W_d_EC
show cartoon, AARNA3274465W_d_EC
color white, AARNA3274465W_d_EC
spectrum b, rainbow, AARNA3274465W_d_EC
set grid_slot, 2, AARNA3274465W_d_EC
center AARNA3274465W_d_EC
orient AARNA3274465W_d_EC
show surface, AARNA3274465W_d_EC
load AARNA3274465W_d_LNMAX.pdb
hide everything, AARNA3274465W_d_LNMAX
show cartoon, AARNA3274465W_d_LNMAX
color white, AARNA3274465W_d_LNMAX
spectrum b, rainbow, AARNA3274465W_d_LNMAX
set grid_slot, 3, AARNA3274465W_d_LNMAX
center AARNA3274465W_d_LNMAX
orient AARNA3274465W_d_LNMAX
show surface, AARNA3274465W_d_LNMAX
load AARNA3274465W_d_LNMIN.pdb
hide everything, AARNA3274465W_d_LNMIN
show cartoon, AARNA3274465W_d_LNMIN
color white, AARNA3274465W_d_LNMIN
spectrum b, rainbow, AARNA3274465W_d_LNMIN
set grid_slot, 4, AARNA3274465W_d_LNMIN
center AARNA3274465W_d_LNMIN
orient AARNA3274465W_d_LNMIN
show surface, AARNA3274465W_d_LNMIN
set seq_view, 1
set transparency, 0.0
