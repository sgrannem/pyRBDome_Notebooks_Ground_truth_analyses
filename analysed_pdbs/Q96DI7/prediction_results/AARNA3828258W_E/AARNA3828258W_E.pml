set grid_mode,1
set surface_quality, 0
load AARNA3828258W_E_BP.pdb
hide everything, AARNA3828258W_E_BP
show cartoon, AARNA3828258W_E_BP
color white, AARNA3828258W_E_BP
spectrum b, rainbow, AARNA3828258W_E_BP
set grid_slot, 1, AARNA3828258W_E_BP
center AARNA3828258W_E_BP
orient AARNA3828258W_E_BP
show surface, AARNA3828258W_E_BP
load AARNA3828258W_E_EC.pdb
hide everything, AARNA3828258W_E_EC
show cartoon, AARNA3828258W_E_EC
color white, AARNA3828258W_E_EC
spectrum b, rainbow, AARNA3828258W_E_EC
set grid_slot, 2, AARNA3828258W_E_EC
center AARNA3828258W_E_EC
orient AARNA3828258W_E_EC
show surface, AARNA3828258W_E_EC
load AARNA3828258W_E_LNMAX.pdb
hide everything, AARNA3828258W_E_LNMAX
show cartoon, AARNA3828258W_E_LNMAX
color white, AARNA3828258W_E_LNMAX
spectrum b, rainbow, AARNA3828258W_E_LNMAX
set grid_slot, 3, AARNA3828258W_E_LNMAX
center AARNA3828258W_E_LNMAX
orient AARNA3828258W_E_LNMAX
show surface, AARNA3828258W_E_LNMAX
load AARNA3828258W_E_LNMIN.pdb
hide everything, AARNA3828258W_E_LNMIN
show cartoon, AARNA3828258W_E_LNMIN
color white, AARNA3828258W_E_LNMIN
spectrum b, rainbow, AARNA3828258W_E_LNMIN
set grid_slot, 4, AARNA3828258W_E_LNMIN
center AARNA3828258W_E_LNMIN
orient AARNA3828258W_E_LNMIN
show surface, AARNA3828258W_E_LNMIN
set seq_view, 1
set transparency, 0.0
