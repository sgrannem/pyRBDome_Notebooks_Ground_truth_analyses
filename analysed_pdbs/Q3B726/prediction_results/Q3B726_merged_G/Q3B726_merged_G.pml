set grid_mode,1
set surface_quality, 0
load AARNA596263W_G_BP.pdb
hide everything, AARNA596263W_G_BP
show cartoon, AARNA596263W_G_BP
color white, AARNA596263W_G_BP
spectrum b, rainbow, AARNA596263W_G_BP
set grid_slot, 1, AARNA596263W_G_BP
center AARNA596263W_G_BP
orient AARNA596263W_G_BP
show surface, AARNA596263W_G_BP
load AARNA596263W_G_EC.pdb
hide everything, AARNA596263W_G_EC
show cartoon, AARNA596263W_G_EC
color white, AARNA596263W_G_EC
spectrum b, rainbow, AARNA596263W_G_EC
set grid_slot, 2, AARNA596263W_G_EC
center AARNA596263W_G_EC
orient AARNA596263W_G_EC
show surface, AARNA596263W_G_EC
load AARNA596263W_G_LNMAX.pdb
hide everything, AARNA596263W_G_LNMAX
show cartoon, AARNA596263W_G_LNMAX
color white, AARNA596263W_G_LNMAX
spectrum b, rainbow, AARNA596263W_G_LNMAX
set grid_slot, 3, AARNA596263W_G_LNMAX
center AARNA596263W_G_LNMAX
orient AARNA596263W_G_LNMAX
show surface, AARNA596263W_G_LNMAX
load AARNA596263W_G_LNMIN.pdb
hide everything, AARNA596263W_G_LNMIN
show cartoon, AARNA596263W_G_LNMIN
color white, AARNA596263W_G_LNMIN
spectrum b, rainbow, AARNA596263W_G_LNMIN
set grid_slot, 4, AARNA596263W_G_LNMIN
center AARNA596263W_G_LNMIN
orient AARNA596263W_G_LNMIN
show surface, AARNA596263W_G_LNMIN
set seq_view, 1
set transparency, 0.0
