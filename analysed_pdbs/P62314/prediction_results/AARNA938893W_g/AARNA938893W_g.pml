set grid_mode,1
set surface_quality, 0
load AARNA938893W_g_BP.pdb
hide everything, AARNA938893W_g_BP
show cartoon, AARNA938893W_g_BP
color white, AARNA938893W_g_BP
spectrum b, rainbow, AARNA938893W_g_BP
set grid_slot, 1, AARNA938893W_g_BP
center AARNA938893W_g_BP
orient AARNA938893W_g_BP
show surface, AARNA938893W_g_BP
load AARNA938893W_g_EC.pdb
hide everything, AARNA938893W_g_EC
show cartoon, AARNA938893W_g_EC
color white, AARNA938893W_g_EC
spectrum b, rainbow, AARNA938893W_g_EC
set grid_slot, 2, AARNA938893W_g_EC
center AARNA938893W_g_EC
orient AARNA938893W_g_EC
show surface, AARNA938893W_g_EC
load AARNA938893W_g_LNMAX.pdb
hide everything, AARNA938893W_g_LNMAX
show cartoon, AARNA938893W_g_LNMAX
color white, AARNA938893W_g_LNMAX
spectrum b, rainbow, AARNA938893W_g_LNMAX
set grid_slot, 3, AARNA938893W_g_LNMAX
center AARNA938893W_g_LNMAX
orient AARNA938893W_g_LNMAX
show surface, AARNA938893W_g_LNMAX
load AARNA938893W_g_LNMIN.pdb
hide everything, AARNA938893W_g_LNMIN
show cartoon, AARNA938893W_g_LNMIN
color white, AARNA938893W_g_LNMIN
spectrum b, rainbow, AARNA938893W_g_LNMIN
set grid_slot, 4, AARNA938893W_g_LNMIN
center AARNA938893W_g_LNMIN
orient AARNA938893W_g_LNMIN
show surface, AARNA938893W_g_LNMIN
set seq_view, 1
set transparency, 0.0
