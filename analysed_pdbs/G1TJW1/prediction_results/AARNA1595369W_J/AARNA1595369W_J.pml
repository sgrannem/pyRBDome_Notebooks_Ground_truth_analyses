set grid_mode,1
set surface_quality, 0
load AARNA1595369W_J_BP.pdb
hide everything, AARNA1595369W_J_BP
show cartoon, AARNA1595369W_J_BP
color white, AARNA1595369W_J_BP
spectrum b, rainbow, AARNA1595369W_J_BP
set grid_slot, 1, AARNA1595369W_J_BP
center AARNA1595369W_J_BP
orient AARNA1595369W_J_BP
show surface, AARNA1595369W_J_BP
load AARNA1595369W_J_EC.pdb
hide everything, AARNA1595369W_J_EC
show cartoon, AARNA1595369W_J_EC
color white, AARNA1595369W_J_EC
spectrum b, rainbow, AARNA1595369W_J_EC
set grid_slot, 2, AARNA1595369W_J_EC
center AARNA1595369W_J_EC
orient AARNA1595369W_J_EC
show surface, AARNA1595369W_J_EC
load AARNA1595369W_J_LNMAX.pdb
hide everything, AARNA1595369W_J_LNMAX
show cartoon, AARNA1595369W_J_LNMAX
color white, AARNA1595369W_J_LNMAX
spectrum b, rainbow, AARNA1595369W_J_LNMAX
set grid_slot, 3, AARNA1595369W_J_LNMAX
center AARNA1595369W_J_LNMAX
orient AARNA1595369W_J_LNMAX
show surface, AARNA1595369W_J_LNMAX
load AARNA1595369W_J_LNMIN.pdb
hide everything, AARNA1595369W_J_LNMIN
show cartoon, AARNA1595369W_J_LNMIN
color white, AARNA1595369W_J_LNMIN
spectrum b, rainbow, AARNA1595369W_J_LNMIN
set grid_slot, 4, AARNA1595369W_J_LNMIN
center AARNA1595369W_J_LNMIN
orient AARNA1595369W_J_LNMIN
show surface, AARNA1595369W_J_LNMIN
set seq_view, 1
set transparency, 0.0
