set grid_mode,1
set surface_quality, 0
load AARNA6536746W_g_BP.pdb
hide everything, AARNA6536746W_g_BP
show cartoon, AARNA6536746W_g_BP
color white, AARNA6536746W_g_BP
spectrum b, rainbow, AARNA6536746W_g_BP
set grid_slot, 1, AARNA6536746W_g_BP
center AARNA6536746W_g_BP
orient AARNA6536746W_g_BP
show surface, AARNA6536746W_g_BP
load AARNA6536746W_g_EC.pdb
hide everything, AARNA6536746W_g_EC
show cartoon, AARNA6536746W_g_EC
color white, AARNA6536746W_g_EC
spectrum b, rainbow, AARNA6536746W_g_EC
set grid_slot, 2, AARNA6536746W_g_EC
center AARNA6536746W_g_EC
orient AARNA6536746W_g_EC
show surface, AARNA6536746W_g_EC
load AARNA6536746W_g_LNMAX.pdb
hide everything, AARNA6536746W_g_LNMAX
show cartoon, AARNA6536746W_g_LNMAX
color white, AARNA6536746W_g_LNMAX
spectrum b, rainbow, AARNA6536746W_g_LNMAX
set grid_slot, 3, AARNA6536746W_g_LNMAX
center AARNA6536746W_g_LNMAX
orient AARNA6536746W_g_LNMAX
show surface, AARNA6536746W_g_LNMAX
load AARNA6536746W_g_LNMIN.pdb
hide everything, AARNA6536746W_g_LNMIN
show cartoon, AARNA6536746W_g_LNMIN
color white, AARNA6536746W_g_LNMIN
spectrum b, rainbow, AARNA6536746W_g_LNMIN
set grid_slot, 4, AARNA6536746W_g_LNMIN
center AARNA6536746W_g_LNMIN
orient AARNA6536746W_g_LNMIN
show surface, AARNA6536746W_g_LNMIN
set seq_view, 1
set transparency, 0.0
