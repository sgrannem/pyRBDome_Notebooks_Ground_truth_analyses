set grid_mode,1
set surface_quality, 0
load AARNA260134W_C_BP.pdb
hide everything, AARNA260134W_C_BP
show cartoon, AARNA260134W_C_BP
color white, AARNA260134W_C_BP
spectrum b, rainbow, AARNA260134W_C_BP
set grid_slot, 1, AARNA260134W_C_BP
center AARNA260134W_C_BP
orient AARNA260134W_C_BP
show surface, AARNA260134W_C_BP
load AARNA260134W_C_EC.pdb
hide everything, AARNA260134W_C_EC
show cartoon, AARNA260134W_C_EC
color white, AARNA260134W_C_EC
spectrum b, rainbow, AARNA260134W_C_EC
set grid_slot, 2, AARNA260134W_C_EC
center AARNA260134W_C_EC
orient AARNA260134W_C_EC
show surface, AARNA260134W_C_EC
load AARNA260134W_C_LNMAX.pdb
hide everything, AARNA260134W_C_LNMAX
show cartoon, AARNA260134W_C_LNMAX
color white, AARNA260134W_C_LNMAX
spectrum b, rainbow, AARNA260134W_C_LNMAX
set grid_slot, 3, AARNA260134W_C_LNMAX
center AARNA260134W_C_LNMAX
orient AARNA260134W_C_LNMAX
show surface, AARNA260134W_C_LNMAX
load AARNA260134W_C_LNMIN.pdb
hide everything, AARNA260134W_C_LNMIN
show cartoon, AARNA260134W_C_LNMIN
color white, AARNA260134W_C_LNMIN
spectrum b, rainbow, AARNA260134W_C_LNMIN
set grid_slot, 4, AARNA260134W_C_LNMIN
center AARNA260134W_C_LNMIN
orient AARNA260134W_C_LNMIN
show surface, AARNA260134W_C_LNMIN
set seq_view, 1
set transparency, 0.0
