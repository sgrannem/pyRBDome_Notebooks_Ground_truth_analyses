set grid_mode,1
set surface_quality, 0
load AARNA1216576W_D_BP.pdb
hide everything, AARNA1216576W_D_BP
show cartoon, AARNA1216576W_D_BP
color white, AARNA1216576W_D_BP
spectrum b, rainbow, AARNA1216576W_D_BP
set grid_slot, 1, AARNA1216576W_D_BP
center AARNA1216576W_D_BP
orient AARNA1216576W_D_BP
show surface, AARNA1216576W_D_BP
load AARNA1216576W_D_EC.pdb
hide everything, AARNA1216576W_D_EC
show cartoon, AARNA1216576W_D_EC
color white, AARNA1216576W_D_EC
spectrum b, rainbow, AARNA1216576W_D_EC
set grid_slot, 2, AARNA1216576W_D_EC
center AARNA1216576W_D_EC
orient AARNA1216576W_D_EC
show surface, AARNA1216576W_D_EC
load AARNA1216576W_D_LNMAX.pdb
hide everything, AARNA1216576W_D_LNMAX
show cartoon, AARNA1216576W_D_LNMAX
color white, AARNA1216576W_D_LNMAX
spectrum b, rainbow, AARNA1216576W_D_LNMAX
set grid_slot, 3, AARNA1216576W_D_LNMAX
center AARNA1216576W_D_LNMAX
orient AARNA1216576W_D_LNMAX
show surface, AARNA1216576W_D_LNMAX
load AARNA1216576W_D_LNMIN.pdb
hide everything, AARNA1216576W_D_LNMIN
show cartoon, AARNA1216576W_D_LNMIN
color white, AARNA1216576W_D_LNMIN
spectrum b, rainbow, AARNA1216576W_D_LNMIN
set grid_slot, 4, AARNA1216576W_D_LNMIN
center AARNA1216576W_D_LNMIN
orient AARNA1216576W_D_LNMIN
show surface, AARNA1216576W_D_LNMIN
set seq_view, 1
set transparency, 0.0
