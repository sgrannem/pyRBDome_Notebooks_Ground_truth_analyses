RESNR	RESTYPE	RESCHAIN	RESNR_LIG	RESTYPE_LIG	RESCHAIN_LIG	SIDECHAIN	DIST_H-A	DIST_D-A	DON_ANGLE	PROTISDON	DONORIDX	DONORTYPE	ACCEPTORIDX	ACCEPTORTYPE	LIGCOO	PROTCOO
209	GLY	I	1	U	T	False	3.05	3.49	108.17	True	7699	Nam	12814	O3	(23.006, 33.326, 56.412)	(19.939, 34.975, 56.214)
104	PHE	I	1	U	T	False	1.79	2.68	149.72	True	6862	Nam	12822	O2	(22.621, 34.443, 60.285)	(23.261, 34.943, 62.841)
63	ARG	I	2	G	T	True	3.15	3.62	110.76	True	6534	Ng+	12845	O2	(20.628, 26.972, 63.28)	(21.911, 24.842, 65.911)
101	THR	I	5	A	T	False	3.26	4.08	142.18	True	6837	Nam	12886	Nar	(30.116, 30.77, 65.333)	(30.982, 34.583, 64.162)
104	PHE	I	1	U	T	False	1.76	2.74	172.43	False	12823	Nar	6865	O2	(20.688, 33.27, 60.494)	(20.442, 34.611, 62.875)
