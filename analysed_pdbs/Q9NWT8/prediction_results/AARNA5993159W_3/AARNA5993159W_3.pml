set grid_mode,1
set surface_quality, 0
load AARNA5993159W_3_BP.pdb
hide everything, AARNA5993159W_3_BP
show cartoon, AARNA5993159W_3_BP
color white, AARNA5993159W_3_BP
spectrum b, rainbow, AARNA5993159W_3_BP
set grid_slot, 1, AARNA5993159W_3_BP
center AARNA5993159W_3_BP
orient AARNA5993159W_3_BP
show surface, AARNA5993159W_3_BP
load AARNA5993159W_3_EC.pdb
hide everything, AARNA5993159W_3_EC
show cartoon, AARNA5993159W_3_EC
color white, AARNA5993159W_3_EC
spectrum b, rainbow, AARNA5993159W_3_EC
set grid_slot, 2, AARNA5993159W_3_EC
center AARNA5993159W_3_EC
orient AARNA5993159W_3_EC
show surface, AARNA5993159W_3_EC
load AARNA5993159W_3_LNMAX.pdb
hide everything, AARNA5993159W_3_LNMAX
show cartoon, AARNA5993159W_3_LNMAX
color white, AARNA5993159W_3_LNMAX
spectrum b, rainbow, AARNA5993159W_3_LNMAX
set grid_slot, 3, AARNA5993159W_3_LNMAX
center AARNA5993159W_3_LNMAX
orient AARNA5993159W_3_LNMAX
show surface, AARNA5993159W_3_LNMAX
load AARNA5993159W_3_LNMIN.pdb
hide everything, AARNA5993159W_3_LNMIN
show cartoon, AARNA5993159W_3_LNMIN
color white, AARNA5993159W_3_LNMIN
spectrum b, rainbow, AARNA5993159W_3_LNMIN
set grid_slot, 4, AARNA5993159W_3_LNMIN
center AARNA5993159W_3_LNMIN
orient AARNA5993159W_3_LNMIN
show surface, AARNA5993159W_3_LNMIN
set seq_view, 1
set transparency, 0.0
