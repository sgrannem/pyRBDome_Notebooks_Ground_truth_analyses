set grid_mode,1
set surface_quality, 0
load AARNA6059497W_k_BP.pdb
hide everything, AARNA6059497W_k_BP
show cartoon, AARNA6059497W_k_BP
color white, AARNA6059497W_k_BP
spectrum b, rainbow, AARNA6059497W_k_BP
set grid_slot, 1, AARNA6059497W_k_BP
center AARNA6059497W_k_BP
orient AARNA6059497W_k_BP
show surface, AARNA6059497W_k_BP
load AARNA6059497W_k_EC.pdb
hide everything, AARNA6059497W_k_EC
show cartoon, AARNA6059497W_k_EC
color white, AARNA6059497W_k_EC
spectrum b, rainbow, AARNA6059497W_k_EC
set grid_slot, 2, AARNA6059497W_k_EC
center AARNA6059497W_k_EC
orient AARNA6059497W_k_EC
show surface, AARNA6059497W_k_EC
load AARNA6059497W_k_LNMAX.pdb
hide everything, AARNA6059497W_k_LNMAX
show cartoon, AARNA6059497W_k_LNMAX
color white, AARNA6059497W_k_LNMAX
spectrum b, rainbow, AARNA6059497W_k_LNMAX
set grid_slot, 3, AARNA6059497W_k_LNMAX
center AARNA6059497W_k_LNMAX
orient AARNA6059497W_k_LNMAX
show surface, AARNA6059497W_k_LNMAX
load AARNA6059497W_k_LNMIN.pdb
hide everything, AARNA6059497W_k_LNMIN
show cartoon, AARNA6059497W_k_LNMIN
color white, AARNA6059497W_k_LNMIN
spectrum b, rainbow, AARNA6059497W_k_LNMIN
set grid_slot, 4, AARNA6059497W_k_LNMIN
center AARNA6059497W_k_LNMIN
orient AARNA6059497W_k_LNMIN
show surface, AARNA6059497W_k_LNMIN
set seq_view, 1
set transparency, 0.0
