set grid_mode,1
set surface_quality, 0
load AARNA6408149W_8_BP.pdb
hide everything, AARNA6408149W_8_BP
show cartoon, AARNA6408149W_8_BP
color white, AARNA6408149W_8_BP
spectrum b, rainbow, AARNA6408149W_8_BP
set grid_slot, 1, AARNA6408149W_8_BP
center AARNA6408149W_8_BP
orient AARNA6408149W_8_BP
show surface, AARNA6408149W_8_BP
load AARNA6408149W_8_EC.pdb
hide everything, AARNA6408149W_8_EC
show cartoon, AARNA6408149W_8_EC
color white, AARNA6408149W_8_EC
spectrum b, rainbow, AARNA6408149W_8_EC
set grid_slot, 2, AARNA6408149W_8_EC
center AARNA6408149W_8_EC
orient AARNA6408149W_8_EC
show surface, AARNA6408149W_8_EC
load AARNA6408149W_8_LNMAX.pdb
hide everything, AARNA6408149W_8_LNMAX
show cartoon, AARNA6408149W_8_LNMAX
color white, AARNA6408149W_8_LNMAX
spectrum b, rainbow, AARNA6408149W_8_LNMAX
set grid_slot, 3, AARNA6408149W_8_LNMAX
center AARNA6408149W_8_LNMAX
orient AARNA6408149W_8_LNMAX
show surface, AARNA6408149W_8_LNMAX
load AARNA6408149W_8_LNMIN.pdb
hide everything, AARNA6408149W_8_LNMIN
show cartoon, AARNA6408149W_8_LNMIN
color white, AARNA6408149W_8_LNMIN
spectrum b, rainbow, AARNA6408149W_8_LNMIN
set grid_slot, 4, AARNA6408149W_8_LNMIN
center AARNA6408149W_8_LNMIN
orient AARNA6408149W_8_LNMIN
show surface, AARNA6408149W_8_LNMIN
set seq_view, 1
set transparency, 0.0
