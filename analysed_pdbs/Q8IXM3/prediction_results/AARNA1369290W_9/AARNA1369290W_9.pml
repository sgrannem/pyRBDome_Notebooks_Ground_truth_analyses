set grid_mode,1
set surface_quality, 0
load AARNA1369290W_9_BP.pdb
hide everything, AARNA1369290W_9_BP
show cartoon, AARNA1369290W_9_BP
color white, AARNA1369290W_9_BP
spectrum b, rainbow, AARNA1369290W_9_BP
set grid_slot, 1, AARNA1369290W_9_BP
center AARNA1369290W_9_BP
orient AARNA1369290W_9_BP
show surface, AARNA1369290W_9_BP
load AARNA1369290W_9_EC.pdb
hide everything, AARNA1369290W_9_EC
show cartoon, AARNA1369290W_9_EC
color white, AARNA1369290W_9_EC
spectrum b, rainbow, AARNA1369290W_9_EC
set grid_slot, 2, AARNA1369290W_9_EC
center AARNA1369290W_9_EC
orient AARNA1369290W_9_EC
show surface, AARNA1369290W_9_EC
load AARNA1369290W_9_LNMAX.pdb
hide everything, AARNA1369290W_9_LNMAX
show cartoon, AARNA1369290W_9_LNMAX
color white, AARNA1369290W_9_LNMAX
spectrum b, rainbow, AARNA1369290W_9_LNMAX
set grid_slot, 3, AARNA1369290W_9_LNMAX
center AARNA1369290W_9_LNMAX
orient AARNA1369290W_9_LNMAX
show surface, AARNA1369290W_9_LNMAX
load AARNA1369290W_9_LNMIN.pdb
hide everything, AARNA1369290W_9_LNMIN
show cartoon, AARNA1369290W_9_LNMIN
color white, AARNA1369290W_9_LNMIN
spectrum b, rainbow, AARNA1369290W_9_LNMIN
set grid_slot, 4, AARNA1369290W_9_LNMIN
center AARNA1369290W_9_LNMIN
orient AARNA1369290W_9_LNMIN
show surface, AARNA1369290W_9_LNMIN
set seq_view, 1
set transparency, 0.0
