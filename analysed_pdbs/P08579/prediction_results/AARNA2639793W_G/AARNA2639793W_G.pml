set grid_mode,1
set surface_quality, 0
load AARNA2639793W_G_BP.pdb
hide everything, AARNA2639793W_G_BP
show cartoon, AARNA2639793W_G_BP
color white, AARNA2639793W_G_BP
spectrum b, rainbow, AARNA2639793W_G_BP
set grid_slot, 1, AARNA2639793W_G_BP
center AARNA2639793W_G_BP
orient AARNA2639793W_G_BP
show surface, AARNA2639793W_G_BP
load AARNA2639793W_G_EC.pdb
hide everything, AARNA2639793W_G_EC
show cartoon, AARNA2639793W_G_EC
color white, AARNA2639793W_G_EC
spectrum b, rainbow, AARNA2639793W_G_EC
set grid_slot, 2, AARNA2639793W_G_EC
center AARNA2639793W_G_EC
orient AARNA2639793W_G_EC
show surface, AARNA2639793W_G_EC
load AARNA2639793W_G_LNMAX.pdb
hide everything, AARNA2639793W_G_LNMAX
show cartoon, AARNA2639793W_G_LNMAX
color white, AARNA2639793W_G_LNMAX
spectrum b, rainbow, AARNA2639793W_G_LNMAX
set grid_slot, 3, AARNA2639793W_G_LNMAX
center AARNA2639793W_G_LNMAX
orient AARNA2639793W_G_LNMAX
show surface, AARNA2639793W_G_LNMAX
load AARNA2639793W_G_LNMIN.pdb
hide everything, AARNA2639793W_G_LNMIN
show cartoon, AARNA2639793W_G_LNMIN
color white, AARNA2639793W_G_LNMIN
spectrum b, rainbow, AARNA2639793W_G_LNMIN
set grid_slot, 4, AARNA2639793W_G_LNMIN
center AARNA2639793W_G_LNMIN
orient AARNA2639793W_G_LNMIN
show surface, AARNA2639793W_G_LNMIN
set seq_view, 1
set transparency, 0.0
