set grid_mode,1
set surface_quality, 0
load AARNA1503684W_a_BP.pdb
hide everything, AARNA1503684W_a_BP
show cartoon, AARNA1503684W_a_BP
color white, AARNA1503684W_a_BP
spectrum b, rainbow, AARNA1503684W_a_BP
set grid_slot, 1, AARNA1503684W_a_BP
center AARNA1503684W_a_BP
orient AARNA1503684W_a_BP
show surface, AARNA1503684W_a_BP
load AARNA1503684W_a_EC.pdb
hide everything, AARNA1503684W_a_EC
show cartoon, AARNA1503684W_a_EC
color white, AARNA1503684W_a_EC
spectrum b, rainbow, AARNA1503684W_a_EC
set grid_slot, 2, AARNA1503684W_a_EC
center AARNA1503684W_a_EC
orient AARNA1503684W_a_EC
show surface, AARNA1503684W_a_EC
load AARNA1503684W_a_LNMAX.pdb
hide everything, AARNA1503684W_a_LNMAX
show cartoon, AARNA1503684W_a_LNMAX
color white, AARNA1503684W_a_LNMAX
spectrum b, rainbow, AARNA1503684W_a_LNMAX
set grid_slot, 3, AARNA1503684W_a_LNMAX
center AARNA1503684W_a_LNMAX
orient AARNA1503684W_a_LNMAX
show surface, AARNA1503684W_a_LNMAX
load AARNA1503684W_a_LNMIN.pdb
hide everything, AARNA1503684W_a_LNMIN
show cartoon, AARNA1503684W_a_LNMIN
color white, AARNA1503684W_a_LNMIN
spectrum b, rainbow, AARNA1503684W_a_LNMIN
set grid_slot, 4, AARNA1503684W_a_LNMIN
center AARNA1503684W_a_LNMIN
orient AARNA1503684W_a_LNMIN
show surface, AARNA1503684W_a_LNMIN
set seq_view, 1
set transparency, 0.0
