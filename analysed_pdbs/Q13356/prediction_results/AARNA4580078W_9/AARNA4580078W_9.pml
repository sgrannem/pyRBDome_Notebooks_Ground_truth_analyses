set grid_mode,1
set surface_quality, 0
load AARNA4580078W_9_BP.pdb
hide everything, AARNA4580078W_9_BP
show cartoon, AARNA4580078W_9_BP
color white, AARNA4580078W_9_BP
spectrum b, rainbow, AARNA4580078W_9_BP
set grid_slot, 1, AARNA4580078W_9_BP
center AARNA4580078W_9_BP
orient AARNA4580078W_9_BP
show surface, AARNA4580078W_9_BP
load AARNA4580078W_9_EC.pdb
hide everything, AARNA4580078W_9_EC
show cartoon, AARNA4580078W_9_EC
color white, AARNA4580078W_9_EC
spectrum b, rainbow, AARNA4580078W_9_EC
set grid_slot, 2, AARNA4580078W_9_EC
center AARNA4580078W_9_EC
orient AARNA4580078W_9_EC
show surface, AARNA4580078W_9_EC
load AARNA4580078W_9_LNMAX.pdb
hide everything, AARNA4580078W_9_LNMAX
show cartoon, AARNA4580078W_9_LNMAX
color white, AARNA4580078W_9_LNMAX
spectrum b, rainbow, AARNA4580078W_9_LNMAX
set grid_slot, 3, AARNA4580078W_9_LNMAX
center AARNA4580078W_9_LNMAX
orient AARNA4580078W_9_LNMAX
show surface, AARNA4580078W_9_LNMAX
load AARNA4580078W_9_LNMIN.pdb
hide everything, AARNA4580078W_9_LNMIN
show cartoon, AARNA4580078W_9_LNMIN
color white, AARNA4580078W_9_LNMIN
spectrum b, rainbow, AARNA4580078W_9_LNMIN
set grid_slot, 4, AARNA4580078W_9_LNMIN
center AARNA4580078W_9_LNMIN
orient AARNA4580078W_9_LNMIN
show surface, AARNA4580078W_9_LNMIN
set seq_view, 1
set transparency, 0.0
