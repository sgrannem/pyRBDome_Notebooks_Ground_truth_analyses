set grid_mode,1
set surface_quality, 0
load AARNA2635807W_e_BP.pdb
hide everything, AARNA2635807W_e_BP
show cartoon, AARNA2635807W_e_BP
color white, AARNA2635807W_e_BP
spectrum b, rainbow, AARNA2635807W_e_BP
set grid_slot, 1, AARNA2635807W_e_BP
center AARNA2635807W_e_BP
orient AARNA2635807W_e_BP
show surface, AARNA2635807W_e_BP
load AARNA2635807W_e_EC.pdb
hide everything, AARNA2635807W_e_EC
show cartoon, AARNA2635807W_e_EC
color white, AARNA2635807W_e_EC
spectrum b, rainbow, AARNA2635807W_e_EC
set grid_slot, 2, AARNA2635807W_e_EC
center AARNA2635807W_e_EC
orient AARNA2635807W_e_EC
show surface, AARNA2635807W_e_EC
load AARNA2635807W_e_LNMAX.pdb
hide everything, AARNA2635807W_e_LNMAX
show cartoon, AARNA2635807W_e_LNMAX
color white, AARNA2635807W_e_LNMAX
spectrum b, rainbow, AARNA2635807W_e_LNMAX
set grid_slot, 3, AARNA2635807W_e_LNMAX
center AARNA2635807W_e_LNMAX
orient AARNA2635807W_e_LNMAX
show surface, AARNA2635807W_e_LNMAX
load AARNA2635807W_e_LNMIN.pdb
hide everything, AARNA2635807W_e_LNMIN
show cartoon, AARNA2635807W_e_LNMIN
color white, AARNA2635807W_e_LNMIN
spectrum b, rainbow, AARNA2635807W_e_LNMIN
set grid_slot, 4, AARNA2635807W_e_LNMIN
center AARNA2635807W_e_LNMIN
orient AARNA2635807W_e_LNMIN
show surface, AARNA2635807W_e_LNMIN
set seq_view, 1
set transparency, 0.0
