set grid_mode,1
set surface_quality, 0
load AARNA6661342W_p_BP.pdb
hide everything, AARNA6661342W_p_BP
show cartoon, AARNA6661342W_p_BP
color white, AARNA6661342W_p_BP
spectrum b, rainbow, AARNA6661342W_p_BP
set grid_slot, 1, AARNA6661342W_p_BP
center AARNA6661342W_p_BP
orient AARNA6661342W_p_BP
show surface, AARNA6661342W_p_BP
load AARNA6661342W_p_EC.pdb
hide everything, AARNA6661342W_p_EC
show cartoon, AARNA6661342W_p_EC
color white, AARNA6661342W_p_EC
spectrum b, rainbow, AARNA6661342W_p_EC
set grid_slot, 2, AARNA6661342W_p_EC
center AARNA6661342W_p_EC
orient AARNA6661342W_p_EC
show surface, AARNA6661342W_p_EC
load AARNA6661342W_p_LNMAX.pdb
hide everything, AARNA6661342W_p_LNMAX
show cartoon, AARNA6661342W_p_LNMAX
color white, AARNA6661342W_p_LNMAX
spectrum b, rainbow, AARNA6661342W_p_LNMAX
set grid_slot, 3, AARNA6661342W_p_LNMAX
center AARNA6661342W_p_LNMAX
orient AARNA6661342W_p_LNMAX
show surface, AARNA6661342W_p_LNMAX
load AARNA6661342W_p_LNMIN.pdb
hide everything, AARNA6661342W_p_LNMIN
show cartoon, AARNA6661342W_p_LNMIN
color white, AARNA6661342W_p_LNMIN
spectrum b, rainbow, AARNA6661342W_p_LNMIN
set grid_slot, 4, AARNA6661342W_p_LNMIN
center AARNA6661342W_p_LNMIN
orient AARNA6661342W_p_LNMIN
show surface, AARNA6661342W_p_LNMIN
set seq_view, 1
set transparency, 0.0
