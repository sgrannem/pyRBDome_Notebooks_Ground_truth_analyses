set grid_mode,1
set surface_quality, 0
load AARNA6005280W_3_BP.pdb
hide everything, AARNA6005280W_3_BP
show cartoon, AARNA6005280W_3_BP
color white, AARNA6005280W_3_BP
spectrum b, rainbow, AARNA6005280W_3_BP
set grid_slot, 1, AARNA6005280W_3_BP
center AARNA6005280W_3_BP
orient AARNA6005280W_3_BP
show surface, AARNA6005280W_3_BP
load AARNA6005280W_3_EC.pdb
hide everything, AARNA6005280W_3_EC
show cartoon, AARNA6005280W_3_EC
color white, AARNA6005280W_3_EC
spectrum b, rainbow, AARNA6005280W_3_EC
set grid_slot, 2, AARNA6005280W_3_EC
center AARNA6005280W_3_EC
orient AARNA6005280W_3_EC
show surface, AARNA6005280W_3_EC
load AARNA6005280W_3_LNMAX.pdb
hide everything, AARNA6005280W_3_LNMAX
show cartoon, AARNA6005280W_3_LNMAX
color white, AARNA6005280W_3_LNMAX
spectrum b, rainbow, AARNA6005280W_3_LNMAX
set grid_slot, 3, AARNA6005280W_3_LNMAX
center AARNA6005280W_3_LNMAX
orient AARNA6005280W_3_LNMAX
show surface, AARNA6005280W_3_LNMAX
load AARNA6005280W_3_LNMIN.pdb
hide everything, AARNA6005280W_3_LNMIN
show cartoon, AARNA6005280W_3_LNMIN
color white, AARNA6005280W_3_LNMIN
spectrum b, rainbow, AARNA6005280W_3_LNMIN
set grid_slot, 4, AARNA6005280W_3_LNMIN
center AARNA6005280W_3_LNMIN
orient AARNA6005280W_3_LNMIN
show surface, AARNA6005280W_3_LNMIN
set seq_view, 1
set transparency, 0.0
