set grid_mode,1
set surface_quality, 0
load AARNA5759943W_G_BP.pdb
hide everything, AARNA5759943W_G_BP
show cartoon, AARNA5759943W_G_BP
color white, AARNA5759943W_G_BP
spectrum b, rainbow, AARNA5759943W_G_BP
set grid_slot, 1, AARNA5759943W_G_BP
center AARNA5759943W_G_BP
orient AARNA5759943W_G_BP
show surface, AARNA5759943W_G_BP
load AARNA5759943W_G_EC.pdb
hide everything, AARNA5759943W_G_EC
show cartoon, AARNA5759943W_G_EC
color white, AARNA5759943W_G_EC
spectrum b, rainbow, AARNA5759943W_G_EC
set grid_slot, 2, AARNA5759943W_G_EC
center AARNA5759943W_G_EC
orient AARNA5759943W_G_EC
show surface, AARNA5759943W_G_EC
load AARNA5759943W_G_LNMAX.pdb
hide everything, AARNA5759943W_G_LNMAX
show cartoon, AARNA5759943W_G_LNMAX
color white, AARNA5759943W_G_LNMAX
spectrum b, rainbow, AARNA5759943W_G_LNMAX
set grid_slot, 3, AARNA5759943W_G_LNMAX
center AARNA5759943W_G_LNMAX
orient AARNA5759943W_G_LNMAX
show surface, AARNA5759943W_G_LNMAX
load AARNA5759943W_G_LNMIN.pdb
hide everything, AARNA5759943W_G_LNMIN
show cartoon, AARNA5759943W_G_LNMIN
color white, AARNA5759943W_G_LNMIN
spectrum b, rainbow, AARNA5759943W_G_LNMIN
set grid_slot, 4, AARNA5759943W_G_LNMIN
center AARNA5759943W_G_LNMIN
orient AARNA5759943W_G_LNMIN
show surface, AARNA5759943W_G_LNMIN
set seq_view, 1
set transparency, 0.0
