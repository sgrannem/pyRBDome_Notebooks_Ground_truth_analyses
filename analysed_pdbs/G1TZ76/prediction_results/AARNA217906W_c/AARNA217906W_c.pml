set grid_mode,1
set surface_quality, 0
load AARNA217906W_c_BP.pdb
hide everything, AARNA217906W_c_BP
show cartoon, AARNA217906W_c_BP
color white, AARNA217906W_c_BP
spectrum b, rainbow, AARNA217906W_c_BP
set grid_slot, 1, AARNA217906W_c_BP
center AARNA217906W_c_BP
orient AARNA217906W_c_BP
show surface, AARNA217906W_c_BP
load AARNA217906W_c_EC.pdb
hide everything, AARNA217906W_c_EC
show cartoon, AARNA217906W_c_EC
color white, AARNA217906W_c_EC
spectrum b, rainbow, AARNA217906W_c_EC
set grid_slot, 2, AARNA217906W_c_EC
center AARNA217906W_c_EC
orient AARNA217906W_c_EC
show surface, AARNA217906W_c_EC
load AARNA217906W_c_LNMAX.pdb
hide everything, AARNA217906W_c_LNMAX
show cartoon, AARNA217906W_c_LNMAX
color white, AARNA217906W_c_LNMAX
spectrum b, rainbow, AARNA217906W_c_LNMAX
set grid_slot, 3, AARNA217906W_c_LNMAX
center AARNA217906W_c_LNMAX
orient AARNA217906W_c_LNMAX
show surface, AARNA217906W_c_LNMAX
load AARNA217906W_c_LNMIN.pdb
hide everything, AARNA217906W_c_LNMIN
show cartoon, AARNA217906W_c_LNMIN
color white, AARNA217906W_c_LNMIN
spectrum b, rainbow, AARNA217906W_c_LNMIN
set grid_slot, 4, AARNA217906W_c_LNMIN
center AARNA217906W_c_LNMIN
orient AARNA217906W_c_LNMIN
show surface, AARNA217906W_c_LNMIN
set seq_view, 1
set transparency, 0.0
