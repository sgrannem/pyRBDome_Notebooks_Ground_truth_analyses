set grid_mode,1
set surface_quality, 0
load AARNA778177W_B_BP.pdb
hide everything, AARNA778177W_B_BP
show cartoon, AARNA778177W_B_BP
color white, AARNA778177W_B_BP
spectrum b, rainbow, AARNA778177W_B_BP
set grid_slot, 1, AARNA778177W_B_BP
center AARNA778177W_B_BP
orient AARNA778177W_B_BP
show surface, AARNA778177W_B_BP
load AARNA778177W_B_EC.pdb
hide everything, AARNA778177W_B_EC
show cartoon, AARNA778177W_B_EC
color white, AARNA778177W_B_EC
spectrum b, rainbow, AARNA778177W_B_EC
set grid_slot, 2, AARNA778177W_B_EC
center AARNA778177W_B_EC
orient AARNA778177W_B_EC
show surface, AARNA778177W_B_EC
load AARNA778177W_B_LNMAX.pdb
hide everything, AARNA778177W_B_LNMAX
show cartoon, AARNA778177W_B_LNMAX
color white, AARNA778177W_B_LNMAX
spectrum b, rainbow, AARNA778177W_B_LNMAX
set grid_slot, 3, AARNA778177W_B_LNMAX
center AARNA778177W_B_LNMAX
orient AARNA778177W_B_LNMAX
show surface, AARNA778177W_B_LNMAX
load AARNA778177W_B_LNMIN.pdb
hide everything, AARNA778177W_B_LNMIN
show cartoon, AARNA778177W_B_LNMIN
color white, AARNA778177W_B_LNMIN
spectrum b, rainbow, AARNA778177W_B_LNMIN
set grid_slot, 4, AARNA778177W_B_LNMIN
center AARNA778177W_B_LNMIN
orient AARNA778177W_B_LNMIN
show surface, AARNA778177W_B_LNMIN
set seq_view, 1
set transparency, 0.0
